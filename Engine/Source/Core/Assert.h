#pragma once

namespace PiConspiracy
{
  static bool AssertStaticVarListOverload(
    const char* const a_stringifiedExpression,
    const char* const a_fileName,
    int32 a_lineNumber,
    ESeverityLevel a_level,
    bool a_expression)
  {
    if (!a_expression)
    {
      Debug::Log(a_stringifiedExpression, a_fileName, a_lineNumber, a_level, nullptr);
      return true;
    }

    return false;
}

  template<typename... Arguments>
  static bool AssertStaticVarListOverload(
    const char* const a_stringifiedExpression,
    const char* const a_fileName,
    int32 a_lineNumber,
    ESeverityLevel a_level,
    bool a_expression,
    const char* const a_format,
    Arguments&&... list)
  {
    if (!a_expression)
    {
      AssertStaticVarListOverload(a_stringifiedExpression, a_fileName, a_lineNumber, a_level, a_format, std::forward<Arguments>(list)...);
      return true;
    }

    return false;
  }

  template<typename... Arguments>
  static bool AssertStaticVarListOverload(
    const char* const a_stringifiedExpression,
    const char* const a_fileName,
    int32 a_lineNumber,
    ESeverityLevel a_level,
    const char* const a_format,
    Arguments&&... list)
  {
    Debug::Log(a_stringifiedExpression, a_fileName, a_lineNumber, a_level, a_format, std::forward<Arguments>(list)...);
    return true;
  }
}

#if ENGINE_DEBUG
  #define TODO(x) GLOBAL_MSG("***TODO***\n\tIn file:  " __FILE__ "\n\tLine:     " STRINGIFY(__LINE__) "\nMessage: " STRINGIFY(x))
#else
  #define TODO(x)
#endif

#if PROPAGATE_SEVERITY
  #define BREAK_ERROR DBGBREAK
  #define BREAK_CRITICAL DBGBREAK
#else
  #if ENGINE_DEBUG
    #define BREAK_ERROR DBGBREAK
    #define BREAK_CRITICAL DBGBREAK
  #elif ENGINE_RELEASE
    #define BREAK_ERROR
    #define BREAK_CRITICAL DBGBREAK
  #else
    #define BREAK_ERROR DBGBREAK
    #define BREAK_CRITICAL DBGBREAK
  #endif
#endif

#if WARNINGS_EQUAL_ERRORS
  #define BREAK_WARNING BREAK_ERROR
#else
  #define BREAK_WARNING
#endif

#if VERBOSE_LOG_NONDEBUG
  #define ENABLE_LOGGING 1
#else
#if ENGINE_DEBUG
  #define ENABLE_LOGGING 1
#else
  #define ENABLE_LOGGING 0
#endif
#endif

#define ENGINE_CRITICAL(...) { bool assertFailed = AssertStaticVarListOverload(STRINGIFY(FIRST_ARG(__VA_ARGS__)), FILENAME, LINENUMBER, ESeverityLevel::Critical, __VA_ARGS__); if(assertFailed) { BREAK_CRITICAL; } }
#define ENGINE_ERROR(...) { bool assertFailed = AssertStaticVarListOverload(STRINGIFY(FIRST_ARG(__VA_ARGS__)), FILENAME, LINENUMBER, ESeverityLevel::Error, __VA_ARGS__); if(assertFailed) { BREAK_ERROR; } }
#define ENGINE_WARNING(...) { bool assertFailed = AssertStaticVarListOverload(STRINGIFY(FIRST_ARG(__VA_ARGS__)), FILENAME, LINENUMBER, ESeverityLevel::Warning, __VA_ARGS__); if(assertFailed) { BREAK_WARNING; } }

#if ENABLE_LOGGING
  #define ENGINE_LOG(...) { AssertStaticVarListOverload(STRINGIFY(FIRST_ARG(__VA_ARGS__)), FILENAME, LINENUMBER, ESeverityLevel::Log, __VA_ARGS__); }
#else
  #define ENGINE_LOG(...)
#endif
