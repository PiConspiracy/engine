#include "Precompiled.h"
#include "Stream.h"

PUSH_BASIC_TYPE_RESTRICTORS

#undef unsigned
#undef int
#undef short
#undef long

#include <Windows.h>

POP_BASIC_TYPE_RESTRICTORS


namespace PiConspiracy
{
  static int32 VSPrint(const char* a_format, ...)
  {
    va_list args;
    va_start(args, a_format);

    int32 len = _vscprintf(a_format, args) + 1;
    char* buffer = static_cast<char*>(alloca(len));

    vsprintf_s(buffer, len, a_format, args);
    va_end(args);

    ::OutputDebugStringA(buffer);

    return len;
  }

  VSStream::VSInternalPrinter VSStream::m_internalPrinter;
  VSStream::VSInternalPrinter::VSInternalPrinter(void)
    : m_printInternal(VSPrint)
  {

  }

  FStream::StreamInternal::StreamInternal(void)
    : m_stream(nullptr)
    , m_streamName("Output.txt")
  {

  }

  FStream::StreamInternal::~StreamInternal(void)
  {
    if (m_stream)
    {
      fclose(m_stream);
    }
  }

  void FStream::StreamInternal::TryOpenStream(void)
  {
    if (m_stream)
    {
      fclose(m_stream);
      m_stream = nullptr;
    }

    errno_t error = fopen_s(&m_stream, m_streamName.c_str(), "w");
    if (error != 0)
    {
      ENGINE_WARNING("Failed to open new file stream.");
    }
  }

  void FStream::StreamInternal::ChangeStream(const string& a_streamName)
  {
    m_streamName = a_streamName;
    TryOpenStream();
  }

  FStream::StreamInternal FStream::m_streamHelper;
}
