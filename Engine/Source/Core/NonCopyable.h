#pragma once

namespace PiConspiracy
{
  class NonCopyable
  {
  public:
    NonCopyable(void) {}
    NonCopyable(const NonCopyable&) = delete;
    NonCopyable& operator=(const NonCopyable&) = delete;
  };
}
