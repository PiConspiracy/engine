#pragma once

PUSH_BASIC_TYPE_RESTRICTORS

#undef unsigned
#undef int
#undef short
#undef long

#include <memory>
#include <limits>
#include <math.h>
#include <ctime>

POP_BASIC_TYPE_RESTRICTORS

