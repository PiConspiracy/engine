#pragma once
PUSH_BASIC_TYPE_RESTRICTORS

#undef unsigned
#undef int
#undef short
#undef long

#include <ctime>
#include <random>
POP_BASIC_TYPE_RESTRICTORS

namespace PiConspiracy
{
	class Random 
	{
		private:
		//uint32 m_seed;
		std::random_device m_seeder;
		public:
			Random()
			{
			};
			~Random() {};
			
			int32 GetBinomialDistributedRandom(int32 a_upperBound, float a_probability)
			{
				//thread safe Mersenne Twister random number generator.
				std::mt19937_64 RNGen(m_seeder());
				int32 result;
				std::binomial_distribution<int32> distributor(a_upperBound, a_probability);
				result = distributor(RNGen);
				return result;
			};
		
			int32 GetUniformlyDistributedRandom(int32 a_min,int32 a_max)
			{
				//thread safe Mersenne Twister random number generator.
				std::mt19937_64 RNGen(m_seeder());
				int32 result;
				std::uniform_int_distribution<int32>distributor(a_min, a_max);
				result = distributor(RNGen);
				return result;
			}
		
	};

}