#pragma once

namespace PiConspiracy
{
  template <class T>
  class Stream
  {
  public:
    virtual ~Stream(void) {}

    protected:
    Stream(void) {}

    TODO("static cricital section or mutex here");
  };

  class STDStream : public Stream<STDStream>
  {
  public:
    template <typename... Arguments>
    static int32 PrintOut(const char* a_format, Arguments&&... list)
    {
      TODO("mutex?");

      return fprintf_s(stdout, a_format, std::forward<Arguments>(list)...);
    }

    template <typename... Arguments>
    static int32 PrintErr(const char* a_format, Arguments&&... list)
    {
      TODO("mutex?");

      return fprintf_s(stderr, a_format, std::forward<Arguments>(list)...);
    }

    template<typename... Arguments>
    static int32 Scan(const char* a_format, Arguments&&... list)
    {
      TODO("mutex?");

      return scanf(a_format, std::forward<Arguments>(list)...);
    }
  };

  class VSStream : public Stream<VSStream>
  {
  public:
    template <typename... Arguments>
    static int32 Print(const char* a_format, Arguments&&... list)
    {
      TODO("mutex?");

      return m_internalPrinter.Print(a_format, list...);
    }

  private:
    typedef int32(*PrintInternal)(const char*, ...);

    class VSInternalPrinter
    {
    public:
      VSInternalPrinter(void);

      template <typename... Args>
      int32 Print(const char* a_format, Args&&... list)
      {
        return m_printInternal(a_format, list...);
      }

    private:
      PrintInternal m_printInternal;
    };

    static VSInternalPrinter m_internalPrinter;
  };

  class FStream : public Stream<FStream>
  {
  public:
    template <typename... Arguments>
    static int32 Write(const char* a_format, Arguments&&... list)
    {
      if (m_streamHelper.GetStream() == nullptr)
      {
        m_streamHelper.TryOpenStream();
      }

      return fprintf_s(m_streamHelper.GetStream(), a_format, list...);
    }

    static void ChangeStream(const string& a_streamName)
    {
      m_streamHelper.ChangeStream(a_streamName);
    }

  private:
    class StreamInternal
    {
    public:
      StreamInternal(void);
      ~StreamInternal(void);

      void TryOpenStream(void);
      void ChangeStream(const string& a_streamName);
      FILE* GetStream(void) const { return m_stream; }

    private:
      FILE* m_stream;
      string m_streamName;
    };

    static StreamInternal m_streamHelper;
  };
}
