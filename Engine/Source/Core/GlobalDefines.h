#pragma once

#ifndef _DEBUG
  #define ENGINE_DEBUG 0
#else
  #define ENGINE_DEBUG 1
#endif

#ifndef _RELEASE
#define ENGINE_RELEASE 0
#else
#define ENGINE_RELEASE 1
#endif

// This will make the execution break on warnings.
#define WARNINGS_EQUAL_ERRORS 0

// This will treat non-debug builds (like Release) as debug builds
// For example, errors will break execution on release if this is true.
#define PROPAGATE_SEVERITY 0

// Verbose logging in non-debug configurations.
#define VERBOSE_LOG_NONDEBUG 0

// Whether we use our memory managers.
#define USE_MEMORY_MANAGERS 1

// Whether the leak detector is enabled.
#define USE_LEAK_DETECTOR 1
