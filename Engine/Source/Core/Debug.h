#pragma once

namespace PiConspiracy
{
  // Severity of the logger
  struct SeverityLevel
  {
    enum Enum
    {
      Critical,
      Error,
      Warning,
      Log
    };
  };

  typedef SeverityLevel::Enum ESeverityLevel;

  // Possible streams for debug logging
  struct StreamOutput
  {
    enum Enum
    {
      STDStream = BITMASK(0),
      Filestream = BITMASK(1),
      IDEStream = BITMASK(2),

      All = STDStream | Filestream | IDEStream
    };
  };

  typedef StreamOutput::Enum EStreamOutput;

  class Debug
  {
  public:
    static void SetOutputStreams(uint32 a_streamMask) { m_streamMask = a_streamMask; }
    static uint32 GetOutputStreams(void) { return m_streamMask; }

    // This logger is not intended to be used directly. If you want to simply print something
    // with specific severity, use one of the macros in Assert.h
    template<typename... Arguments>
    static void Log(
      const char* const a_stringifiedExpression,
      const char* const a_fileName,
      int32 a_lineNumber,
      ESeverityLevel a_level,
      const char* const a_format,
      Arguments&&... list)
    {
      //TODO("date and time");

      // Log system info
      Debug::LogInternal("%s %s(%i), ", Debug::SeverityLevelToString(a_level), a_fileName, a_lineNumber);

      // Log expression if it is not a string
      if (a_stringifiedExpression[0] != '"')
      {
        Debug::LogInternal("Expression: %s. ", a_stringifiedExpression);
      }

      // Log formatted text if present
      if (a_format)
      {
        Debug::LogInternal(a_format, std::forward<Arguments>(list)...);
      }

      // Newline at end
      Debug::LogInternal("\n");
    }

  private:
    static const char* SeverityLevelToString(ESeverityLevel a_severetyLevel)
    {
      switch (a_severetyLevel)
      {
      case ESeverityLevel::Critical:
        return "CRITICAL:";

      case ESeverityLevel::Error:
        return "ERROR:   ";

      case ESeverityLevel::Warning:
        return "WARNING: ";

      case ESeverityLevel::Log:
        return "LOG:     ";

      default:
        break;
      }

      return "UNKNOWN: ";
    }

    template<typename... Arguments>
    static void LogInternal(const char* a_format, Arguments&&... list)
    {
      if (CHECKBITS(m_streamMask, EStreamOutput::STDStream))
      {
        STDStream::PrintOut(a_format, std::forward<Arguments>(list)...);
      }

      if (CHECKBITS(m_streamMask, EStreamOutput::Filestream))
      {
        FStream::Write(a_format, std::forward<Arguments>(list)...);
      }

      if (CHECKBITS(m_streamMask, EStreamOutput::IDEStream))
      {
        VSStream::Print(a_format, std::forward<Arguments>(list)...);
      }
    }

    static uint32 m_streamMask;
  };
}
