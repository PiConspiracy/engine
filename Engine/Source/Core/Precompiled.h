#pragma once

// Replacement for basic types
#include "BasicTypes.h"

// Core C/C++ includes
#include "BuiltIn.h"

// STL includes
#include "STL.h"

// Pointer redefinitions
#include "Pointer.h"

// System time
#include "SystemClock.h"

// Various helper macros
#include "Macro.h"

// Global switches
#include "GlobalDefines.h"

// Containers
#include "BaseContainers.h"

// Concurrent containers
#include "ConcurrentContainers.h"

// Debug logger encapsulation
#include "Debug.h"

// Assertion tools
#include "Assert.h"

// SuperFastHash hasher
#include "Hash.h"

// Copy prevention
#include "NonCopyable.h"

// Singleton class
#include "Singleton.h"

// Stream base
#include "Stream.h"

// Simple ring buffer
#include "StaticRingBuffer.h"

//Random Genenerators
#include "Random.h"


