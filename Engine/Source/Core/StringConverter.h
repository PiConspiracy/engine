#pragma once

namespace PiConspiracy
{
  string WCharToChar(const wstring& a_wstring);
  string WCharToChar(const wchar_t* a_wstring);
  string WCharToChar(const wchar_t* a_wstring, size_t size);
}
