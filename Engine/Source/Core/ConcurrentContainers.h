#pragma once

PUSH_BASIC_TYPE_RESTRICTORS

#undef unsigned
#undef int
#undef short
#undef long

#include <concurrent_vector.h>
#include <concurrent_queue.h>
#include <concurrent_unordered_map.h>

POP_BASIC_TYPE_RESTRICTORS

namespace PiConspiracy
{
  template <typename Type, class Allocator = std::allocator<Type> >
  using concurrent_vector = concurrency::concurrent_vector<Type, Allocator>;

  template <typename Type, class Allocator = std::allocator<Type> >
  using concurrent_queue = concurrency::concurrent_queue<Type, Allocator>;

  template <
    typename Key, 
    typename Type, 
    typename Hash = std::hash<Key>, 
    typename KeyEqual = std::equal_to<Key>,
    typename Allocator = std::allocator<std::pair<const Key, Type> > >
  using concurrent_unordered_map = concurrency::concurrent_unordered_map<Key, Type, Hash, KeyEqual, Allocator>;
}

