#pragma once

namespace PiConspiracy
{
  template <class T>
  class Singleton : public NonCopyable
  {
  public:
    static bool Exists(void)
    {
      return m_self != nullptr;
    }

    static T* Get(void)
    {
      ENGINE_CRITICAL(m_self != nullptr);

      return static_cast<T*>(m_self);
    }

  protected:
    Singleton(void)
    {
      ENGINE_CRITICAL(m_self == nullptr);

      m_self = static_cast<void*>(this);
    }

    ~Singleton(void)
    {
      ENGINE_CRITICAL(m_self != nullptr);

      m_self = nullptr;
    }

  public:
    static void* m_self;
  };

  template <class T>
  void* Singleton<T>::m_self = nullptr;
}
