#pragma once

#include <cstdint>

#define unsigned __pragma(message("Please use an appropriate byte-based representation of this basic type."))
#define int __pragma(message("Please use an appropriate byte-based representation of this basic type."))
#define short __pragma(message("Please use an appropriate byte-based representation of this basic type."))
#define long __pragma(message("Please use an appropriate byte-based representation of this basic type."))

#define PUSH_BASIC_TYPE_RESTRICTORS \
__pragma(push_macro("unsigned")) \
__pragma(push_macro("int")) \
__pragma(push_macro("short")) \
__pragma(push_macro("long"))

#define POP_BASIC_TYPE_RESTRICTORS \
__pragma(pop_macro("unsigned")) \
__pragma(pop_macro("int")) \
__pragma(pop_macro("short")) \
__pragma(pop_macro("long"))

namespace PiConspiracy
{
  typedef int8_t  int8;
  typedef int16_t int16;
  typedef int32_t int32;
  typedef int64_t int64;

  typedef uint8_t  uint8;
  typedef uint16_t uint16;
  typedef uint32_t uint32;
  typedef uint64_t uint64;
}

