#pragma once

PUSH_BASIC_TYPE_RESTRICTORS

#undef unsigned
#undef int
#undef short
#undef long

// Generic functionality and algorithms
#include <cstdlib>
#include <cstdarg>
#include <typeinfo>
#include <utility>
#include <memory>
#include <cassert>
#include <cstring>
#include <algorithm>

// Containers
#include <string>
#include <list>
#include <array>
#include <vector>
#include <map>
#include <unordered_map>
#include <queue>
#include <deque>

POP_BASIC_TYPE_RESTRICTORS


