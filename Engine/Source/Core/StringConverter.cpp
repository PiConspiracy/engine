#include "Precompiled.h"
#include "StringConverter.h"

PUSH_BASIC_TYPE_RESTRICTORS

#undef unsigned
#undef int
#undef short
#undef long

#include <codecvt>
#include <locale.h>

#pragma warning (disable : 4996)

POP_BASIC_TYPE_RESTRICTORS

namespace PiConspiracy
{
  string WCharToChar(const wstring& a_wstring)
  {
    std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t> converter;
    return converter.to_bytes(a_wstring);
  }

  string WCharToChar(const wchar_t* a_wstring)
  {
    std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t> converter;
    return converter.to_bytes(wstring(a_wstring));
  }

  string WCharToChar(const wchar_t* a_wstring, size_t size)
  {
    std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t> converter;
    return converter.to_bytes(wstring(a_wstring, size));
  }
}
