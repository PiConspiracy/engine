#include "Precompiled.h"
#include "SystemClock.h"

PUSH_BASIC_TYPE_RESTRICTORS

#undef unsigned
#undef int
#undef short
#undef long

#include <ctime>

POP_BASIC_TYPE_RESTRICTORS

namespace PiConspiracy
{
  void SystemClock::GetDateAndTime(DateAndTime& a_dateAndTime)
  {
    ::tm container;
    time_t t = ::time(0);
    ::localtime_s(&container, &t);

    a_dateAndTime.m_day = container.tm_mday;
    a_dateAndTime.m_month = container.tm_mon + 1;
    a_dateAndTime.m_year = container.tm_year + 1900;
    a_dateAndTime.m_second = container.tm_sec;
    a_dateAndTime.m_minute = container.tm_min;
    a_dateAndTime.m_hour = container.tm_hour;
  }
}
