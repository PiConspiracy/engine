#pragma once

namespace PiConspiracy
{
  typedef std::string string;
  typedef std::wstring wstring;

  template <class Type, class Allocator = std::allocator<Type> >
  using list = std::list<Type, Allocator>;

  template <class Type, size_t Size>
  using array = std::array<Type, Size>;

  template <class Type, class Allocator = std::allocator<Type> >
  using vector = std::vector<Type, Allocator>;

  template <
    class Key, 
    class Type, 
    class Compare = std::less<Key>, 
    class Allocator = std::allocator<std::pair<const Key, Type> > >
  using map = std::map<Key, Type, Compare, Allocator>;

  template <
    class Key, 
    class Type, 
    class Hash = std::hash<Key>,
    class KeyEqual = std::equal_to<Key>,
    class Allocator = std::allocator<std::pair<const Key, Type> > >
  using unordered_map = std::unordered_map<Key, Type, Hash, KeyEqual, Allocator>;

  template <class Type, class Container = std::deque<Type> >
  using queue = std::queue<Type, Container>;

  template <class Type, class Allocator = std::allocator<Type>>
  using deque = std::deque<Type, Allocator>;
}

