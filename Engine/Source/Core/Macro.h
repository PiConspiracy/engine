#pragma once

// Macro expansion (to avoid annoying MS bug)
#define EXPAND(x) x

// Stringification of a macro expression
#define STRINGIFY_INTERNAL(x) #x
#define STRINGIFY(x) STRINGIFY_INTERNAL(x)

// Separation of the first argument in the variadic list
#define FIRST_ARG_INTERNAL(First, ...) First
#define FIRST_ARG(First, ...) FIRST_ARG_INTERNAL(First, __VA_ARGS__)

// Returns the mask of a specific bit
#define BITMASK(n) (1u << n)

// Checks whether the bit mask contains the bit combination
#define CHECKBITS(Flags, Bits) ((Flags & (Bits)) == (Bits))

// Message that can be placed anywhere
#define GLOBAL_MSG(msg) PRAGMA(message(msg))

// Disable optimizations for code below (such as inline expansion)
#define OPTIMIZATIONS_DISABLE PRAGMA(optimize("", off))

// Enable optimizations below (correspond to the project optimization complexity)
#define OPTIMIZATIONS_ENABLE PRAGMA(optimize("", on))

// Macro-friendly pragma redefinition
#define PRAGMA __pragma

// Force the compiler to inline a function
#define FORCEINLINE __forceinline

// Force the compiler to not inline a function
#define NOINLINE __declspec(noinline)

// Exportable code
#define DLLEXPORT __declspec(dllexport)

// Importable code
#define DLLIMPORT __declspec(dllimport)

// Execution stopper
#define DBGBREAK { int32* brkptr = nullptr; *brkptr = 0; }

// File name
#define FILENAME __FILE__

// Line number
#define LINENUMBER __LINE__


