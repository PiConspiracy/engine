#pragma once

namespace PiConspiracy
{
  struct DateAndTime
  {
    int32 m_year;
    int32 m_month;
    int32 m_day;
    int32 m_hour;
    int32 m_minute;
    int32 m_second;
  };

  class SystemClock
  {
  public:
    static void GetDateAndTime(DateAndTime& a_dateAndTime);
  };
}

