#pragma once

namespace PiConspiracy
{
  template<class Type>
  using WeakPtr = std::weak_ptr<Type>;

  template<class Type>
  using SharedPtr = std::shared_ptr<Type>;

  template<class Type>
  using UniquePtr = std::unique_ptr<Type>;
}

