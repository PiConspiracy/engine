#pragma once

namespace PiConspiracy
{
  template <typename T>
  class StaticRingBuffer : public NonCopyable
  {
  public:
    StaticRingBuffer(void)
      : m_size(1)
      , m_index(0)
      , m_count(0)
    {
      m_buffer = new T[1];
    }

    StaticRingBuffer(uint32 a_size)
      : m_size(a_size)
      , m_index(0)
      , m_count(0)
    {
      m_buffer = new T[a_size];
    }

    ~StaticRingBuffer(void)
    {
      delete[] m_buffer;
    }

    void Push(const T& a_element)
    {
      if (m_index >= m_size)
      {
        m_index = 0;
      }

      m_buffer[m_index] = a_element;
      ++m_count;
    }

    T* Last(void)
    {
      if (m_count == 0)
      {
        return nullptr;
      }

      return &m_buffer[m_index - 1];
    }

    void Flush(void)
    {
      delete[] m_buffer;
      m_buffer = new T[m_size];
    }

  private:
    T* m_buffer;
    uint32 m_size;
    uint32 m_index;
    uint32 m_count;
  };
}
