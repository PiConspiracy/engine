#pragma once

namespace PiConspiracy
{
  uint32 Hash(const char* data, int32 len);
  uint32 Hash(const char* data);
  uint32 Hash(const string& data, int32 len);
  uint32 Hash(const string& data);
}
