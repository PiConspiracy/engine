#pragma once

struct IDXGIFactory5;
struct IDXGIAdapter3;

namespace PiConspiracy
{
  class Adapter
  {
  public:
    virtual bool Initialize(void) = 0;
    virtual void Exit(void) = 0;

    const IDXGIFactory5*   GetAdapterFactory(void)     const { return m_adapterFactory; }
    const IDXGIAdapter3*   GetDisplayAdapter(void)     const { return m_displayAdapter; }
    const AdapterDesc      GetAdapterDesc(void)        const { return m_adapterDesc; }
    const Device*          GetDevice(uint32 a_index = 0) const;
    const EFeatureLevel    GetFeatureLevel(void)       const { return m_featureLevel; }
    const vector<Output*>& GetOutputs(void)            const { return m_outputs; }
    const uint32           GetAdapterIndex(void)       const { return m_adapterIndex; }
    const uint32           GetDeviceNodeCount(void)    const;

    void SetDefaultAdaptor(bool a_value) { m_defaultAdaptor = a_value; }
    const bool IsDefaultAdaptor(void) const { return m_defaultAdaptor; };

#if ENGINE_DEBUG
    void LogAdaptorDesc(void);
#endif

  protected:
    Adapter(IDXGIAdapter3* a_adapter, uint32 a_index);
    ~Adapter(void);

    vector<Output*> m_outputs;
    IDXGIFactory5*  m_adapterFactory;
    IDXGIAdapter3*  m_displayAdapter;
    AdapterDesc     m_adapterDesc;
    vector<Device*> m_devices;
    EFeatureLevel   m_featureLevel;
    EGPUVendor      m_vendor;
    uint32          m_adapterIndex;
    bool            m_defaultAdaptor;
    bool            m_hardwareDriver;
  };
}

