#pragma once

namespace PiConspiracy
{
  class ARI : public Singleton<ARI>
  {
  public:
    virtual void Initialize(void) = 0;
    virtual void Update(void) = 0;
    virtual void Exit(void) = 0;

    // Accessor & Utils
    const vector<Adapter*>& GetAdapters(void) const { return m_adapters; }
    bool IsSupported(void) const { return m_supported; }
    static ARI* CreateARI(EARIAPI a_api);

    // States
    virtual DepthStrencilState* CreateDepthStencilState(DepthStencilDesc& a_desc) = 0;
    virtual RasterizerState* CreateRasterizerState(RasterizerStateDesc& a_desc) = 0;
    virtual SamplerState* CreateSamplerState(SamplerStateDesc& a_desc) = 0;
    virtual BlendState* CreateBlendState(BlendStateDesc& a_desc) = 0;

    // RI functions
    virtual Shader* CreateShader(const string& a_path) = 0;
    virtual Shader* CreateShader(const vector<uint8>& a_bytes) = 0;

  protected:
    ARI(void);
    ~ARI(void);

    vector<Adapter*> m_adapters;
    IDXGIFactory5*   m_adapterFactory;
    bool             m_supported;
  };
}

