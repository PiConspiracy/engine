#include "Precompiled.h"
#include "D3D11Adapter.h"

namespace PiConspiracy
{
  D3D11Adapter::D3D11Adapter(IDXGIAdapter3* a_adapter, uint32 a_index)
    : Adapter(a_adapter, a_index)
  {

  }

  D3D11Adapter::~D3D11Adapter(void)
  {
    Exit();
  }

  bool D3D11Adapter::Initialize(void)
  {
    DXGI_ADAPTER_DESC2 d3d11Desc;
    m_displayAdapter->GetDesc2(&d3d11Desc);
    memcpy(&m_adapterDesc, &d3d11Desc, sizeof(DXGI_ADAPTER_DESC2));

    m_vendor = static_cast<EGPUVendor>(m_adapterDesc.m_vendorID);
    if (m_vendor != EGPUVendor::AMD &&
      m_vendor != EGPUVendor::Intel &&
      m_vendor != EGPUVendor::Nvidia &&
      m_vendor != EGPUVendor::Microsoft)
    {
      m_vendor = EGPUVendor::Unknown;
    }
    
    if (m_adapterDesc.m_flags & DXGI_ADAPTER_FLAG_SOFTWARE)
    {
      m_hardwareDriver = false;
    }

    CreateOutputs();
    bool success = CreateDevice();

#if ENGINE_DEBUG
    LogAdaptorDesc();
#endif

    return success;
  }

  void D3D11Adapter::Exit(void)
  {

  }

  void D3D11Adapter::CreateOutputs(void)
  {
    IDXGIOutput* tempOutput;
    for (uint32 i = 0; m_displayAdapter->EnumOutputs(i, &tempOutput) != DXGI_ERROR_NOT_FOUND; ++i)
    {
      if (tempOutput)
      {
        IDXGIOutput5* outputLatestInterface;
        tempOutput->QueryInterface<IDXGIOutput5>(&outputLatestInterface);
        ENGINE_CRITICAL(outputLatestInterface != nullptr);

        Output* output = new Output;
        output->m_output = outputLatestInterface;

        DXGI_OUTPUT_DESC outputDesc;
        outputLatestInterface->GetDesc(&outputDesc);
        memcpy(&output->m_outputDesc, &outputDesc, sizeof(DXGI_OUTPUT_DESC));

        m_outputs.push_back(output);
      }
    }
  }

  bool D3D11Adapter::CreateDevice(void)
  {
    if (!m_hardwareDriver)
    {
      return false;
    }

    //HRESULT result;
    //
    //D3D_FEATURE_LEVEL SupportedLevels[] =
    //{
    //  D3D_FEATURE_LEVEL_12_1,
    //  D3D_FEATURE_LEVEL_12_0,
    //  D3D_FEATURE_LEVEL_11_1,
    //  D3D_FEATURE_LEVEL_11_0
    //};
    //
    //ID3D12Device* device = nullptr;
    //uint32 SupportedLevelCount = ARRAY_SIZE(SupportedLevels);
    //for (uint32 i = 0; i < SupportedLevelCount; ++i)
    //{
    //  result = D3D12CreateDevice(m_displayAdapter, SupportedLevels[i], IID_PPV_ARGS(&device));
    //  if (result == S_OK)
    //  {
    //    break;
    //  }
    //}
    //
    //if (!device)
    //{
    //  return false;
    //}

    //# ifdef _DEBUG
    //    ID3D12InfoQueue* d3dInfoQueue = nullptr;
    //    result = device->QueryInterface(__uuidof(ID3D12InfoQueue), (void**)&d3dInfoQueue);
    //    ENGINE_CRITICAL(result == S_OK);
    //
    //    d3dInfoQueue->SetBreakOnSeverity(D3D12_MESSAGE_SEVERITY_CORRUPTION, true);
    //    d3dInfoQueue->SetBreakOnSeverity(D3D12_MESSAGE_SEVERITY_ERROR, true);
    //
    //    d3dInfoQueue->Release();
    //# endif

    // Create GPU nodes
    //for (uint32 i = 0; i < device->GetNodeCount(); ++i)
    //{
    //  m_devices.push_back(new DX12Device(device, i));
    //}

    return true;
  }
}

