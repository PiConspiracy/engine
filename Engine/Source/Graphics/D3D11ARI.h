#pragma once

namespace PiConspiracy
{
  class D3D11ARI : public Singleton<D3D11ARI>, public ARI
  {
  public:
    D3D11ARI(void);
    ~D3D11ARI(void);

    virtual void Initialize(void);
    virtual void Update(void);
    virtual void Exit(void);

    // States
    virtual DepthStrencilState* CreateDepthStencilState(DepthStencilDesc& a_desc);
    virtual RasterizerState* CreateRasterizerState(RasterizerStateDesc& a_desc);
    virtual SamplerState* CreateSamplerState(SamplerStateDesc& a_desc);
    virtual BlendState* CreateBlendState(BlendStateDesc& a_desc);

    // RI functions
    virtual Shader* CreateShader(const string& a_path);
    virtual Shader* CreateShader(const vector<uint8>& a_bytes);

  private:
    void CreateAdaptorFactory(void);
    void CreateAdaptors(void);
  };
}

