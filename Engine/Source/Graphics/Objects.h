#pragma once

namespace PiConspiracy
{
  class Viewport {public:};
  class Shader {public:};
  class Device {public:};

  class Output
  {
  public:
    void*      m_output;
    OutputDesc m_outputDesc;
  };
}

