﻿#pragma once

namespace PiConspiracy
{
  struct DepthWriteMask
  {
    enum Enum
    {
      // Turn off writes to the depth-stencil buffer.
      Zero,
      // Turn on writes to the depth-stencil buffer.
      All
    };
  };

  typedef DepthWriteMask::Enum EDepthWriteMask;

  struct ComparisonFunc
  {
    enum Enum
    {
      // Never pass the comparison.
      Never,
      // If the source data is less than the destination data, the comparison passes.
      Less,
      // If the source data is equal to the destination data, the comparison passes.
      Equal,
      // If the source data is less than or equal to the destination data, the comparison passes.
      LessEqual,
      // If the source data is greater than the destination data, the comparison passes.
      Greater,
      // If the source data is not equal to the destination data, the comparison passes.
      NotEqual,
      // If the source data is greater than or equal to the destination data, the comparison passes.
      GreaterEqual,
      // Always pass the comparison.
      Always
    };
  };

  typedef ComparisonFunc::Enum EComparisonFunc;

  struct StencilOp
  {
    enum Enum
    {
      // Keep the existing stencil data.
      Keep,
      // Set the stencil data to 0.
      Zero,
      // Set the stencil data to the reference value set by calling ID3DXDeviceContext::OMSetDepthStencilState.
      Replace,
      // Increment the stencil value by 1, and clamp the result.
      IncrementSat,
      // Decrement the stencil value by 1, and clamp the result.
      DecrementSat,
      // Invert the stencil data.
      Invert,
      // Increment the stencil value by 1, and wrap the result if necessary.
      Increment,
      // Decrement the stencil value by 1, and wrap the result if necessary.
      Decrement
    };
  };

  typedef StencilOp::Enum EStencilOp;

  struct FillMode
  {
    enum Enum
    {
      // Draw lines connecting the vertices. Adjacent vertices are not drawn.
      Wireframe,
      // Fill the triangles formed by the vertices. Adjacent vertices are not drawn.
      Solid
    };
  };

  typedef FillMode::Enum EFillMode;

  struct CullMode
  {
    enum Enum
    {
      // Always draw all triangles.
      None,
      // Do not draw triangles that are front-facing.
      Front,
      // Do not draw triangles that are back-facing.
      Back
    };
  };

  typedef CullMode::Enum ECullMode;

  struct ConservativeRasterizationMode
  {
    enum Enum
    {
      // Conservative rasterization is off.
      Off,
      // Conservative rasterization is on.
      On
    };
  };

  typedef ConservativeRasterizationMode::Enum EConservativeRasterizationMode;

  struct Filter
  {
    enum Enum
    {
      // Resolved as MIN_MAG_MIP_POINT.
      Point,
      // Resolved as MIN_MAG_LINEAR_MIP_POINT.
      Bilinear,
      // Resolved as MIN_MAG_MIP_LINEAR.
      Trilinear,
      // Resolved as ANISOTROPIC.
      Anisotropic,
    };
  };

  typedef Filter::Enum EFilter;

  struct TextureAddressMode
  {
    enum Enum
    {
      // Tile the texture at every (u,v) integer junction. For example, for u values between 0 and 3, the texture is repeated three times.
      Wrap,
      // Flip the texture at every (u,v) integer junction. For u values between 0 and 1, for example, the texture is addressed normally; 
      // between 1 and 2, the texture is flipped (mirrored); between 2 and 3, the texture is normal again; and so on.
      Mirror,
      // Texture coordinates outside the range [0.0, 1.0] are set to the texture color at 0.0 or 1.0, respectively.
      Clamp,
      // Texture coordinates outside the range [0.0, 1.0] are set to the border color specified in D3D11_SAMPLER_DESC or HLSL code.
      Border,
      // Similar to D3D11_TEXTURE_ADDRESS_MIRROR and D3D11_TEXTURE_ADDRESS_CLAMP. Takes the absolute value of the texture coordinate 
      // (thus, mirroring around 0), and then clamps to the maximum value.
      MirrorOnce
    };
  };

  typedef TextureAddressMode::Enum ETextureAddressMode;

  struct Blend
  {
    enum Enum
    {
      // The blend factor is (0, 0, 0, 0). No pre-blend operation.
      Zero,
      // The blend factor is (1, 1, 1, 1). No pre-blend operation.
      One,
      // The blend factor is (Rₛ, Gₛ, Bₛ, Aₛ), that is color data (RGB) from a pixel shader. No pre-blend operation.
      SrcColor,
      // The blend factor is (1 - Rₛ, 1 - Gₛ, 1 - Bₛ, 1 - Aₛ), that is color data (RGB) from a pixel shader. The pre-blend operation inverts the data, generating 1 - RGB.
      InvSrcColor,
      // The blend factor is (Aₛ, Aₛ, Aₛ, Aₛ), that is alpha data (A) from a pixel shader. No pre-blend operation.
      SrcAlpha,
      // The blend factor is ( 1 - Aₛ, 1 - Aₛ, 1 - Aₛ, 1 - Aₛ), that is alpha data (A) from a pixel shader. The pre-blend operation inverts the data, generating 1 - A.
      InvSrcAlpha,
      // The blend factor is (Ad Ad Ad Ad), that is alpha data from a render target. No pre-blend operation.
      DestAlpha,
      // The blend factor is (1 - Ad 1 - Ad 1 - Ad 1 - Ad), that is alpha data from a render target. The pre-blend operation inverts the data, generating 1 - A.
      InvDestAlpha,
      // The blend factor is (Rd, Gd, Bd, Ad), that is color data from a render target. No pre-blend operation.
      DestColor,
      // The blend factor is (1 - Rd, 1 - Gd, 1 - Bd, 1 - Ad), that is color data from a render target. The pre-blend operation inverts the data, generating 1 - RGB.
      InvDestColor,
      // The blend factor is (f, f, f, 1); where f = min(Aₛ, 1 - Ad). The pre-blend operation clamps the data to 1 or less.
      SrcAlphaSat,
      // The blend factor is the blend factor set with ID3DXDeviceContext::OMSetBlendState. No pre-blend operation.
      BlendFactor,
      // The blend factor is the blend factor set with ID3DXDeviceContext::OMSetBlendState. The pre-blend operation inverts the blend factor, generating 1 - blend_factor.
      InvBlendFactor,
      // The blend factor is data sources both as color data output by a pixel shader.There is no pre - blend operation.This blend factor supports dual - source color blending.
      Src1Color,
      // The blend factor is data sources both as color data output by a pixel shader. The pre-blend operation inverts the data, generating 1 - RGB. 
      // This blend factor supports dual-source color blending.
      InvSrc1Color,
      // The blend factor is data sources as alpha data output by a pixel shader. There is no pre-blend operation. This blend factor supports dual-source color blending.
      Src1Alpha,
      // The blend factor is data sources as alpha data output by a pixel shader. The pre-blend operation inverts the data, generating 1 - A. 
      // This blend factor supports dual-source color blending.
      InvSrc1Alpha
    };
  };

  typedef Blend::Enum EBlend;

  struct BlendOp
  {
    enum Enum
    {
      // Add source 1 and source 2.
      Add,
      // Subtract source 1 from source 2.
      Subtract,
      // Subtract source 2 from source 1.
      RevSubtract,
      // Find the minimum of source 1 and source 2.
      Min,
      // Find the maximum of source 1 and source 2.
      Max
    };
  };

  typedef BlendOp::Enum EBlendOp;
}

