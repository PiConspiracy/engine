#include "Precompiled.h"
#include "ARI.h"

#include "D3D11ARI.h"

namespace PiConspiracy
{
  ARI::ARI(void) :
    m_adapterFactory(nullptr),
    m_supported(false)
  {

  }

  ARI::~ARI(void)
  {

  }

  ARI* ARI::CreateARI(EARIAPI a_api)
  {
    switch (a_api)
    {
    case EARIAPI::D3D11:
    {
      return new D3D11ARI();
    }
    break;

    case EARIAPI::D3D12:
    {
      return nullptr;
    }
    break;

    default:
      ENGINE_CRITICAL("Unsupported ARI version requested.");
    }

    return nullptr;
  }
}
