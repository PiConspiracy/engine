#include "Precompiled.h"
#include "D3D11ARI.h"
#include "D3D11Adapter.h"

namespace PiConspiracy
{
  D3D11ARI::D3D11ARI(void)
  {

  }

  D3D11ARI::~D3D11ARI(void)
  {

  }

  void D3D11ARI::Initialize(void)
  {
    CreateAdaptorFactory();
    CreateAdaptors();
  }

  void D3D11ARI::Update(void)
  {

  }

  void D3D11ARI::Exit(void)
  {

  }

  // States
  DepthStrencilState* D3D11ARI::CreateDepthStencilState(DepthStencilDesc& a_desc)
  {
    return new DepthStrencilState();
  }

  RasterizerState* D3D11ARI::CreateRasterizerState(RasterizerStateDesc& a_desc)
  {
    return new RasterizerState();
  }

  SamplerState* D3D11ARI::CreateSamplerState(SamplerStateDesc& a_desc)
  {
    return new SamplerState();
  }

  BlendState* D3D11ARI::CreateBlendState(BlendStateDesc& a_desc)
  {
    return new BlendState();
  }

  // RI functions
  Shader* D3D11ARI::CreateShader(const string& a_path)
  {
    return new Shader();
  }

  Shader* D3D11ARI::CreateShader(const vector<uint8>& a_bytes)
  {
    return new Shader();
  }

  void D3D11ARI::CreateAdaptorFactory(void)
  {
    ENGINE_CRITICAL(m_adapterFactory == nullptr);

    UINT flags = 0;

#if ENGINE_DEBUG
    flags |= DXGI_CREATE_FACTORY_DEBUG;
#endif

    HRESULT result = ::CreateDXGIFactory2(flags, __uuidof(IDXGIFactory5), (void**)&m_adapterFactory);
    ENGINE_CRITICAL(result == S_OK, "Failed to create DXGI factory! Error %ld", result);
  }

  void D3D11ARI::CreateAdaptors(void)
  {
    ENGINE_CRITICAL(m_adapters.size() == 0);
    
    IDXGIAdapter* tempAdapter;
    for (uint32 i = 0; m_adapterFactory->EnumAdapters(i, &tempAdapter) != DXGI_ERROR_NOT_FOUND; ++i)
    {
      if (tempAdapter)
      {
        IDXGIAdapter3* adapterLatestInterface;
        tempAdapter->QueryInterface<IDXGIAdapter3>(&adapterLatestInterface);
        ENGINE_CRITICAL(adapterLatestInterface != nullptr);
      
        Adapter* adapter = new D3D11Adapter(adapterLatestInterface, i);
        bool success = adapter->Initialize();
        if (success)
        {
          m_adapters.push_back(adapter);
        }
      }
    }
  }
}
