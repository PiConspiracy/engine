#pragma once

namespace PiConspiracy
{
  // Describes depth-stencil state.
  struct DepthStencilOpDesc
  {
    EStencilOp      m_stencilFailOp;
    EStencilOp      m_stencilDepthFailOp;
    EStencilOp      m_stencilPassOp;
    EComparisonFunc m_stencilFunc;
  };

  // Describes depth-stencil state.
  struct DepthStencilDesc
  {
    bool               m_depthEnable;
    EDepthWriteMask    m_depthWriteMask;
    EComparisonFunc    m_depthFunc;
    bool               m_stencilEnable;
    uint8              m_stencilReadMask;
    uint8              m_stencilWriteMask;
    DepthStencilOpDesc m_frontFace;
    DepthStencilOpDesc m_backFace;
  };

  // Describes rasterizer state.
  struct RasterizerStateDesc
  {
    EFillMode                      m_fillMode;
    ECullMode                      m_cullMode;
    bool                           m_frontCounterClockwise;
    int32                          m_depthBias;
    float                          m_depthBiasClamp;
    float                          m_slopeScaledDepthBias;
    bool                           m_depthClipEnable;
    bool                           m_multisampleEnable;
    bool                           m_antialiasedLineEnable;
    uint32                         m_forcedSampleCount;
    EConservativeRasterizationMode m_conservativeRaster;
  };

  // Describes a sampler state.
  struct SamplerStateDesc
  {
    EFilter             m_filter;
    ETextureAddressMode m_addressU;
    ETextureAddressMode m_addressV;
    ETextureAddressMode m_addressW;
    float               m_mipLODBias;
    uint32              m_maxAnisotropy;
    EComparisonFunc     m_comparisonFunc;
    float               m_borderColor[4];
    float               m_minLOD;
    float               m_maxLOD;
  };

  // Describes the blend state for a render target.
  struct RenderTargetBlendDesc
  {
    bool     m_blendEnable;
    bool     m_logicOpEnable;
    EBlend   m_srcBlend;
    EBlend   m_destBlend;
    EBlendOp m_blendOp;
    EBlend   m_srcBlendAlpha;
    EBlend   m_destBlendAlpha;
    EBlend   m_blendOpAlpha;
    uint8    m_renderTargetWriteMask;
  };

  // Describes the blend state for a blend state object.
  struct BlendStateDesc
  {
    bool m_alphaToCoverageEnable;
    bool m_independentBlendEnable;
    RenderTargetBlendDesc m_renderTarget[8];
  };

  class DepthStrencilState {};
  class RasterizerState {};
  class SamplerState {};
  class BlendState {};
}

