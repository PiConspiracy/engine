#pragma once

// Abstract rendering interface definitions.
#include "ARIDefinitions.h"
#include "Definitions.h"
#include "States.h"
#include "Objects.h"

// Low-level interface.
#include "Adapter.h"

// The interface body.
#include "ARI.h"

