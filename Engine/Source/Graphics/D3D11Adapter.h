#pragma once

namespace PiConspiracy
{
  class D3D11Adapter : public Adapter
  {
  public:
    D3D11Adapter(IDXGIAdapter3* a_adapter, uint32 a_index);
    ~D3D11Adapter(void);

    virtual bool Initialize(void) final;
    virtual void Exit(void) final;

  private:
    void CreateOutputs(void);
    bool CreateDevice(void);
  };
}
