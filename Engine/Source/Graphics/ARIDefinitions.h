#pragma once

namespace PiConspiracy
{
  struct GPUVendor
  {
    enum Enum
    {
      Intel = 0x8086,
      Nvidia = 0x10de,
      AMD = 0x1002,
      Microsoft = 0x1414,
      Unknown
    };
  };

  typedef GPUVendor::Enum EGPUVendor;

  struct ARIAPI
  {
    enum Enum
    {
      D3D11,
      D3D12
    };
  };

  typedef ARIAPI::Enum EARIAPI;

  struct FeatureLevel
  {
    enum Enum
    {
      D3D9_1 = 0x9100,
      D3D9_2 = 0x9200,
      D3D9_3 = 0x9300,
      D3D10_0 = 0xa000,
      D3D10_1 = 0xa100,
      D3D11_0 = 0xb000,
      D3D11_1 = 0xb100,
      D3D12_0 = 0xc000,
      D3D12_1 = 0xc100
    };
  };

  typedef FeatureLevel::Enum EFeatureLevel;

  struct LuID
  {
    uint32 LowPart;
    int32 HighPart;
  };

  struct GraphicsPreemptionGranularity
  {
    enum Enum
    {
      DMA_BUFFER_BOUNDARY = 0,
      PRIMITIVE_BOUNDARY = 1,
      TRIANGLE_BOUNDARY = 2,
      PIXEL_BOUNDARY = 3,
      INSTRUCTION_BOUNDARY = 4
    };
  };

  typedef GraphicsPreemptionGranularity::Enum EGraphicsPreemptionGranularity;

  struct ComputePreemptionGranularity
  {
    enum Enum
    {
      DMA_BUFFER_BOUNDARY = 0,
      DISPATCH_BOUNDARY = 1,
      THREAD_GROUP_BOUNDARY = 2,
      THREAD_BOUNDARY = 3,
      INSTRUCTION_BOUNDARY = 4
    };
  };

  typedef ComputePreemptionGranularity::Enum EComputePreemptionGranularity;

  struct AdapterDesc
  {
    wchar_t m_description[128];
    uint32 m_vendorID;
    uint32 m_deviceID;
    uint32 m_subSystemID;
    uint32 m_revision;
    size_t m_dedicatedVideoMemory;
    size_t m_dedicatedSystemMemory;
    size_t m_sharedSystemMemory;
    LuID m_adapterLUID;
    uint32 m_flags;
    EGraphicsPreemptionGranularity m_graphicsPreemption;
    EComputePreemptionGranularity m_computePreemption;
  };

  struct Rect
  {
    int32 left;
    int32 top;
    int32 right;
    int32 bottom;
  };

  struct ModeRotation
  {
    enum Enum
    {
      UNSPECIFIED = 0,
      IDENTITY = 1,
      ROTATE90 = 2,
      ROTATE180 = 3,
      ROTATE270 = 4
    };
  };

  typedef ModeRotation::Enum EModeRotation;

  struct OutputDesc
  {
    wchar_t DeviceName[32];
    Rect DesktopCoordinates;
    int32 AttachedToDesktop;
    EModeRotation Rotation;
    void* Monitor;
  };
}

