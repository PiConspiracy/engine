#pragma once

// Engine
#include "Core/Core.h"
#include "Math/Math.h"
#include "Engine/Engine.h"

PUSH_BASIC_TYPE_RESTRICTORS

#undef unsigned
#undef int
#undef short
#undef long

// DirectX includes
#include <D3DCommon.h>
#include <DXGI1_5.h>
#include <d3d11.h>
#include <d3d12.h>

POP_BASIC_TYPE_RESTRICTORS

// Abstract rendering interface definitions.
#include "ARIDefinitions.h"
#include "Definitions.h"
#include "States.h"
#include "Objects.h"

// Low-level interface.
#include "Adapter.h"

// The interface body.
#include "ARI.h"

