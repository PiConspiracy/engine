#include "Precompiled.h"
#include "Adapter.h"

namespace PiConspiracy
{
  Adapter::Adapter(IDXGIAdapter3* a_adapter, uint32 a_index) :
    m_adapterFactory(nullptr),
    m_displayAdapter(a_adapter),
    m_featureLevel(EFeatureLevel::D3D9_1),
    m_vendor(EGPUVendor::Unknown),
    m_adapterIndex(a_index),
    m_defaultAdaptor(false),
    m_hardwareDriver(true)
  {
    memset(&m_adapterDesc, 0, sizeof(DXGI_ADAPTER_DESC));
  }

  Adapter::~Adapter(void)
  {

  }

  const Device* Adapter::GetDevice(uint32 a_index) const
  {
    ENGINE_CRITICAL(a_index < m_devices.size());

    return m_devices[a_index];
  }

  const uint32 Adapter::GetDeviceNodeCount(void) const
  {
    return static_cast<uint32>(m_devices.size());
  }

#if ENGINE_DEBUG
  void Adapter::LogAdaptorDesc(void)
  {
    uint32 nodeCount = GetDeviceNodeCount();

    string convertedDesc = WCharToChar(m_adapterDesc.m_description);

    ENGINE_LOG("Adapter %u for %s:\n", m_adapterIndex, convertedDesc.c_str());
    ENGINE_LOG("\tDevice dedicated video memory: %uMB\n\tDevice dedicated system memory: %uMB\n\tDevice shared system memory: %uMB\n",
      static_cast<uint32>(m_adapterDesc.m_dedicatedVideoMemory) / (1024 * 1024),
      static_cast<uint32>(m_adapterDesc.m_dedicatedSystemMemory) / (1024 * 1024),
      static_cast<uint32>(m_adapterDesc.m_sharedSystemMemory) / (1024 * 1024));

    ENGINE_LOG("\tAadapter budget per node:\n");
    DXGI_QUERY_VIDEO_MEMORY_INFO memInfo;
    for (uint32 i = 0; i < nodeCount; ++i)
    {
      m_displayAdapter->QueryVideoMemoryInfo(nodeCount - 1, DXGI_MEMORY_SEGMENT_GROUP_LOCAL, &memInfo);
      ENGINE_LOG("\t\tNode %u budget: %lluMB\n", i, memInfo.Budget / (1024 * 1024));
    }

    if (m_hardwareDriver)
    {
      ENGINE_LOG("\tHardware adapter: true\n");
    }
    else
    {
      ENGINE_LOG("\tHardware adapter: false\n");
    }

    ENGINE_LOG("\tAdapter output count %u:\n", static_cast<uint32>(m_outputs.size()));
    for (uint32 i = 0; i < m_outputs.size(); ++i)
    {
      ENGINE_LOG("\tAdapter output %u:\n", i);
      const DXGI_OUTPUT_DESC& desc = reinterpret_cast<const DXGI_OUTPUT_DESC&>(m_outputs[i]->m_outputDesc);

      convertedDesc = WCharToChar(desc.DeviceName);
      ENGINE_LOG("\t\tOutput name: %s\n", convertedDesc.c_str());

      if (desc.AttachedToDesktop)
      {
        ENGINE_LOG("\t\tAttached: true\n");
      }
      else
      {
        ENGINE_LOG("\t\tAttached: false\n");
      }

      ENGINE_LOG("\t\tResolution: %u x %u\n", desc.DesktopCoordinates.right, desc.DesktopCoordinates.bottom);
    }
    ENGINE_LOG("\n");
  }
#endif
}
