#pragma once

namespace PiConspiracy
{
  struct Matrix4;
  struct Matrix3;
  struct Matrix2;
  struct Vector3;

  // Matrix 4
  // creates a tensor product matrix
  void Matrix4TensorProduct(Matrix4& ret, const Vector3& v);

  // creates a cross product matrix
  void Matrix4CrossProduct(Matrix4& ret, const Vector3& v);

  // creates a transpose of a matrix
  void Matrix4Transpose(Matrix4& ret, const Matrix4& m);

  // creates an inverse of a matrix
  void Matrix4Inverse(Matrix4& ret, const Matrix4& m);

  // creates per-component absolute value matrix
  void Matrix4Abs(Matrix4& ret, const Matrix4& m);

  // creates a translation matrix
  void Matrix4Translation(Matrix4& ret, float x, float y, float z);
  void Matrix4Translation(Matrix4& ret, const Vector3& v);

  // creates a rotation matrix (in radians)
  void Matrix4RotationX(Matrix4& ret, float angle);
  void Matrix4RotationY(Matrix4& ret, float angle);
  void Matrix4RotationZ(Matrix4& ret, float angle);
  void Matrix4RotationYawPitchRoll(Matrix4& ret, float yaw, float pitch, float roll);
  void Matrix4RotationYawPitchRoll(Matrix4& ret, const Vector3& v);
  void Matrix4RotationArbitrary(Matrix4& ret, const Vector3& axis, float angle);

  // create a scaling matrix
  Matrix4 Matrix4Scaling(float s);
  void Matrix4Scaling(Matrix4& ret, float s);
  void Matrix4Scaling(Matrix4& ret, float x, float y, float z);
  void Matrix4Scaling(Matrix4& ret, const Vector3& v);

  // create a left-handed view matrix
  void Matrix4LookAtLH(Matrix4& ret, const Vector3& eye, const Vector3& lookAt, const Vector3& up);

  // create a left-handed projection matrix
  void Matrix4OrthographicLH(Matrix4& ret, float width, float height, float zNear, float zFar);
  void Matrix4PerspectiveFovLH(Matrix4& ret, float fov, float aspectRatio, float zNear, float zFar);

  // create a right-handed view matrix 
  void Matrix4LookAtRH(Matrix4& ret, const Vector3& eye, const Vector3& at, const Vector3& up); 

  // create a right-handed projection matrix
  void Matrix4OrthographicRH(Matrix4& ret, float width, float height, float zNear, float zFar);
  void Matrix4PerspectiveFovRH(Matrix4& ret, float fov, float aspectRatio, float zNear, float zFar);

  // create a centered left-handed orthographic projeciton matrix
  void Matrix4OrthoOffCenterLH(Matrix4& ret, float l, float r, float b, float t, float zn, float zf);

  // create a centered right-handed orthographic projeciton matrix
  void Matrix4OrthoOffCenterRH(Matrix4& ret, float l, float r, float b, float t, float zn, float zf);

  // Matrix 3
  // creates a rotation matrix (in radians)
  void Matrix3RotationArbitrary(Matrix3& ret, const Vector3& axis, float angle);

  // Matrix 2
  Matrix2 MatrixTranspose(const Matrix2& m);
  Matrix2 MatrixInverse(const Matrix2& m);
}

