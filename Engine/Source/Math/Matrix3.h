#pragma once

namespace PiConspiracy
{
  struct Vector3;

  struct Matrix3
  {
    union
    {
      struct  
      {
        float m00, m01, m02,
              m10, m11, m12,
              m20, m21, m22;
      };
  
      float m[3][3];
      float v[9];
    };
  
    // Constructors
    Matrix3(void);
    Matrix3(const Matrix3& rhs);
    Matrix3(float a_m00, float a_m01, float a_m02,
            float a_m10, float a_m11, float a_m12,
            float a_m20, float a_m21, float a_m22);
  
    // Assignment operator, does not need to handle self-assignment
    Matrix3& operator=(const Matrix3& rhs);
  
    // Multiplying a Matrix3 with a Vector3 or a Point3
    Vector3 operator*(const Vector3& rhs) const;
  
    // Basic Matrix arithmetic operations
    Matrix3 operator+(const Matrix3& rhs) const;
    Matrix3 operator-(const Matrix3& rhs) const;
    Matrix3 operator*(const Matrix3& rhs) const;
    Matrix3& operator+=(const Matrix3& rhs);
    Matrix3& operator-=(const Matrix3& rhs);
    Matrix3& operator*=(const Matrix3& rhs);
    Matrix3 operator*(const float rhs) const;
    Matrix3 operator/(const float rhs) const;
    Matrix3& operator*=(const float rhs);
    Matrix3& operator/=(const float rhs);
  
    // Comparison operators 
    bool operator==(const Matrix3& rhs) const;
    bool operator!=(const Matrix3& rhs) const;

    // Linear Algebra
    void Transpose();
    void Inverse();

    // Setters
    void Zero(void);
    void Identity(void);
  
    // Other
    void Print(void) const;
  };
}

