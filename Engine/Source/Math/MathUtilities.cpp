#include "Precompiled.h"
#include "MathUtilities.h"

namespace PiConspiracy
{
  float ToRadians(float deg)
  {
    return deg * PI / 180.0f;
  }
  float ToDegrees(float rad)
  {
    return rad * 180.0f / PI;
  }

  uint16 CeilPower2(float val)
  {
    float pow = 1.0f;
    while(pow < val)
    {
      pow *= 2.0f;
    }

    return static_cast<uint16>(pow);
  }

  int32 Round(float val)
  {
    if(fabs(val - floor(val)) >= fabs(val - ceil(val)))
    {
      return static_cast<int32>(ceil(val));
    }
    else
    {
      return static_cast<int32>(floor(val));
    }
  }

  int32 Floor(float val)
  {
    return static_cast<int32>(floor(val));
  }
}