#pragma once

namespace PiConspiracy
{
  struct Vector2i;
  struct Vector4i;

  struct Vector3i
  {
    // Constructors
    Vector3i(void);
    Vector3i(const Vector2i& rhs, int32 a_z = 0);
    Vector3i(const Vector3i& rhs);
    Vector3i(int32 a_x, int32 a_y, int32 a_z);

    // Assignment operator, does not need to handle self assignment
    Vector3i& operator=(const Vector3i& rhs);

    // Unary negation operator, negates all components and returns a copy
    Vector3i  operator-(void) const;  

    // Math Operators
    Vector3i  operator+(const Vector3i& rhs) const;
    Vector3i  operator-(const Vector3i& rhs) const;
    //Vector3i  operator*(const Matrix4& rhs) const;
    //Vector3i  operator*(const Matrix3& rhs) const;
    Vector3i  operator*(const int32 rhs) const;
    Vector3i  operator/(const int32 rhs) const;
    Vector3i& operator+=(const Vector3i& rhs);
    Vector3i& operator-=(const Vector3i& rhs);
    Vector3i& operator*=(const int32 rhs);
    Vector3i& operator/=(const int32 rhs);

    // Comparison operators
    bool operator==(const Vector3i& rhs) const;
    bool operator!=(const Vector3i& rhs) const;
    bool operator< (const Vector3i & rhs) const;
    bool operator> (const Vector3i & rhs) const;
         
    // casting operators
    friend Vector3i operator*(int32 lhs, Vector3i const& rhs);

    // Linear Algebra
    float    Length(void) const;
    int32    LengthSq(void) const;
    void     Negate(void);
    Vector3  GetNormalized(void); //returns a normalized version of the vector
    int32    Dot(const Vector3i& rhs) const;
    Vector3i Cross(const Vector3i& rhs) const;

    // Setters
    void Zero(void);

    // Helpers
    void     Print(void) const;
    Vector3i Min(const Vector3i& rhs);
    Vector3i Max(const Vector3i& rhs);

    Vector4i Swizzle(SWIZZLE a, SWIZZLE b, SWIZZLE c, SWIZZLE d);
    Vector3i Swizzle(SWIZZLE a, SWIZZLE b, SWIZZLE c);
    Vector2i Swizzle(SWIZZLE a, SWIZZLE b);

    Vector3 ToF();
  public:
    union
    {
      struct 
      {
        int32 x, y, z;
      };

      int32 v[3];
    };
  };
}

