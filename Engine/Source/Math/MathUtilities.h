#pragma once

namespace PiConspiracy
{
  // Rounding
  uint16 CeilPower2(float val);
  int32 Round(float val);
  int32 Floor(float val);

  // Conversion
  float ToRadians(float deg);
  float ToDegrees(float rad);  
}
