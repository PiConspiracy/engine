#pragma once

namespace PiConspiracy
{
  struct Vector2;
  struct Vector3;
  struct Vector4i;
  struct Matrix4;

  struct Vector4
  {
    // Constructors
    Vector4(void);
    Vector4(const Vector2& rhs, float a_z = 0.0f, float a_w = 1.0f);
    Vector4(const Vector3& rhs, float a_w = 1.0f);
    Vector4(const Vector4& rhs);
    Vector4(float a_x, float a_y, float a_z, float a_w);

    // Assignment operator, does not need to handle self assignment
    Vector4& operator=(const Vector4& rhs);

    // Unary negation operator, negates all components and returns a copy
    Vector4  operator-(void) const;  

    // Math Operators
    Vector4  operator+(const Vector4& rhs) const;
    Vector4  operator-(const Vector4& rhs) const;
    Vector4  operator*(const Matrix4& rhs) const; 
    Vector4  operator*(const Vector4& rhs) const;
    Vector4  operator*(const float rhs) const;
    Vector4  operator/(const float rhs) const;
    Vector4& operator+=(const Vector4& rhs);
    Vector4& operator-=(const Vector4& rhs);
    Vector4& operator*=(const float rhs);
    Vector4& operator/=(const float rhs);

    // Comparison operators
    bool operator==(const Vector4& rhs) const;
    bool operator!=(const Vector4& rhs) const;
    bool operator< (const Vector4 & rhs) const;
    bool operator> (const Vector4 & rhs) const;
         
    // casting operators
    friend Vector4 operator*(float lhs, Vector4 const & rhs);

    // Linear Algebra
    float   Length(void) const;
    float   LengthSq(void) const;
    void    Negate(void);
    void    Maximize(void);
    void    Minimize(void);
    Vector4 GetNormalized(void); //returns a normalized version of the vector
    void    Normalize(void);
    float   Dot(const Vector4& rhs) const;
    Vector4 Cross(const Vector4& rhs) const;
    bool    IsZero(void);

    //Setter
    void Zero(void);

    //Helpers
    void    Print(void) const;
    Vector4 Min(const Vector4& rhs);
    Vector4 Max(const Vector4& rhs);

    Vector4 Swizzle(SWIZZLE a, SWIZZLE b, SWIZZLE c, SWIZZLE d);
    Vector3 Swizzle(SWIZZLE a, SWIZZLE b, SWIZZLE c);
    Vector2 Swizzle(SWIZZLE a, SWIZZLE b);

    Vector4i ToI(void);
  public:
    union
    {
      struct 
      {
        float x, y, z, w;
      };

      float v[4];
    };
  };  
}

