#pragma once

namespace PiConspiracy
{
  struct Vector2i;

  struct Vector2
  {
    // Constructor
    Vector2(void);  
    Vector2(const Vector2& rhs);
    Vector2(float a_x, float a_y);
  
    // Assignment operator, does not need to handle self assignment
    Vector2& operator=(const Vector2& rhs);
  
    // Unary negation operator, negates all components and returns a copy
    Vector2  operator-(void) const;  
    
    // Math Operators
    Vector2  operator+(const Vector2& rhs) const;
    Vector2  operator-(const Vector2& rhs) const;
    Vector2  operator*(const float rhs) const;
    Vector2  operator/(const float rhs) const;
    Vector2& operator+=(const Vector2& rhs);
    Vector2& operator-=(const Vector2& rhs);
    Vector2& operator*=(const float rhs);
    Vector2& operator/=(const float rhs);
  
    // Comparison operators
    bool operator==(const Vector2& rhs) const;
    bool operator!=(const Vector2& rhs) const;
    bool operator< (const Vector2 & rhs) const;
    bool operator> (const Vector2 & rhs) const;
         
    // Casting operators
    friend Vector2 operator*(float lhs, Vector2 const & rhs);

    // Linear Algebra
    float   Length(void) const;
    float   LengthSq(void) const;
    void    Negate(void);
    Vector2 GetNormalized(void);
    void    Normalize(void);
    float   Dot(const Vector2& rhs) const;
    float   Cross(const Vector2& rhs) const;
    bool    IsZero(void);

    // set all components to zero
    void Zero(void);

    // Helpers
    void    Print(void) const;
    Vector2 Min(const Vector2& rhs);
    Vector2 Max(const Vector2& rhs);

    Vector4 Swizzle(SWIZZLE a, SWIZZLE b, SWIZZLE c, SWIZZLE d);
    Vector3 Swizzle(SWIZZLE a, SWIZZLE b, SWIZZLE c);
    Vector2 Swizzle(SWIZZLE a, SWIZZLE b);

    Vector2i ToI(void);
    
  public:
    union
    {
      struct 
      {
        float x, y;
      };

      float v[2];
    };
  };
}
