#include "Precompiled.h"
#include "Matrix4.h"

namespace PiConspiracy
{
  Matrix4::Matrix4(void) : m00(0.0f), m01(0.0f), m02(0.0f), m03(0.0f),
                           m10(0.0f), m11(0.0f), m12(0.0f), m13(0.0f),
                           m20(0.0f), m21(0.0f), m22(0.0f), m23(0.0f),
                           m30(0.0f), m31(0.0f), m32(0.0f), m33(0.0f)
  {
  }

  Matrix4::Matrix4(const Matrix4& rhs) : m00(rhs.m00), m01(rhs.m01), m02(rhs.m02), m03(rhs.m03),
                                         m10(rhs.m10), m11(rhs.m11), m12(rhs.m12), m13(rhs.m13),
                                         m20(rhs.m20), m21(rhs.m21), m22(rhs.m22), m23(rhs.m23),
                                         m30(rhs.m30), m31(rhs.m31), m32(rhs.m32), m33(rhs.m33)
  {
  }

  Matrix4::Matrix4(float a_m00, float a_m01, float a_m02, float a_m03,
                   float a_m10, float a_m11, float a_m12, float a_m13,
                   float a_m20, float a_m21, float a_m22, float a_m23,
                   float a_m30, float a_m31, float a_m32, float a_m33) : m00(a_m00), m01(a_m01), m02(a_m02), m03(a_m03),
                                                                     m10(a_m10), m11(a_m11), m12(a_m12), m13(a_m13),
                                                                     m20(a_m20), m21(a_m21), m22(a_m22), m23(a_m23),
                                                                     m30(a_m30), m31(a_m31), m32(a_m32), m33(a_m33)
  {
  }

  //assignment
  Matrix4& Matrix4::operator=(const Matrix4& rhs)
  {
    if(this != &rhs)
    {
      m00 = rhs.m00; m01 = rhs.m01; m02 = rhs.m02; m03 = rhs.m03;
      m10 = rhs.m10; m11 = rhs.m11; m12 = rhs.m12; m13 = rhs.m13;
      m20 = rhs.m20; m21 = rhs.m21; m22 = rhs.m22; m23 = rhs.m23;
      m30 = rhs.m30; m31 = rhs.m31; m32 = rhs.m32; m33 = rhs.m33;
    }
    return *this;
  }

  void Matrix4::Transpose(void)
  {
    std::swap(m10, m01); std::swap(m20, m02); std::swap(m30, m03);
    std::swap(m21, m12); std::swap(m31, m13);
    std::swap(m32, m23);
  }

  void Matrix4::Inverse(void)
  {
    float const *original = v;		// cast the matrix as a flat array
    float temp[12];				// used for intermediate calculations
    float src[16];				// transpose of original matrix inverse

    // transpose the original matrix
    for( int32 i =  0; i < 4; i++ )
    {
      src[i]      = original[i * 4];
      src[i + 4]  = original[i * 4 + 1];
      src[i + 8]  = original[i * 4 + 2];
      src[i + 12] = original[i * 4 + 3];
    }

    // calculate pairs for the first 8 elements (cofactors)
    temp[0]  = src[10] * src[15];
    temp[1]  = src[11] * src[14];
    temp[2]  = src[9]  * src[15];
    temp[3]  = src[11] * src[13];
    temp[4]  = src[9]  * src[14];
    temp[5]  = src[10] * src[13];
    temp[6]  = src[8]  * src[15];
    temp[7]  = src[11] * src[12];
    temp[8]  = src[8]  * src[14];
    temp[9]  = src[10] * src[12];
    temp[10] = src[8]  * src[13];
    temp[11] = src[9]  * src[12];

    // calculate first 8 elements (cofactors)
    v[0]  = temp[0] * src[5] + temp[3] * src[6] + temp[4]  * src[7];
    v[0] -= temp[1] * src[5] + temp[2] * src[6] + temp[5]  * src[7];
    v[1]  = temp[1] * src[4] + temp[6] * src[6] + temp[9]  * src[7];
    v[1] -= temp[0] * src[4] + temp[7] * src[6] + temp[8]  * src[7];
    v[2]  = temp[2] * src[4] + temp[7] * src[5] + temp[10] * src[7];
    v[2] -= temp[3] * src[4] + temp[6] * src[5] + temp[11] * src[7];
    v[3]  = temp[5] * src[4] + temp[8] * src[5] + temp[11] * src[6];
    v[3] -= temp[4] * src[4] + temp[9] * src[5] + temp[10] * src[6];
    v[4]  = temp[1] * src[1] + temp[2] * src[2] + temp[5]  * src[3];
    v[4] -= temp[0] * src[1] + temp[3] * src[2] + temp[4]  * src[3];
    v[5]  = temp[0] * src[0] + temp[7] * src[2] + temp[8]  * src[3];
    v[5] -= temp[1] * src[0] + temp[6] * src[2] + temp[9]  * src[3];
    v[6]  = temp[3] * src[0] + temp[6] * src[1] + temp[11] * src[3];
    v[6] -= temp[2] * src[0] + temp[7] * src[1] + temp[10] * src[3];
    v[7]  = temp[4] * src[0] + temp[9] * src[1] + temp[10] * src[2];
    v[7] -= temp[5] * src[0] + temp[8] * src[1] + temp[11] * src[2];

    // calculate pairs for second 8 elements (cofactors)
    temp[0] = src[2] * src[7];
    temp[1] = src[3] * src[6];
    temp[2] = src[1] * src[7];
    temp[3] = src[3] * src[5];
    temp[4] = src[1] * src[6];
    temp[5] = src[2] * src[5];
    temp[6] = src[0] * src[7];
    temp[7] = src[3] * src[4];
    temp[8] = src[0] * src[6];
    temp[9] = src[2] * src[4];
    temp[10] = src[0] * src[5];
    temp[11] = src[1] * src[4];

    // calculate second 8 elements (cofactors)
    v[8]   = temp[0]  * src[13] + temp[3]  * src[14] + temp[4]  * src[15];
    v[8]  -= temp[1]  * src[13] + temp[2]  * src[14] + temp[5]  * src[15];
    v[9]   = temp[1]  * src[12] + temp[6]  * src[14] + temp[9]  * src[15];
    v[9]  -= temp[0]  * src[12] + temp[7]  * src[14] + temp[8]  * src[15];
    v[10]  = temp[2]  * src[12] + temp[7]  * src[13] + temp[10] * src[15];
    v[10] -= temp[3]  * src[12] + temp[6]  * src[13] + temp[11] * src[15];
    v[11]  = temp[5]  * src[12] + temp[8]  * src[13] + temp[11] * src[14];
    v[11] -= temp[4]  * src[12] + temp[9]  * src[13] + temp[10] * src[14];
    v[12]  = temp[2]  * src[10] + temp[5]  * src[11] + temp[1]  * src[9];
    v[12] -= temp[4]  * src[11] + temp[0]  * src[9]  + temp[3]  * src[10];
    v[13]  = temp[8]  * src[11] + temp[0]  * src[8]  + temp[7]  * src[10];
    v[13] -= temp[6]  * src[10] + temp[9]  * src[11] + temp[1]  * src[8];
    v[14]  = temp[6]  * src[9]  + temp[11] * src[11] + temp[3]  * src[8];
    v[14] -= temp[10] * src[11] + temp[2]  * src[8]  + temp[7]  * src[9];
    v[15]  = temp[10] * src[10] + temp[4]  * src[8]  + temp[9]  * src[9];
    v[15] -= temp[8]  * src[9]  + temp[11] * src[10] + temp[5]  * src[8];

    // calculate determinant
    float det = src[0] * v[0] + src[1] * v[1] + src[2] * v[2] + src[3] * v[3];

    if(det == 0) 
    {
      return;
    }

    det =  1.0f / det;
    *this *= det;
  }

  //multiplication by vector / point
  Vector4 Matrix4::operator*(const Vector4& rhs) const
  {
    return Vector4(m00 * rhs.x + m01 * rhs.y + m02 * rhs.z + m03 *rhs.w,
                   m10 * rhs.x + m11 * rhs.y + m12 * rhs.z + m13 *rhs.w,
                   m20 * rhs.x + m21 * rhs.y + m22 * rhs.z + m23 *rhs.w,
                   m30 * rhs.x + m31 * rhs.y + m32 * rhs.z + m33 *rhs.w);
  }

  //addition
  Matrix4 Matrix4::operator+(const Matrix4& rhs) const
  {
    return Matrix4(m00 + rhs.m00, m01 + rhs.m01, m02 + rhs.m02, m03 + rhs.m03,
                   m10 + rhs.m10, m11 + rhs.m11, m12 + rhs.m12, m13 + rhs.m13,
                   m20 + rhs.m20, m21 + rhs.m21, m22 + rhs.m22, m23 + rhs.m23,
                   m30 + rhs.m30, m31 + rhs.m31, m32 + rhs.m32, m33 + rhs.m33);
  }

  //subtraction
  Matrix4 Matrix4::operator-(const Matrix4& rhs) const
  {
    return Matrix4(m00 - rhs.m00, m01 - rhs.m01, m02 - rhs.m02, m03 - rhs.m03,
                   m10 - rhs.m10, m11 - rhs.m11, m12 - rhs.m12, m13 - rhs.m13,
                   m20 - rhs.m20, m21 - rhs.m21, m22 - rhs.m22, m23 - rhs.m23,
                   m30 - rhs.m30, m31 - rhs.m31, m32 - rhs.m32, m33 - rhs.m33);
  }

  //multiplication
  Matrix4 Matrix4::operator*(const Matrix4& rhs) const
  {
    return Matrix4(m00 * rhs.m00 + m01 * rhs.m10 + m02 * rhs.m20 + m03 * rhs.m30, m00 * rhs.m01 + m01 * rhs.m11 + m02 * rhs.m21 + m03 * rhs.m31, m00 * rhs.m02 + m01 * rhs.m12 + m02 * rhs.m22 + m03 * rhs.m32, m00 * rhs.m03 + m01 * rhs.m13 + m02 * rhs.m23 + m03 * rhs.m33,
                   m10 * rhs.m00 + m11 * rhs.m10 + m12 * rhs.m20 + m13 * rhs.m30, m10 * rhs.m01 + m11 * rhs.m11 + m12 * rhs.m21 + m13 * rhs.m31, m10 * rhs.m02 + m11 * rhs.m12 + m12 * rhs.m22 + m13 * rhs.m32, m10 * rhs.m03 + m11 * rhs.m13 + m12 * rhs.m23 + m13 * rhs.m33,
                   m20 * rhs.m00 + m21 * rhs.m10 + m22 * rhs.m20 + m23 * rhs.m30, m20 * rhs.m01 + m21 * rhs.m11 + m22 * rhs.m21 + m23 * rhs.m31, m20 * rhs.m02 + m21 * rhs.m12 + m22 * rhs.m22 + m23 * rhs.m32, m20 * rhs.m03 + m21 * rhs.m13 + m22 * rhs.m23 + m23 * rhs.m33,
                   m30 * rhs.m00 + m31 * rhs.m10 + m32 * rhs.m20 + m33 * rhs.m30, m30 * rhs.m01 + m31 * rhs.m11 + m32 * rhs.m21 + m33 * rhs.m31, m30 * rhs.m02 + m31 * rhs.m12 + m32 * rhs.m22 + m33 * rhs.m32, m30 * rhs.m03 + m31 * rhs.m13 + m32 * rhs.m23 + m33 * rhs.m33);
  }

  //addition assignment
  Matrix4& Matrix4::operator+=(const Matrix4& rhs)
  {
    m00 += rhs.m00; m01 += rhs.m01; m02 += rhs.m02; m03 += rhs.m03;
    m10 += rhs.m10; m11 += rhs.m11; m12 += rhs.m12; m13 += rhs.m13;
    m20 += rhs.m20; m21 += rhs.m21; m22 += rhs.m22; m23 += rhs.m23;
    m30 += rhs.m30; m31 += rhs.m31; m32 += rhs.m32; m33 += rhs.m33;
    return *this;
  }

  //subtraction assignment
  Matrix4& Matrix4::operator-=(const Matrix4& rhs)
  {
    m00 -= rhs.m00; m01 -= rhs.m01; m02 -= rhs.m02; m03 -= rhs.m03;
    m10 -= rhs.m10; m11 -= rhs.m11; m12 -= rhs.m12; m13 -= rhs.m13;
    m20 -= rhs.m20; m21 -= rhs.m21; m22 -= rhs.m22; m23 -= rhs.m23;
    m30 -= rhs.m30; m31 -= rhs.m31; m32 -= rhs.m32; m33 -= rhs.m33;
    return *this;
  }

  //mulitplication assignment
  Matrix4& Matrix4::operator*=(const Matrix4& rhs)
  {
    (*this) = (*this) * rhs;
    return *this;
  }

  //scaling
  Matrix4 Matrix4::operator*(const float rhs) const
  {
    return Matrix4(m00 * rhs, m01 * rhs, m02 * rhs, m03 * rhs,
                   m10 * rhs, m11 * rhs, m12 * rhs, m13 * rhs,
                   m20 * rhs, m21 * rhs, m22 * rhs, m23 * rhs,
                   m30 * rhs, m31 * rhs, m32 * rhs, m33 * rhs);
  }

  //dividing scalar
  Matrix4 Matrix4::operator/(const float rhs) const
  {
    return Matrix4(m00 / rhs, m01 / rhs, m02 / rhs, m03 / rhs,
                   m10 / rhs, m11 / rhs, m12 / rhs, m13 / rhs,
                   m20 / rhs, m21 / rhs, m22 / rhs, m23 / rhs,
                   m30 / rhs, m31 / rhs, m32 / rhs, m33 / rhs);
  }

  //scale assignment
  Matrix4& Matrix4::operator*=(const float rhs)
  {
    m00 *= rhs; m01 *= rhs; m02 *= rhs; m03 *= rhs;
    m10 *= rhs; m11 *= rhs; m12 *= rhs; m13 *= rhs;
    m20 *= rhs; m21 *= rhs; m22 *= rhs; m23 *= rhs;
    m30 *= rhs; m31 *= rhs; m32 *= rhs; m33 *= rhs;
    return *this;
  }

  //dividing scale assignment
  Matrix4& Matrix4::operator/=(const float rhs)
  {
    m00 /= rhs; m01 /= rhs; m02 /= rhs; m03 /= rhs;
    m10 /= rhs; m11 /= rhs; m12 /= rhs; m13 /= rhs;
    m20 /= rhs; m21 /= rhs; m22 /= rhs; m23 /= rhs;
    m30 /= rhs; m31 /= rhs; m32 /= rhs; m33 /= rhs;
    return *this;
  }

  //equality
  bool Matrix4::operator==(const Matrix4& rhs) const
  {
    return (m00 - rhs.m00 < EPSILON && m00 - rhs.m00 > -EPSILON) && (m01 - rhs.m01 < EPSILON && m01 - rhs.m01 > -EPSILON) && (m02 - rhs.m02 < EPSILON && m02 - rhs.m02 > -EPSILON) && (m03 - rhs.m03 < EPSILON && m03 - rhs.m03 > -EPSILON) &&
           (m10 - rhs.m10 < EPSILON && m10 - rhs.m10 > -EPSILON) && (m11 - rhs.m11 < EPSILON && m11 - rhs.m11 > -EPSILON) && (m12 - rhs.m12 < EPSILON && m12 - rhs.m12 > -EPSILON) && (m13 - rhs.m13 < EPSILON && m13 - rhs.m13 > -EPSILON) &&
           (m20 - rhs.m20 < EPSILON && m20 - rhs.m20 > -EPSILON) && (m21 - rhs.m21 < EPSILON && m21 - rhs.m21 > -EPSILON) && (m22 - rhs.m22 < EPSILON && m22 - rhs.m22 > -EPSILON) && (m23 - rhs.m23 < EPSILON && m23 - rhs.m23 > -EPSILON) &&
           (m30 - rhs.m30 < EPSILON && m30 - rhs.m30 > -EPSILON) && (m31 - rhs.m31 < EPSILON && m31 - rhs.m31 > -EPSILON) && (m32 - rhs.m32 < EPSILON && m32 - rhs.m32 > -EPSILON) && (m33 - rhs.m33 < EPSILON && m33 - rhs.m33 > -EPSILON);
  }

  //inequality
  bool Matrix4::operator!=(const Matrix4& rhs) const
  {
    return (m00 - rhs.m00 >= EPSILON || m00 - rhs.m00 <= -EPSILON) || (m01 - rhs.m01 >= EPSILON || m01 - rhs.m01 <= -EPSILON) || (m02 - rhs.m02 >= EPSILON || m02 - rhs.m02 <= -EPSILON) || (m03 - rhs.m03 >= EPSILON || m03 - rhs.m03 <= -EPSILON) ||
           (m10 - rhs.m10 >= EPSILON || m10 - rhs.m10 <= -EPSILON) || (m11 - rhs.m11 >= EPSILON || m11 - rhs.m11 <= -EPSILON) || (m12 - rhs.m12 >= EPSILON || m12 - rhs.m12 <= -EPSILON) || (m13 - rhs.m13 >= EPSILON || m13 - rhs.m13 <= -EPSILON) ||
           (m20 - rhs.m20 >= EPSILON || m20 - rhs.m20 <= -EPSILON) || (m21 - rhs.m21 >= EPSILON || m21 - rhs.m21 <= -EPSILON) || (m22 - rhs.m22 >= EPSILON || m22 - rhs.m22 <= -EPSILON) || (m23 - rhs.m23 >= EPSILON || m23 - rhs.m23 <= -EPSILON) ||
           (m30 - rhs.m30 >= EPSILON || m30 - rhs.m30 <= -EPSILON) || (m31 - rhs.m31 >= EPSILON || m31 - rhs.m31 <= -EPSILON) || (m32 - rhs.m32 >= EPSILON || m32 - rhs.m32 <= -EPSILON) || (m33 - rhs.m33 >= EPSILON || m33 - rhs.m33 <= -EPSILON);
  }

  void Matrix4::Zero(void)
  {
    m00 = m01 = m02 = m03 = 
    m10 = m11 = m12 = m13 =
    m20 = m21 = m22 = m23 =
    m30 = m31 = m32 = m33 = 0.0f;
  }

  void Matrix4::Identity(void)
  {
          m01 = m02 = m03 = 
          m10 =       m12 = m13 =
          m20 = m21 =       m23 =
          m30 = m31 = m32 =       0.0f;

          m00 = m11 = m22 = m33 = 1.0f;
  }

  void Matrix4::Print(void) const
  {
    ENGINE_LOG("--------------------------\n");
    ENGINE_LOG("%5.3f %5.3f %5.3f %5.3f\n", m00, m01, m02, m03);
    ENGINE_LOG("%5.3f %5.3f %5.3f %5.3f\n", m10, m11, m12, m13);
    ENGINE_LOG("%5.3f %5.3f %5.3f %5.3f\n", m20, m21, m22, m23);
    ENGINE_LOG("%5.3f %5.3f %5.3f %5.3f\n", m30, m31, m32, m33);
    ENGINE_LOG("--------------------------\n");
  }
}