#include "Precompiled.h"
#include "Vector4i.h"

namespace PiConspiracy
{
  Vector4i::Vector4i(void) : 
    x(0), y(0), z(0), w(0)
  {
  }

  Vector4i::Vector4i(const Vector2i& rhs, int32 a_z, int32 a_w) : 
    x(rhs.x), y(rhs.y), z(a_z), w(a_w)
  {
  }

  Vector4i::Vector4i( const Vector3i& rhs, int32 a_w) : 
    x(rhs.x), y(rhs.y), z(rhs.z), w(a_w)
  {
  }

  Vector4i::Vector4i(const Vector4i& rhs) : 
    x(rhs.x), y(rhs.y), z(rhs.z), w(rhs.w)
  {
  }

  Vector4i::Vector4i(int32 a_x, int32 a_y, int32 a_z, int32 a_w) : 
    x(a_x), y(a_y), z(a_z), w(a_w)
  {
  }

  Vector4i& Vector4i::operator=(const Vector4i& rhs)
  {
    if(this != &rhs)
    {
      x = rhs.x;
      y = rhs.y;
      z = rhs.z;
      w = rhs.w;
    }
    return *this;
  }

  Vector4i Vector4i::operator-(void) const
  {
    return Vector4i(-x, -y, -z, -w);
  }

  Vector4i Vector4i::operator+(const Vector4i& rhs) const
  {
    return Vector4i( x + rhs.x,
                    y + rhs.y, 
                    z + rhs.z,
                    w + rhs.w);
  }

  Vector4i Vector4i::operator-(const Vector4i& rhs) const
  {
    return Vector4i( x - rhs.x,
                    y - rhs.y, 
                    z - rhs.z,
                    w - rhs.w);
  }

  Vector4i Vector4i::operator*(const int32 rhs) const
  {
    return Vector4i( x * rhs,
                    y * rhs, 
                    z * rhs,
                    w * rhs);
  }

  Vector4i Vector4i::operator*(const Vector4i& rhs) const
  {
    return Vector4i( x * rhs.x,
                    y * rhs.y, 
                    z * rhs.z, 
                    w * rhs.w);
  }

  Vector4i Vector4i::operator/(const int32 rhs) const
  {
    return Vector4i( x / rhs,
                    y / rhs, 
                    z / rhs, 
                    w / rhs);
  }


  Vector4i& Vector4i::operator+=(const Vector4i& rhs)
  {
    x += rhs.x;
    y += rhs.y; 
    z += rhs.z;
    w += rhs.w;
    return *this;
  }

  Vector4i& Vector4i::operator-=(const Vector4i& rhs)
  {
    x -= rhs.x;
    y -= rhs.y; 
    z -= rhs.z;
    w -= rhs.w;
    return *this;
  }

  Vector4i& Vector4i::operator*=(const int32 rhs)
  {
    x *= rhs;
    y *= rhs; 
    z *= rhs;
    w *= rhs;
    return *this;
  }

  Vector4i& Vector4i::operator/=(const int32 rhs)
  {
    if(rhs == 0)
    {
      Zero();
    }
    else
    {
      x /= rhs;
      y /= rhs; 
      z /= rhs;
      w /= rhs;
    }
    return *this;
  }

  bool Vector4i::operator==(const Vector4i& rhs) const
  {
    return  abs(x - rhs.x) <= EPSILON &&
            abs(y - rhs.y) <= EPSILON &&
            abs(z - rhs.z) <= EPSILON &&
            abs(w - rhs.w) <= EPSILON;
  }

  bool Vector4i::operator!=(const Vector4i& rhs) const
  {
    return  abs(x - rhs.x) > EPSILON ||
            abs(y - rhs.y) > EPSILON ||
            abs(z - rhs.z) > EPSILON ||
            abs(w - rhs.w) > EPSILON;
  }

  bool Vector4i::operator < (const Vector4i & rhs) const
  {
    return  (x < rhs.x) &&
            (y < rhs.y) && 
            (z < rhs.z) &&
            (w < rhs.w);
  }

  bool Vector4i::operator > (const Vector4i & rhs) const
  {
    return  (x > rhs.x) &&
            (y > rhs.y) && 
            (z > rhs.z) &&
            (w > rhs.w);
  }

  Vector4i operator*(int32 f, Vector4i const & v)
  {
    return Vector4i(v.x * f, v.y * f, v.z * f, v.w);
  }

  float Vector4i::Length(void) const
  {
    return ::sqrt(static_cast<float>(LengthSq()));
  }

  int32 Vector4i::LengthSq(void) const
  {
    return x * x + y * y + z * z;
  }

  void Vector4i::Negate(void)
  {
    x = -x; y = -y; z = -z; w = -w;
  }

  Vector4 Vector4i::GetNormalized(void)
  {
    Vector4 floats = ToF();
    floats.Normalize();
    return floats;
  }

  int32 Vector4i::Dot(const Vector4i& rhs) const
  {
    return (x * rhs.x) + (y * rhs.y) + (z * rhs.z);// + (w * rhs.w);
  }

  Vector4i Vector4i::Cross(const Vector4i& rhs) const
  {
    return Vector4i(y * rhs.z - z * rhs.y,
                    z * rhs.x - x * rhs.z,
                    x * rhs.y - y * rhs.x, 
                    1);
  }

  void Vector4i::Zero(void)
  {
    x = y = z = 0;
  }

  void Vector4i::Print(void) const
  {
    ENGINE_LOG("%5.3f, %5.3f, %5.3f, %5.3f\n",x,y,z,w);
  }

  Vector4i Vector4i::Min(const Vector4i& rhs)
  {
    return Vector4i(std::min(x, rhs.x), std::min(y, rhs.y), std::min(z, rhs.z), std::min(w, rhs.w));
  }

  Vector4i Vector4i::Max(const Vector4i& rhs)
  {
    return Vector4i(std::max(x, rhs.x), std::max(y, rhs.y), std::max(z, rhs.z), std::max(w, rhs.w));
  }

  Vector4i Vector4i::Swizzle(SWIZZLE a, SWIZZLE b, SWIZZLE c, SWIZZLE d)
  {
#ifdef ENGINE_DEBUG
    if (a > 2 || b > 2 || c > 2 || d > 2)
    {
      ENGINE_WARNING("Swizzling out of bounds.");
      return Vector4i();
    }
#endif

    return Vector4i(v[a], v[b], v[c], v[d]);
  }

  Vector3i Vector4i::Swizzle(SWIZZLE a, SWIZZLE b, SWIZZLE c)
  {
#ifdef ENGINE_DEBUG
    if (a > 2 || b > 2 || c > 2)
    {
      ENGINE_WARNING("Swizzling out of bounds.");
      return Vector3i();
    }
#endif

    return Vector3i(v[a], v[b], v[c]);
  }

  Vector2i Vector4i::Swizzle(SWIZZLE a, SWIZZLE b)
  {
#ifdef ENGINE_DEBUG
    if (a > 2 || b > 2)
    {
      ENGINE_WARNING("Swizzling out of bounds.");
      return Vector2i();
    }
#endif

    return Vector2i(v[a], v[b]);
  }

  Vector4 Vector4i::ToF()
  {
    return Vector4(static_cast<float>(x), static_cast<float>(y), static_cast<float>(z), static_cast<float>(w));
  }
}
