#pragma once

namespace PiConspiracy
{
  // Floating 
  float RandGauss01(); //[0,1]
  float RandGauss11(); //[-1,1]
  float RandGauss33(); //[-3,3]

  float Rand01(); //[0,1]
  float Rand11(); //[-1,1]
  float RandBetween(float min, float max, float base);
  float RandFloat(float mini = 0, float maxi = 1);

  // integer 
  int32 RandBetween(int32 min, int32 max);
  int32 RandInt32(int32 mini, int32 maxi);
}
