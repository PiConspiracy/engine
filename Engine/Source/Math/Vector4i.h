#pragma once

namespace PiConspiracy
{
  struct Vector2i;
  struct Vector3i;

  struct Vector4i
  {
    // Constructors
    Vector4i(void);
    Vector4i(const Vector2i& rhs, int32 a_z = 0, int32 a_w = 1);
    Vector4i(const Vector3i& rhs, int32 a_w = 1);
    Vector4i(const Vector4i& rhs);
    Vector4i(int32 a_x, int32 a_y, int32 a_z, int32 a_w);

    // Assignment operator, does not need to handle self assignment
    Vector4i& operator=(const Vector4i& rhs);

    // Unary negation operator, negates all components and returns a copy
    Vector4i  operator-(void) const;  

    // Math Operators
    Vector4i  operator+(const Vector4i& rhs) const;
    Vector4i  operator-(const Vector4i& rhs) const;
    Vector4i  operator*(const Vector4i& rhs) const;
    Vector4i  operator*(const int32 rhs) const;
    Vector4i  operator/(const int32 rhs) const;
    Vector4i& operator+=(const Vector4i& rhs);
    Vector4i& operator-=(const Vector4i& rhs);
    Vector4i& operator*=(const int32 rhs);
    Vector4i& operator/=(const int32 rhs);

    // Comparison operators
    bool operator==(const Vector4i& rhs) const;
    bool operator!=(const Vector4i& rhs) const;
    bool operator< (const Vector4i& rhs) const;
    bool operator> (const Vector4i& rhs) const;
         
    // casting operators
    friend Vector4i operator*(int32 lhs, Vector4i const& rhs);

    // Linear Algebra
    float    Length(void) const;
    int32    LengthSq(void) const;
    void     Negate(void);
    Vector4  GetNormalized(void); //returns a normalized version of the vector
    int32    Dot(const Vector4i& rhs) const;
    Vector4i Cross(const Vector4i& rhs) const;

    //Setter
    void Zero(void);

    //Helpers
    void    Print(void) const;
    Vector4i Min(const Vector4i& rhs);
    Vector4i Max(const Vector4i& rhs);

    Vector4i Swizzle(SWIZZLE a, SWIZZLE b, SWIZZLE c, SWIZZLE d);
    Vector3i Swizzle(SWIZZLE a, SWIZZLE b, SWIZZLE c);
    Vector2i Swizzle(SWIZZLE a, SWIZZLE b);

    Vector4 ToF();
  public:
    union
    {
      struct 
      {
        int32 x, y, z, w;
      };

      int32 v[4];
    };
  };  
}

