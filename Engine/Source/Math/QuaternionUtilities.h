#pragma once

namespace PiConspiracy
{
	//TODO
	//build quat from 2 vec, 152 quat.inl

	// Build a quaternion from an angle and a normalized axis.
	Quaternion QuatFrmAngleAxis(float a_angle,const Vector3& a_axis);
	// Converts a pure rotation 3 * 3 matrix to a quaternion.
	Quaternion QuatFromMatrix3(Matrix3& a_mat);

	// Converts a pure rotation 4 * 4 matrix to a quaternion.
	Quaternion QuatFromMatrix4(Matrix4& a_mat);



	/// Spherical linear interpolation of two quaternions.
	/// The interpolation is oriented and the rotation is performed at constant speed.
	/// For short path spherical linear interpolation, use the slerp function.
	///
	/// @param x A quaternion
	/// @param y A quaternion
	/// @param a Interpolation factor. The interpolation is defined beyond the range [0, 1].
	/// @tparam T Floating-point scalar types.

	Quaternion Mix(Quaternion& a_x, Quaternion& a_y, float a_InterpolationFactor);

	// Linear interpolation of two quaternions.
	// The interpolation is oriented.
	// @param x A quaternion
	// @param y A quaternion
	// @param a Interpolation factor. The interpolation is defined in the range [0, 1].
	Quaternion Lerp2Quat(Quaternion& a_x,Quaternion& a_y,float a_InterpolationFactor);

	// Spherical linear interpolation of two quaternions.
	// The interpolation always take the short path and the rotation is performed at constant speed.
	//
	// @param x A quaternion
	// @param y A quaternion
	// @param a Interpolation factor. The interpolation is defined beyond the range [0, 1].
	// @tparam T Floating-point scalar types.
	Quaternion Slerp2Quat(Quaternion& a_x, Quaternion& a_y, float a_InterpolationFactor);

	float Compute_Mix(float a_x,float a_y,float a_A);

	/// Rotates a quaternion from a vector of 3 components axis and an angle.
	///
	/// @param q Source orientation
	/// @param angle Angle expressed in radians.
	/// @param axis Axis of the rotation
	/// @tparam T Floating-point scalar types.
	Quaternion RotateQuat(Quaternion& a_Q, float angle, Vector3& a_Axis);
}

