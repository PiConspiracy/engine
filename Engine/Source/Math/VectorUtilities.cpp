#include "Precompiled.h"
#include "VectorUtilities.h"

namespace PiConspiracy
{
  // Vector4
  void Vector4Dot(float& res, const Vector4& v1, const Vector4& v2)
  {
    res = v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
  }

  void Vector4Cross(Vector4& res, const Vector4& v1, const Vector4& v2)
  {
    res = Vector4(v1.y * v2.z - v1.z * v2.y,
                  v1.z * v2.x - v1.x * v2.z,
                  v1.x * v2.y - v1.y * v2.x, 1.0f);
  }

  void Vector4Distance(float& res, const Vector4& v1, const Vector4& v2)
  {
    Vector4 diff = v1 - v2;
    res = diff.Length();
  }

  void Vector4DistanceSq(float& res, const Vector4& v1, const Vector4& v2)
  {
    Vector4 diff = v1 - v2;
    res = diff.LengthSq();
  }

  void Vector4Normalize(Vector4& res, const Vector4& v)
  {
    float len = v.Length();
    if(len)
    {
      res.x = v.x/len;
      res.y = v.y/len;
      res.z = v.z/len;
    }
  }

  void Vector4Min(Vector4& res, const Vector4 & v1, const Vector4 & v2)
  {
    float x = std::min(v1.x, v2.x);
    float y = std::min(v1.y, v2.y);
    float z = std::min(v1.z, v2.z);
    res = Vector4(x, y, z, 1.0f);
  }

  void Vector4Max(Vector4& res, const Vector4 & v1, const Vector4 & v2)
  {
    float x = std::max(v1.x, v2.x);
    float y = std::max(v1.y, v2.y);
    float z = std::max(v1.z, v2.z);
    res = Vector4(x, y, z, 1.0f);
  }

  void Vector4Transform(Vector4& res, const Vector4& v, const Matrix4& m)
  {
    res = v * m;
  }

  void Vector4Lerp(Vector4& res, const Vector4& v1, const Vector4& v2, float t)
  {
    res = (1.0f - t) * v1 + t * v2;
  }

  Vector4 Vector4Lerp(const Vector4& v1, const Vector4& v2, float t)
  {
    return Lerp(v1, v2, t);
  }
  
  // Vector3
  void Vector3Min(Vector3& ret, const Vector3 & lhs, const Vector3 & rhs)
  {
    ret = Vector3(std::min(lhs.x, rhs.x), std::min(lhs.y, rhs.y), std::min(lhs.z, rhs.z));
  }

  void Vector3Max(Vector3& ret, const Vector3 & lhs, const Vector3 & rhs)
  {
    ret = Vector3(std::max(lhs.x, rhs.x), std::max(lhs.y, rhs.y), std::max(lhs.z, rhs.z));
  }

  void Vector3Transform(Vector3& ret, const Vector3& v, const Matrix4& m)
  {
    ret = v * m;
  }
  
  void Vector3Transform(Vector3& ret, const Vector3& v, const Matrix3& m)
  {
    ret = v * m;
  }

  void Vector3Projection(Vector3& ret, const Vector3& v1, const Vector3& v2)
  {
    ret = v2 * (v1.Dot(v2) / v2.Dot(v2));
  }
  // dot product of 2 vectors
  void Vector3Dot(float& ret, const Vector3& lhs, const Vector3& rhs)
  {
    ret = lhs.x * rhs.x + lhs.y * rhs.y + lhs.z * rhs.z;
  }

  // cross product of 2 vectors
  void Vector3Cross(Vector3& ret, const Vector3& lhs, const Vector3& rhs)
  {
    ret = Vector3(lhs.y * rhs.z - lhs.z * rhs.y,
                  lhs.z * rhs.x - lhs.x * rhs.z,
                  lhs.x * rhs.y - lhs.y * rhs.x);
  }

  // distance of a vector
  void Vector3Distance(float& ret, const Vector3& lhs, const Vector3& rhs)
  {
    Vector3 diff = lhs - rhs;
    ret = diff.Length();
  }

  // squared distance of a vector
  void Vector3DistanceSq(float& ret, const Vector3& lhs, const Vector3& rhs)
  {
    ret = (lhs - rhs).LengthSq();
  }

  // normalize the vector
  void Vector3Normalize(Vector3& ret, const Vector3 &vec)
  {
    float len = vec.Length();
    if(len)
    {
      ret.x = vec.x/len;
      ret.y = vec.y/len;
      ret.z = vec.z/len;
    }
  }

  float Vector3Angle( const Vector3 & lhs, const Vector3 & rhs )
  {
    float costheta; 
    Vector3Dot(costheta, lhs, rhs);
    costheta /= lhs.Length();
    costheta /= rhs.Length();
    return acos(costheta);
  }
  
  bool Vector3IsClose(const Vector3& lhs, const Vector3& rhs, float magicNumber)
  {
    if((lhs - rhs).Length() < magicNumber)
    {
      return true;
    }
    return false;
  }

  float Vector3DistanceSq_PointToLine(
    const Vector3 & line0, 
    const Vector3 & line1, 
    const Vector3 & point)
  {
    Vector3 v = line1 - line0;
    Vector3 w = point - line0;
    float c1;
    Vector3Dot(c1, w, v);
    if(c1 <= 0)
    {
      return (point - line0).LengthSq();
    }

    float c2;
    Vector3Dot(c2, v, v);
    if(c2 <= c1)
    {
      return (point - line1).LengthSq();
    }

    float b = c1 / c2;
    Vector3 Pb = line0 + (v*b);
    return (Pb - point).LengthSq();
  }

  float Vector3DistanceSq_PointToTri(const Vector3 & t0, 
                                     const Vector3 & t1, 
                                     const Vector3 & t2, 
                                     const Vector3 & point)
  {
    //find normal of the tri.
    Vector3 v0 = t2 - t0;
    Vector3 v1 = t1 - t0;
    Vector3 v2 = point - t0;
    float dot00, dot01, dot02, dot11, dot12;
    Vector3Dot(dot00, v0, v0);
    Vector3Dot(dot01, v0, v1);
    Vector3Dot(dot02, v0, v2);
    Vector3Dot(dot11, v1, v1);
    Vector3Dot(dot12, v1, v2);

    float invDenom = 1.0f / (dot00 * dot11 - dot01 * dot01);
    float u = (dot11 * dot02 - dot01 * dot12) * invDenom;
    float v = (dot00 * dot12 - dot01 * dot02) * invDenom;

    if((u >= 0) && (v >= 0) && (u + v < 1)) //we're inside
    {
      return 0.0f;
    }
    else if(u < 0.0f) // on the side with the line t0t1
    {
      return Vector3DistanceSq_PointToLine(t0, t1, point);
    }
    else if(v < 0.0f) // on the side with the line t0t2
    {
      return Vector3DistanceSq_PointToLine(t0, t2, point);
    }
    else // on the side with the line t1t2
    {
      return Vector3DistanceSq_PointToLine(t1, t2, point);
    }
  }

  PiConspiracy::Vector3 Vector3NormalizedPerpendicular( const Vector3 & rhs )
  {
    Vector3 result;
    Vector3 v1(1,0,0);
    Vector3 v2(0,1,0);
    if (Vector3Angle(v1, rhs) < 0.01f)
    {
      Vector3Cross(result, v2, rhs);
    }
    else
    {
      Vector3Cross(result, v1, rhs);
    }
    Vector3Normalize(result, result);
    return result;
  }

  void Vector3Lerp(Vector3& ret, const Vector3& v1, const Vector3& v2, float t)
  {
    ret = (1.0f - t) * v1 + t * v2;
  }

  Vector3 Vector3Lerp(const Vector3& v1, const Vector3& v2, float t)
  {
    return Lerp(v1, v2, t);
  }
  
  // Vector2
  void Vector2Dot(float& res, const Vector2& v1, const Vector2& v2)
  {
    res = v1.x * v2.x + v1.y * v2.y;
  }

  void Vector2Cross(float& res, const Vector2& v1, const Vector2& v2)
  {
    res = v1.x * v2.y - v1.y * v2.x;
  }

  void Vector2Distance(float& res, const Vector2& v1, const Vector2& v2)
  {
    Vector2 diff = v1 - v2;
    res = diff.Length();
  }
  void Vector2DistanceSq(float& res, const Vector2& v1, const Vector2& v2)
  {
    Vector2 diff = v1 - v2;
    res = diff.LengthSq();
  }
  
  void Vector2Normalize(Vector2& res, const Vector2& v)
  {
    float len = v.Length();
    if(len)
    {
      res.x = v.x/len;
      res.y = v.y/len;
    }
  }

  void Vector2Min(Vector2& res, const Vector2& v1, const Vector2& v2)
  {
    res = Vector2(std::min(v1.x, v2.x), std::min(v1.y, v2.y));
  }

  void Vector2Max(Vector2& res, const Vector2& v1, const Vector2& v2)
  {
    res = Vector2(std::max(v1.x, v2.x), std::max(v1.y, v2.y));
  }

  void Vector2Lerp(Vector2& ret, const Vector2& v1, const Vector2& v2, float t)
  {
    ret = (1.0f - t) * v1 + t * v2;
  }

  Vector2 Vector2Lerp(const Vector2& v1, const Vector2& v2, float t)
  {
    return Lerp(v1, v2, t);
  }

  // Plane 
  void PlaneEquation(const Vector3& point, Vector3& normal, Vector4& outPlane)
  {
    normal;
    Vector3Normalize(normal, normal);
    outPlane = Vector4(normal);
    Vector3Dot(outPlane.w, -point, normal);
  }
  
  void PlaneDotProduct(float& ret, const Vector4& p, const Vector3& v)
  {
    ret = p.x * v.x + p.y * v.y + p.z * v.z + p.w;
  }

  void PlaneNormalize(Vector4&res, const Vector4& p)
  {
    float len = sqrt(p.x*p.x + p.y * p.y + p.z * p.z);
    if(len)
    {
      res.x = p.x/len;
      res.y = p.y/len;
      res.z = p.z/len;
      res.w = p.w/len;
    }
  }
}
