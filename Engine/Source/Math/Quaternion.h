#pragma once


namespace PiConspiracy
{
	struct Vector3;
	struct Vector4;
	struct Matrix3;

	struct Quaternion
	{	
		float   x, y, z, w;
		Quaternion(void);
		Quaternion(float a_w,float a_x, float a_y, float a_z);
		//unary arithmetic operators
		Quaternion& operator=(const Quaternion& rhs);
		Quaternion& operator+=(const Quaternion& rhs);
		Quaternion& operator-=(const Quaternion& rhs);
		Quaternion& operator*=(const Quaternion& rhs);
		Quaternion& operator*=(float scalar);
		Quaternion& operator/=(float scalar);
		//unary bit operators
		Quaternion operator-(void)const;
		//binary operators
		Quaternion operator+(const Quaternion& rhs) const;
		Quaternion operator-(const Quaternion& rhs) const;
		Quaternion operator*(const Quaternion& rhs) const;
		Vector3 operator*(const Vector3& v)const;
		Quaternion operator*(const float a_scalar)const;

		//V3 * Q
		friend Vector3 operator*(const Vector3& v,const Quaternion& q);
		//Q * V3
		Vector4 operator*(const Vector4& v) const;
		//scalar * Q
		friend Quaternion operator*(float lhs,Quaternion& rhs);
		//V4 * Q
		friend Vector4 operator*(const Vector4& a_v, const Quaternion& a_Q);
		Quaternion operator/(float scalar)const;

		//comparison
		bool operator==(const Quaternion& rhs) const;
		bool operator!=(const Quaternion& rhs) const;
		bool operator< (const Quaternion& rhs) const;
		bool operator> (const Quaternion& rhs) const;
		bool operator>=(const Quaternion& rhs) const;
		bool operator<=(const Quaternion& rhs) const;

		//helper operations
		const Quaternion& Inverse(void) const;
		const Quaternion& Conjugate(void) const;
		const float Dot(const Quaternion& a_rhs) const;
		const float GetLength(void) const;

		const Quaternion& GetNormalize(void) const;
		//all angles expressed in radians
		float GetRoll(void) const;
		float GetPitch(void) const;
		float GetYaw(void) const;
		/// Returns euler angles, pitch as x, yaw as y, roll as z.
		/// The result is expressed in radians.
		Vector3 GetEulerAngles(void) const;
		
		// Returns the quaternion rotation angle.
		float GetAngle(void) const;
		
		//Returns the q rotation axis.
		Vector3 GetAxis(void) const;
		//stores 0 / 1 depending upon the nan values of the components
		Vector4 IsNan(void) const;
		Vector4 IsInf(void) const;

		//convert quat to matrix3
		Matrix3 ToMatrix3(void) const;

		//convert quat to matrix4
		Matrix4 ToMatrix4(void) const;

		

	};
}


