#pragma once

#define EPSILON  0.00001f
#define PI       3.14159265358f
#define PI_TWO   6.28318530716f
#define PI_RCP   0.31830988618f
#define GAUSS_DEFAULT_SEED 4256234177

