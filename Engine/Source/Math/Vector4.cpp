#include "Precompiled.h"
#include "Vector4.h"

namespace PiConspiracy
{
  Vector4::Vector4(void) : 
    x(0.0f), y(0.0f), z(0.0f), w(1.0f)
  {

  }

  Vector4::Vector4(const Vector2& rhs, float a_z, float a_w) : 
    x(rhs.x), y(rhs.y), z(a_z), w(a_w)
  {

  }

  Vector4::Vector4( const Vector3& rhs, float a_w) : 
    x(rhs.x), y(rhs.y), z(rhs.z), w(a_w)
  {
  }

  Vector4::Vector4(const Vector4& rhs) : 
    x(rhs.x), y(rhs.y), z(rhs.z), w(rhs.w)
  {
  }

  Vector4::Vector4(float a_x, float a_y, float a_z, float a_w) : 
    x(a_x), y(a_y), z(a_z), w(a_w)
  {
  }

  Vector4& Vector4::operator=(const Vector4& rhs)
  {
    if(this != &rhs)
    {
      x = rhs.x;
      y = rhs.y;
      z = rhs.z;
      w = rhs.w;
    }
    return *this;
  }

  Vector4 Vector4::operator-(void) const
  {
    return Vector4(-x, -y, -z, -w);
  }

  Vector4 Vector4::operator+(const Vector4& rhs) const
  {
    return Vector4( x + rhs.x,
                    y + rhs.y, 
                    z + rhs.z,
                    w + rhs.w);
  }

  Vector4 Vector4::operator-(const Vector4& rhs) const
  {
    return Vector4( x - rhs.x,
                    y - rhs.y, 
                    z - rhs.z,
                    w - rhs.w);
  }

  Vector4 Vector4::operator*(const float rhs) const
  {
    return Vector4( x * rhs,
                    y * rhs, 
                    z * rhs,
                    w * rhs);
  }

  Vector4 Vector4::operator*(const Matrix4& rhs) const
  {
    return Vector4(x * rhs.m00 + y * rhs.m10 + z * rhs.m20 + w * rhs.m30,
                   x * rhs.m01 + y * rhs.m11 + z * rhs.m21 + w * rhs.m31,
                   x * rhs.m02 + y * rhs.m12 + z * rhs.m22 + w * rhs.m32,
                   x * rhs.m03 + y * rhs.m13 + z * rhs.m23 + w * rhs.m33);
  }

  Vector4 Vector4::operator*(const Vector4& rhs) const
  {
    return Vector4( x * rhs.x,
                    y * rhs.y, 
                    z * rhs.z, 
                    w * rhs.w);
  }

  Vector4 Vector4::operator/(const float rhs) const
  {
    return Vector4( x / rhs,
                    y / rhs, 
                    z / rhs, 
                    w / rhs);
  }


  Vector4& Vector4::operator+=(const Vector4& rhs)
  {
    x += rhs.x;
    y += rhs.y; 
    z += rhs.z;
    w += rhs.w;
    return *this;
  }

  Vector4& Vector4::operator-=(const Vector4& rhs)
  {
    x -= rhs.x;
    y -= rhs.y; 
    z -= rhs.z;
    w -= rhs.w;
    return *this;
  }

  Vector4& Vector4::operator*=(const float rhs)
  {
    x *= rhs;
    y *= rhs; 
    z *= rhs;
    w *= rhs;
    return *this;
  }

  Vector4& Vector4::operator/=(const float rhs)
  {
    if(rhs == 0)
    {
      Zero();
    }
    else
    {
      x /= rhs;
      y /= rhs; 
      z /= rhs;
      w /= rhs;
    }
    return *this;
  }

  bool Vector4::operator==(const Vector4& rhs) const
  {
    return  abs(x - rhs.x) <= EPSILON &&
            abs(y - rhs.y) <= EPSILON &&
            abs(z - rhs.z) <= EPSILON &&
            abs(w - rhs.w) <= EPSILON;
  }

  bool Vector4::operator!=(const Vector4& rhs) const
  {
    return  abs(x - rhs.x) > EPSILON ||
            abs(y - rhs.y) > EPSILON ||
            abs(z - rhs.z) > EPSILON ||
            abs(w - rhs.w) > EPSILON;
  }

  bool Vector4::operator < (const Vector4 & rhs) const
  {
    return  (x < rhs.x) &&
            (y < rhs.y) && 
            (z < rhs.z) &&
            (w < rhs.w);
  }

  bool Vector4::operator > (const Vector4 & rhs) const
  {
    return  (x > rhs.x) &&
            (y > rhs.y) && 
            (z > rhs.z) &&
            (w > rhs.w);
  }

  Vector4 operator*(float f, Vector4 const & v)
  {
    return Vector4(v.x * f, v.y * f, v.z * f, v.w);
  }

  float Vector4::Length(void) const
  {
    return ::sqrt(x * x + y * y + z * z);
  }

  float Vector4::LengthSq(void) const
  {
    return x * x + y * y + z * z;
  }

  void Vector4::Negate(void)
  {
    x = -x; y = -y; z = -z; w = -w;
  }

  void Vector4::Maximize(void)
  {
    x = y = z = FLT_MAX;
  }

  void Vector4::Minimize(void)
  {
    x = y = z = FLT_MIN;
  }

  Vector4 Vector4::GetNormalized(void)
  {
    float len = Length();
    if(len == 0)
    {
      ENGINE_WARNING("Normalizing a zero vector!");
      return Vector4();
    }
    return Vector4(x/len, y/len, z/len, w/len);
  }

  void Vector4::Normalize(void)
  {
    float len = Length();
    if(len == 0)
    {
      ENGINE_WARNING("Normalizing a zero vector!");
      return;
    }
    x /=  len; 
    y /=  len; 
    z /=  len; 
    w /=  len;
  }

  float Vector4::Dot(const Vector4& rhs) const
  {
    return (x * rhs.x) + (y * rhs.y) + (z * rhs.z);// + (w * rhs.w);
  }

  Vector4 Vector4::Cross(const Vector4& rhs) const
  {
    return Vector4( y * rhs.z - z * rhs.y,
                    z * rhs.x - x * rhs.z,
                    x * rhs.y - y * rhs.x, 
                    1.0f);
  }

  bool Vector4::IsZero(void)
  {
    if(x == 0.0f && y == 0.0f && z == 0.0f && w == 0.0f) return true;
    else                                                 return false;
  }

  void Vector4::Zero(void)
  {
    x = y = z = 0.0f;
  }

  void Vector4::Print(void) const
  {
    ENGINE_LOG("%5.3f, %5.3f, %5.3f, %5.3f\n",x,y,z,w);
  }

  Vector4 Vector4::Min(const Vector4& rhs)
  {
    return Vector4(std::min(x, rhs.x), std::min(y, rhs.y), std::min(z, rhs.z), std::min(w, rhs.w));
  }

  Vector4 Vector4::Max(const Vector4& rhs)
  {
    return Vector4(std::max(x, rhs.x), std::max(y, rhs.y), std::max(z, rhs.z), std::max(w, rhs.w));
  }

  Vector4 Vector4::Swizzle(SWIZZLE a, SWIZZLE b, SWIZZLE c, SWIZZLE d)
  {
#ifdef ENGINE_DEBUG
    if (a > 2 || b > 2 || c > 2 || d > 2)
    {
      ENGINE_WARNING("Swizzling out of bounds.");
      return Vector4();
    }
#endif

    return Vector4(v[a], v[b], v[c], v[d]);
  }

  Vector3 Vector4::Swizzle(SWIZZLE a, SWIZZLE b, SWIZZLE c)
  {
#ifdef ENGINE_DEBUG
    if (a > 2 || b > 2 || c > 2)
    {
      ENGINE_WARNING("Swizzling out of bounds.");
      return Vector3();
    }
#endif

    return Vector3(v[a], v[b], v[c]);
  }

  Vector2 Vector4::Swizzle(SWIZZLE a, SWIZZLE b)
  {
#ifdef ENGINE_DEBUG
    if (a > 2 || b > 2)
    {
      ENGINE_WARNING("Swizzling out of bounds.");
      return Vector2();
    }
#endif

    return Vector2(v[a], v[b]);
  }

  Vector4i Vector4::ToI(void)
  {
    return Vector4i(static_cast<int32>(x), static_cast<int32>(y), static_cast<int32>(z), static_cast<int32>(w));
  }
}

