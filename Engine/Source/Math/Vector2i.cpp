#include "Precompiled.h"
#include "Vector2i.h"

namespace PiConspiracy
{
  Vector2i::Vector2i(void) : x(0), y(0)
  {
  }

  Vector2i::Vector2i(const Vector2i& rhs) : x(rhs.x), y(rhs.y)
  {
  }
  
  Vector2i::Vector2i(const Vector3i& rhs) : x(rhs.x), y(rhs.y)
  {
  }

  Vector2i::Vector2i(const Vector4i& rhs) : x(rhs.x), y(rhs.y)
  {
  }

  Vector2i::Vector2i(int32 a_x, int32 a_y) : x(a_x), y(a_y)
  {
  }

  Vector2i& Vector2i::operator=(const Vector2i& rhs)
  {
    if(this != &rhs)
    {
      x = rhs.x;
      y = rhs.y;
    }
    return *this;
  }

  Vector2i Vector2i::operator-(void) const
  {
    return Vector2i(-x, -y);
  }

  Vector2i Vector2i::operator+(const Vector2i& rhs) const
  {
    return Vector2i(x + rhs.x, y + rhs.y);
  }

  Vector2i Vector2i::operator-(const Vector2i& rhs) const
  {
    return Vector2i(x - rhs.x, y - rhs.y);
  }

  Vector2i Vector2i::operator*(const int32 rhs) const
  {
    return Vector2i(x * rhs, y * rhs);
  }

  Vector2i Vector2i::operator/(const int32 rhs) const
  {
    return Vector2i(x / rhs, y / rhs);
  }


  Vector2i& Vector2i::operator+=(const Vector2i& rhs)
  {
    x += rhs.x;
    y += rhs.y;
    return *this;
  }

  Vector2i& Vector2i::operator-=(const Vector2i& rhs)
  {
    x -= rhs.x;
    y -= rhs.y;
    return *this;
  }

  Vector2i& Vector2i::operator*=(const int32 rhs)
  {
    x *= rhs;
    y *= rhs;
    return *this;
  }

  Vector2i& Vector2i::operator/=(const int32 rhs)
  {
    x /= rhs;
    y /= rhs;
    return *this;
  }

  bool Vector2i::operator==(const Vector2i& rhs) const
  {
    return abs(x - rhs.x) <= EPSILON &&
           abs(y - rhs.y) <= EPSILON;
  }

  bool Vector2i::operator!=(const Vector2i& rhs) const
  {
    return abs(x - rhs.x) > EPSILON ||
           abs(y - rhs.y) > EPSILON;
  }

  Vector2i operator*(int32 f, Vector2i const & v)
  {
    return Vector2i(v.x * f, v.y * f);
  }

  bool Vector2i::operator < (const Vector2i & rhs) const
  {
    return 
      (x < rhs.x) || 
      (y < rhs.y);
  }
  bool Vector2i::operator > (const Vector2i & rhs) const
  {
    return 
      (x > rhs.x) || 
      (y > rhs.y);
  }

  Vector2 Vector2i::GetNormalized(void) const
  {
    Vector2 floats = ToF();
    floats.Normalize();
    return floats;
  }
  
  int32 Vector2i::Dot(const Vector2i& rhs) const
  {
    return (x * rhs.x) + (y * rhs.y);
  }

  int32 Vector2i::Cross(const Vector2i& rhs) const
  {
    return (x * rhs.y) - (y * rhs.x);
  }

  void Vector2i::Zero(void)
  {
    x = y = 0;
  }

  void Vector2i::Negate(void)
  {
    x = -x; y = -y;
  }

  float Vector2i::Length(void) const
  {
    return sqrt(static_cast<float>(LengthSq()));
  }
  
  int32 Vector2i::LengthSq(void) const
  {
    return (x * x) + (y * y);
  }
  
  void Vector2i::Print(void) const
  {
    ENGINE_LOG("%i, %i\n",x,y);
  }

  Vector2i Vector2i::Min(const Vector2i& rhs)
  {
    return Vector2i(std::min(x, rhs.x), std::min(y, rhs.y));
  }

  Vector2i Vector2i::Max(const Vector2i& rhs)
  {
    return Vector2i(std::max(x, rhs.x), std::max(y, rhs.y));
  }

  Vector4i Vector2i::Swizzle(SWIZZLE a, SWIZZLE b, SWIZZLE c, SWIZZLE d)
  {
#ifdef ENGINE_DEBUG
    if(a > 1 || b > 1 || c > 1 || d > 1) 
    {
      ENGINE_WARNING("Swizzling out of bounds.");
      return Vector4i(); 
    }
#endif

    return Vector4i(v[a], v[b], v[c], v[d]);
  }

  Vector3i Vector2i::Swizzle(SWIZZLE a, SWIZZLE b, SWIZZLE c)
  {
#ifdef ENGINE_DEBUG
    if(a > 1 || b > 1 || c > 1) 
    { 
      ENGINE_WARNING("Swizzling out of bounds.");
      return Vector3i(); 
    }

    return Vector3i(v[a], v[b], v[c]);
  }
#endif

  Vector2i Vector2i::Swizzle(SWIZZLE a, SWIZZLE b)
  {
#ifdef ENGINE_DEBUG
    if(a > 1 || b > 1) 
    {
      ENGINE_WARNING("Swizzling out of bounds.");
      return Vector2i(); 
    }
#endif

    return Vector2i(v[a], v[b]);
  }

  Vector2 Vector2i::ToF(void) const
  {
    return Vector2(static_cast<float>(x), static_cast<float>(y));
  }
}

