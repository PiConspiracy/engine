#pragma once

namespace PiConspiracy
{
  struct Vector4;
  struct Vector3;
  struct Vector2;

  //Linear
  Vector4 Lerp(const Vector4& vs, const Vector4& vf, float t);
  Vector3 Lerp(const Vector3& vs, const Vector3& vf, float t);
  Vector2 Lerp(const Vector2& vs, const Vector2& vf, float t);
  float   Lerp(float vs, float vf, float t);

  //Bi-linear
  Vector4 Blerp(const Vector4& v0s, const Vector4& v0f, 
                const Vector4& v1s, const Vector4& v1f, 
                float t1, float t2);
  Vector3 Blerp(const Vector3& v0s, const Vector3& v0f, 
                const Vector3& v1s, const Vector3& v1f, 
                float t1, float t2);
  Vector2 Blerp(const Vector2& v0s, const Vector2& v0f, 
                const Vector2& v1s, const Vector2& v1f, 
                float t1, float t2);
  float Blerp(float v0s, float v0f, 
              float v1s, float v1f, 
              float t1, float t2);

  //Tri-linear
  Vector4 Tlerp(const Vector4& v0s, const Vector4& v0f, 
                const Vector4& v1s, const Vector4& v1f,
		            const Vector4& v2s, const Vector4& v2f, 
                const Vector4& v3s, const Vector4& v3f,
		            float t1, float t2, float t3);

  Vector3 Tlerp(const Vector3& v0s, const Vector3& v0f, 
                const Vector3& v1s, const Vector3& v1f,
		            const Vector3& v2s, const Vector3& v2f, 
                const Vector3& v3s, const Vector3& v3f,
		            float t1, float t2, float t3);

  Vector2 Tlerp(const Vector2& v0s, const Vector2& v0f, 
                const Vector2& v1s, const Vector2& v1f,
		            const Vector2& v2s, const Vector2& v2f, 
                const Vector2& v3s, const Vector2& v3f,
		            float t1, float t2, float t3);

  float Tlerp(float v0s, float v0f, float v1s, float v1f,
		          float v2s, float v2f, float v3s, float v3f,
		          float t1, float t2, float t3);
}
