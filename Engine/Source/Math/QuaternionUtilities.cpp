#include "Precompiled.h"
#include "QuaternionUtilities.h"

namespace PiConspiracy
{
	Quaternion QuatFrmAngleAxis(float a_angle, const Vector3 & a_axis)
	{
		Quaternion Result;
		float const a = a_angle;
		float const s = sin(a * 0.5f);

		Result.w = cos(a * 0.5f);
		Result.x = a_axis.x * s;
		Result.y = a_axis.y * s;
		Result.z = a_axis.z * s;
		return Result;
	}
	Quaternion QuatFromMatrix3(Matrix3 & a_mat)
	{
		float fourXSquaredMinus1 = a_mat.m[0][0] - a_mat.m[1][1] - a_mat.m[2][2];
		float fourYSquaredMinus1 = a_mat.m[1][1] - a_mat.m[0][0] - a_mat.m[2][2];
		float fourZSquaredMinus1 = a_mat.m[2][2] - a_mat.m[0][0] - a_mat.m[1][1];
		float fourWSquaredMinus1 = a_mat.m[0][0] + a_mat.m[1][1] + a_mat.m[2][2];

		int32 biggestIndex = 0;
		float fourBiggestSquaredMinus1 = fourWSquaredMinus1;
		if (fourXSquaredMinus1 > fourBiggestSquaredMinus1)
		{
			fourBiggestSquaredMinus1 = fourXSquaredMinus1;
			biggestIndex = 1;
		}
		if (fourYSquaredMinus1 > fourBiggestSquaredMinus1)
		{
			fourBiggestSquaredMinus1 = fourYSquaredMinus1;
			biggestIndex = 2;
		}
		if (fourZSquaredMinus1 > fourBiggestSquaredMinus1)
		{
			fourBiggestSquaredMinus1 = fourZSquaredMinus1;
			biggestIndex = 3;
		}

		float biggestVal = sqrt(fourBiggestSquaredMinus1 + 1.0f) * 0.5f;
		float mult = 0.25f / biggestVal;

		switch (biggestIndex)
		{
		case 0:
			return Quaternion(biggestVal, (a_mat.m[1][2] - a_mat.m[2][1]) * mult, (a_mat.m[2][0] - a_mat.m[0][2]) * mult, (a_mat.m[0][1] - a_mat.m[1][0]) * mult);
		case 1:	   
			return Quaternion((a_mat.m[1][2] - a_mat.m[2][1]) * mult, biggestVal, (a_mat.m[0][1] + a_mat.m[1][0]) * mult, (a_mat.m[2][0] + a_mat.m[0][2]) * mult);
		case 2:	   
			return Quaternion((a_mat.m[2][0] - a_mat.m[0][2]) * mult, (a_mat.m[0][1] + a_mat.m[1][0]) * mult, biggestVal, (a_mat.m[1][2] + a_mat.m[2][1]) * mult);
		case 3:	   
			return Quaternion((a_mat.m[0][1] - a_mat.m[1][0]) * mult, (a_mat.m[2][0] + a_mat.m[0][2]) * mult, (a_mat.m[1][2] + a_mat.m[2][1]) * mult, biggestVal);
		default: // Silence a -Wswitch-default warning in GCC. Should never actually get here. Assert is just for sanity.
			ENGINE_WARNING(false);
			return Quaternion(1, 0, 0, 0);
		}
	}

	Quaternion Mix(Quaternion & a_x, Quaternion & a_y, float a_InterpolationFactor)
	{
		float cosTheta = a_x.Dot(a_y);

		// Perform a linear interpolation when cosTheta is close to 1 to avoid side effect of sin(angle) becoming a zero denominator
		if (cosTheta > 1.0f - EPSILON)
		{
			// Linear interpolation
			return Quaternion(
				Compute_Mix(a_x.w, a_y.w, a_InterpolationFactor),
				Compute_Mix(a_x.x, a_y.x, a_InterpolationFactor),
				Compute_Mix(a_x.y, a_y.y, a_InterpolationFactor),
				Compute_Mix(a_x.z, a_y.z, a_InterpolationFactor));
		}
		else
		{
			// Essential Mathematics, page 467
			float angle = acos(cosTheta);
			return (sinf((1.0f - a_InterpolationFactor) * angle) * a_x + sin(a_InterpolationFactor * angle) * a_y) / sin(angle);
		}
	}

	Quaternion Lerp2Quat(Quaternion & a_x, Quaternion & a_y, float a_InterpolationFactor)
	{
		ENGINE_WARNING(a_InterpolationFactor >= 0.0f);
		ENGINE_WARNING(a_InterpolationFactor <= 0.0f);

		return a_x * (1.0f - a_InterpolationFactor) + (a_y * a_InterpolationFactor);
	}
	Quaternion Slerp2Quat(Quaternion & a_x, Quaternion & a_y, float a_InterpolationFactor)
	{
		Quaternion z = a_y;

		float cosTheta = a_x.Dot(a_y);

		// If cosTheta < 0, the interpolation will take the long way around the sphere.
		// To fix this, one quat must be negated.
		if (cosTheta < 0.0f)
		{
			z = -a_y;
			cosTheta = -cosTheta;
		}

		// Perform a linear interpolation when cosTheta is close to 1 to avoid side effect of sin(angle) becoming a zero denominator
		if (cosTheta > 1.0f - EPSILON)
		{
			// Linear interpolation
			return Quaternion(
				Compute_Mix(a_x.w, z.w, a_InterpolationFactor),
				Compute_Mix(a_x.x, z.x, a_InterpolationFactor),
				Compute_Mix(a_x.y, z.y, a_InterpolationFactor),
				Compute_Mix(a_x.z, z.z, a_InterpolationFactor));
		}
		else
		{
			// file quaternion.inl line# 561
			float angle = acos(cosTheta);
			return ( sin((1.0f - a_InterpolationFactor) * angle) * a_x + sin(a_InterpolationFactor * angle) * z) / sin(angle);
		}
	}
	float Compute_Mix(float a_x, float a_y, float a_A)
	{
		return a_x + a_A * (a_y-a_x);
	}
	Quaternion RotateQuat(Quaternion & a_Q, float angle, Vector3 & a_Axis)
	{
		Vector3 Tmp = a_Axis;

		// Axis of rotation must be normalised
		float len = Tmp.Length();
		if (abs(len - 1.0f) > 0.001)
		{
			float oneOverLen = 1.0f / len;
			Tmp.x *= oneOverLen;
			Tmp.y *= oneOverLen;
			Tmp.z *= oneOverLen;
		}

		float const AngleRad(angle);
		float const Sin = sin(AngleRad * 0.5f);

		return a_Q * Quaternion(cosf(AngleRad * 0.5), Tmp.x * Sin, Tmp.y * Sin, Tmp.z * Sin);
	}
}
