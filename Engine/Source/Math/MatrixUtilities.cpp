#include "Precompiled.h"
#include "MatrixUtilities.h"

namespace PiConspiracy
{
  // creates a tensor product matrix
  void Matrix4TensorProduct(Matrix4& ret, const Vector3& v)
  {
    ret = Matrix4(v.x * v.x, v.x * v.y, v.x * v.z, 0.0f,
                  v.y * v.x, v.y * v.y, v.y * v.z, 0.0f,
                  v.z * v.x, v.z * v.y, v.z * v.z, 0.0f,
                  0.0f,      0.0f,      0.0f,      1.0f);
  }

  // creates a cross product matrix
  void Matrix4CrossProduct(Matrix4& ret, const Vector3& v)
  {
    ret = Matrix4(0.0f, -v.z, v.y,  0.0f,
                  v.z,  0.0f, -v.x, 0.0f,
                 -v.y,  v.x,  0.0f, 0.0f,
                  0.0f, 0.0f, 0.0f, 1.0f);

  }

  // creates a transpose of a matrix
  void Matrix4Transpose(Matrix4& ret, const Matrix4& m)
  {
    ret = Matrix4(m.m00, m.m10, m.m20, m.m30,
                  m.m01, m.m11, m.m21, m.m31,
                  m.m02, m.m12, m.m22, m.m32,
                  m.m03, m.m13, m.m23, m.m33);
  }

  // creates an inverse of a matrix
  void Matrix4Inverse(Matrix4& ret, const Matrix4& m)
  {
    float const *original = m.v;		// cast the matrix as a flat array
    float temp[12];				// used for intermediate calculations
    float src[16];				// transpose of original matrix inverse
    float inverse[16];			// matrix inverse

    // transpose the original matrix
    for( int32 i =  0; i < 4; i++ )
    {
      src[i]      = original[i * 4];
      src[i + 4]  = original[i * 4 + 1];
      src[i + 8]  = original[i * 4 + 2];
      src[i + 12] = original[i * 4 + 3];
    }

    // calculate pairs for the first 8 elements (cofactors)
    temp[0]  = src[10] * src[15];
    temp[1]  = src[11] * src[14];
    temp[2]  = src[9]  * src[15];
    temp[3]  = src[11] * src[13];
    temp[4]  = src[9]  * src[14];
    temp[5]  = src[10] * src[13];
    temp[6]  = src[8]  * src[15];
    temp[7]  = src[11] * src[12];
    temp[8]  = src[8]  * src[14];
    temp[9]  = src[10] * src[12];
    temp[10] = src[8]  * src[13];
    temp[11] = src[9]  * src[12];

    // calculate first 8 elements (cofactors)
    inverse[0]  = temp[0] * src[5] + temp[3] * src[6] + temp[4]  * src[7];
    inverse[0] -= temp[1] * src[5] + temp[2] * src[6] + temp[5]  * src[7];
    inverse[1]  = temp[1] * src[4] + temp[6] * src[6] + temp[9]  * src[7];
    inverse[1] -= temp[0] * src[4] + temp[7] * src[6] + temp[8]  * src[7];
    inverse[2]  = temp[2] * src[4] + temp[7] * src[5] + temp[10] * src[7];
    inverse[2] -= temp[3] * src[4] + temp[6] * src[5] + temp[11] * src[7];
    inverse[3]  = temp[5] * src[4] + temp[8] * src[5] + temp[11] * src[6];
    inverse[3] -= temp[4] * src[4] + temp[9] * src[5] + temp[10] * src[6];
    inverse[4]  = temp[1] * src[1] + temp[2] * src[2] + temp[5]  * src[3];
    inverse[4] -= temp[0] * src[1] + temp[3] * src[2] + temp[4]  * src[3];
    inverse[5]  = temp[0] * src[0] + temp[7] * src[2] + temp[8]  * src[3];
    inverse[5] -= temp[1] * src[0] + temp[6] * src[2] + temp[9]  * src[3];
    inverse[6]  = temp[3] * src[0] + temp[6] * src[1] + temp[11] * src[3];
    inverse[6] -= temp[2] * src[0] + temp[7] * src[1] + temp[10] * src[3];
    inverse[7]  = temp[4] * src[0] + temp[9] * src[1] + temp[10] * src[2];
    inverse[7] -= temp[5] * src[0] + temp[8] * src[1] + temp[11] * src[2];

    // calculate pairs for second 8 elements (cofactors)
    temp[0] = src[2] * src[7];
    temp[1] = src[3] * src[6];
    temp[2] = src[1] * src[7];
    temp[3] = src[3] * src[5];
    temp[4] = src[1] * src[6];
    temp[5] = src[2] * src[5];
    temp[6] = src[0] * src[7];
    temp[7] = src[3] * src[4];
    temp[8] = src[0] * src[6];
    temp[9] = src[2] * src[4];
    temp[10] = src[0] * src[5];
    temp[11] = src[1] * src[4];

    // calculate second 8 elements (cofactors)
    inverse[8]   = temp[0]  * src[13] + temp[3]  * src[14] + temp[4]  * src[15];
    inverse[8]  -= temp[1]  * src[13] + temp[2]  * src[14] + temp[5]  * src[15];
    inverse[9]   = temp[1]  * src[12] + temp[6]  * src[14] + temp[9]  * src[15];
    inverse[9]  -= temp[0]  * src[12] + temp[7]  * src[14] + temp[8]  * src[15];
    inverse[10]  = temp[2]  * src[12] + temp[7]  * src[13] + temp[10] * src[15];
    inverse[10] -= temp[3]  * src[12] + temp[6]  * src[13] + temp[11] * src[15];
    inverse[11]  = temp[5]  * src[12] + temp[8]  * src[13] + temp[11] * src[14];
    inverse[11] -= temp[4]  * src[12] + temp[9]  * src[13] + temp[10] * src[14];
    inverse[12]  = temp[2]  * src[10] + temp[5]  * src[11] + temp[1]  * src[9];
    inverse[12] -= temp[4]  * src[11] + temp[0]  * src[9]  + temp[3]  * src[10];
    inverse[13]  = temp[8]  * src[11] + temp[0]  * src[8]  + temp[7]  * src[10];
    inverse[13] -= temp[6]  * src[10] + temp[9]  * src[11] + temp[1]  * src[8];
    inverse[14]  = temp[6]  * src[9]  + temp[11] * src[11] + temp[3]  * src[8];
    inverse[14] -= temp[10] * src[11] + temp[2]  * src[8]  + temp[7]  * src[9];
    inverse[15]  = temp[10] * src[10] + temp[4]  * src[8]  + temp[9]  * src[9];
    inverse[15] -= temp[8]  * src[9]  + temp[11] * src[10] + temp[5]  * src[8];

    // calculate determinant
    float det = src[0] * inverse[0] + src[1] * inverse[1] + src[2] * inverse[2] + src[3] * inverse[3];

    if(det == 0) 
    {
      ret = Matrix4(m);
    }

    det =  1.0f / det;
    ret = Matrix4(inverse[0], inverse[1], inverse[2], inverse[3],
                  inverse[4], inverse[5], inverse[6], inverse[7],
                  inverse[8], inverse[9], inverse[10], inverse[11],
                  inverse[12], inverse[13], inverse[14], inverse[15]) * det;
  }

  // creates per-component absolute value matrix
  void Matrix4Abs(Matrix4& ret, const Matrix4& m)
  {
    ret = Matrix4(fabs(m.m00), fabs(m.m01), fabs(m.m02), fabs(m.m03),
                  fabs(m.m10), fabs(m.m11), fabs(m.m12), fabs(m.m13),
                  fabs(m.m20), fabs(m.m21), fabs(m.m22), fabs(m.m23),
                  fabs(m.m30), fabs(m.m31), fabs(m.m32), fabs(m.m33));
  }

  // creates a translation matrix
  void Matrix4Translation(Matrix4& ret, float x, float y, float z)
  {
    ret = Matrix4(1, 0, 0, 0,
                  0, 1, 0, 0,
                  0, 0, 1, 0,
                  x, y, z, 1);
  }

  // creates a translation matrix
  void Matrix4Translation(Matrix4& ret, const Vector3& v)
  {
    ret = Matrix4(1, 0, 0, 0,
                  0, 1, 0, 0,
                  0, 0, 1, 0,
                  v.x, v.y, v.z, 1);
  }

  // creates a rotation matrix (in radians)
  void Matrix4RotationX(Matrix4& ret, float angle)
  {
    float c = ::cosf(angle);
    float s = ::sinf(angle);
    ret = Matrix4(1,  0,  0,  0,
                  0,  c,  s,  0,
                  0, -s,  c,  0,
                  0,  0,  0,  1);
  }

  // creates a rotation matrix (in radians)
  void Matrix4RotationY(Matrix4& ret, float angle)
  {
    float c = ::cosf(angle);
    float s = ::sinf(angle);
    ret = Matrix4(c,  0, -s,  0,
                  0,  1,  0,  0,
                  s,  0,  c,  0,
                  0,  0,  0,  1);
  }

  // creates a rotation matrix (in radians)
  void Matrix4RotationZ(Matrix4& ret, float angle)
  {
    float c = ::cosf(angle);
    float s = ::sinf(angle);
    ret = Matrix4(c,  s,  0,  0,
                 -s,  c,  0,  0,
                  0,  0,  1,  0,
                  0,  0,  0,  1);
  }

  // creates a rotation matrix (in radians)
  void Matrix4RotationYawPitchRoll(Matrix4& ret, float yaw, float pitch, float roll)
  {
    Matrix4 z, x, y;
    Matrix4RotationY(y, yaw);
    Matrix4RotationX(x, pitch);
    Matrix4RotationZ(z, roll);
    ret = z * x * y;
  }

  // creates a rotation matrix (in radians)
  void Matrix4RotationYawPitchRoll(Matrix4& ret, const Vector3& v)
  {
    Matrix4 z, x, y;
    Matrix4RotationY(y, v.y);
    Matrix4RotationX(x, v.x);
    Matrix4RotationZ(z, v.z);
    ret = z * x * y;
  }

  // create a centered left-handed orthographic projeciton matrix
  void Matrix4OrthoOffCenterLH(Matrix4& ret, float l, float r, float b, float t, float zn, float zf)
  {
    ret = Matrix4(2/(r-l),      0,            0,           0,
                  0,            2/(t-b),      0,           0,
                  0,            0,            1/(zf-zn),   0,
                  (l+r)/(l-r),  (t+b)/(b-t),  zn/(zn-zf),  1);
  }

  // create a centered right-handed orthographic projeciton matrix
  void Matrix4OrthoOffCenterRH(Matrix4& ret, float l, float r, float b, float t, float zn, float zf)
  {
    ret = Matrix4(2/(r-l),      0,            0,           0,
                  0,            2/(t-b),      0,           0,
                  0,            0,            1/(zn-zf),   0,
                  (l+r)/(l-r),  (t+b)/(b-t),  zn/(zn-zf),  1);
  }

  // creates a rotation matrix (in radians)
  void Matrix4RotationArbitrary(Matrix4& ret, const Vector3& axis, float angle)
  {
    Vector4 normalized;
    Vector4Normalize(normalized, axis);
    float x = normalized.x;
    float y = normalized.y;
    float z = normalized.z;

    float c = ::cosf(angle);
    float s = ::sinf(angle);
    float t = 1 - c;

    ret = Matrix4(t * x * x + c,     t * x * y + s * z, t * x * z - s * y, 0,
                  t * x * y - s * z, t * y * y + c,     t * y * z + s * x, 0,
                  t * x * z + s * y, t * y * z - s * x, t * z * z + c,     0,
                  0,                 0,                 0,                 1);
  }

  Matrix4 Matrix4Scaling(float s)
  {
  	return Matrix4(s, 0, 0, 0,
  				            0, s, 0, 0,
  				            0, 0, s, 0,
  				            0, 0, 0, 1);
  }

  // create a scaling matrix
  void Matrix4Scaling(Matrix4& ret, float s)
  {
	  ret = Matrix4(s, 0, 0, 0,
				          0, s, 0, 0,
				          0, 0, s, 0,
				          0, 0, 0, 1);
  }

  // create a scaling matrix
  void Matrix4Scaling(Matrix4& ret, float x, float y, float z)
  {
    ret = Matrix4(x, 0, 0, 0,
                  0, y, 0, 0,
                  0, 0, z, 0,
                  0, 0, 0, 1);
  }

  // create a scaling matrix
  void Matrix4Scaling(Matrix4& ret, const Vector3& v)
  {
    ret = Matrix4(v.x, 0,   0,   0,
                  0,   v.y, 0,   0,
                  0,   0,   v.z, 0,
                  0,   0,   0,   1);
  }

  // create a transform matrix
  //Matrix4 Matrix4VQS(const Vector3& v, const Quaternion& q, float s)
  //{
  //
  //}

  //// create a left-handed view matrix
  void Matrix4LookAtLH(Matrix4& ret, const Vector3& eye, const Vector3& lookAt, const Vector3& up)
  {
    Vector3 zAxis, xAxis, yAxis;
    float dotx, doty, dotz;
    Vector3Normalize(zAxis, lookAt - eye);
    Vector3Cross(xAxis, up, zAxis);
    Vector3Normalize(xAxis, xAxis);
    Vector3Cross(yAxis, zAxis, xAxis);
    Vector3Dot(dotx, xAxis, eye);
    Vector3Dot(doty, yAxis, eye);
    Vector3Dot(dotz, zAxis, eye);

    ret = Matrix4(xAxis.x,                 yAxis.x,                zAxis.x,               0,
                  xAxis.y,                 yAxis.y,                zAxis.y,               0,
                  xAxis.z,                 yAxis.z,                zAxis.z,               0,
                  -dotx,                   -doty,                  -dotz,                 1);
  }

  //// create a left-handed projection matrix
  void Matrix4OrthographicLH(Matrix4& ret, float width, float height, float zNear, float zFar)
  {
    ret = Matrix4(2 / width, 0,          0,                         0,
                  0,         2 / height, 0,                         0,
                  0,         0,          1 / ( zFar - zNear ),      0,
                  0,         0,          -zNear / ( zFar - zNear ), 1);
  }

  //// create a left-handed projection matrix
  void Matrix4PerspectiveFovLH(Matrix4& ret, float fov, float aspectRatio, float zNear, float zFar)
  {
    float yScale = 1 / ::tan(fov / 2);
    float xScale = yScale / aspectRatio;

    ret = Matrix4(xScale, 0,      0,                                0,
                  0,      yScale, 0,                                0,
                  0,      0,      zFar / ( zFar - zNear ),          1,
                  0,      0,      -zNear * zFar / ( zFar - zNear ), 0);  
  }
  
  //// create a right-handed view matrix
  void Matrix4LookAtRH(Matrix4& ret, const Vector3& eye, const Vector3& at, const Vector3& up)
  {
    Vector3 zAxis, xAxis, yAxis;
    float dotx, doty, dotz;
    Vector3Normalize(zAxis, eye - at);
    Vector3Cross(xAxis, up, zAxis);
    Vector3Normalize(zAxis, zAxis);
    Vector3Cross(yAxis, zAxis, xAxis);
    Vector3Dot(dotx, xAxis, eye);
    Vector3Dot(doty, yAxis, eye);
    Vector3Dot(dotz, zAxis, eye);

    ret = Matrix4(xAxis.x,                  yAxis.x,                  zAxis.x,                 0,
                  xAxis.y,                  yAxis.y,                  zAxis.y,                 0,
                  xAxis.z,                  yAxis.z,                  zAxis.z,                 0,
                  -dotx,                    -doty,                    -dotz,                   1);  
  }
  
  //// create a right-handed projection matrix
  void Matrix4OrthographicRH(Matrix4& ret, float width, float height, float zNear, float zFar)
  {
    ret = Matrix4(2 / width, 0,          0,                        0,
                  0,         2 / height, 0,                        0,
                  0,         0,          1 / ( zNear - zFar ),     0,
                  0,         0,          zNear / ( zNear - zFar ), 1);  
  }
  
  //// create a right-handed projection matrix
  void Matrix4PerspectiveFovRH(Matrix4& ret, float fov, float aspectRatio, float zNear, float zFar)
  {
    float yScale = 1 / ::tan(fov / 2);
    float xScale = yScale / aspectRatio;

    ret = Matrix4(xScale, 0,      0,                                0,
                  0,      yScale, 0,                                0,
                  0,      0,      zFar / ( zNear - zFar ),         -1,
                  0,      0,      zNear * zFar / ( zNear - zFar ),  0);  
  }

  // Matrix 3
  // creates a rotation matrix (in radians)
  void Matrix3RotationArbitrary(Matrix3& ret, const Vector3& axis, float angle)
  {
    Vector3 normalized;
    Vector3Normalize(normalized, axis);
    float x = normalized.x;
    float y = normalized.y;
    float z = normalized.z;

    float c = ::cosf(angle);
    float s = ::sinf(angle);
    float t = 1 - c;

    ret = Matrix3(t * x * x + c,     t * x * y + s * z, t * x * z - s * y,
                  t * x * y - s * z, t * y * y + c,     t * y * z + s * x,
                  t * x * z + s * y, t * y * z - s * x, t * z * z + c);
  }

  // Matrix 2
  Matrix2 MatrixTranspose(const Matrix2& m)
  {
    return Matrix2(m.m00, m.m10, m.m01, m.m11);         
  }

  Matrix2 MatrixInverse(const Matrix2& m)
  {
    return Matrix2(m.m11, -m.m01, -m.m10, m.m00);
  }
}
