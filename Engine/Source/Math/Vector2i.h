#pragma once

namespace PiConspiracy
{
  struct Vector3i;
  struct Vector4i;

  struct Vector2i
  {
    // Constructor
    Vector2i(void);  
    Vector2i(const Vector2i& rhs);
    Vector2i(const Vector3i& rhs);
    Vector2i(const Vector4i& rhs);
    Vector2i(int32 a_x, int32 a_y);
  
    // Assignment operator, does not need to handle self assignment
    Vector2i& operator=(const Vector2i& rhs);
  
    // Unary negation operator, negates all components and returns a copy
    Vector2i  operator-(void) const;  
    
    // Math Operators
    Vector2i  operator+(const Vector2i& rhs) const;
    Vector2i  operator-(const Vector2i& rhs) const;
    Vector2i  operator*(const int32 rhs) const;
    Vector2i  operator/(const int32 rhs) const;
    Vector2i& operator+=(const Vector2i& rhs);
    Vector2i& operator-=(const Vector2i& rhs);
    Vector2i& operator*=(const int32 rhs);
    Vector2i& operator/=(const int32 rhs);
  
    // Comparison operators
    bool operator==(const Vector2i& rhs) const;
    bool operator!=(const Vector2i& rhs) const;
    bool operator< (const Vector2i & rhs) const;
    bool operator> (const Vector2i & rhs) const;
         
    // Casting operators
    friend Vector2i operator*(int32 lhs, Vector2i const & rhs);

    // Linear Algebra
    float    Length(void) const;
    int32    LengthSq(void) const;
    void     Negate(void);
    Vector2  GetNormalized(void) const;
    int32    Dot(const Vector2i& rhs) const;
    int32    Cross(const Vector2i& rhs) const;

    // set all components to zero
    void Zero(void);

    // Helpers
    void    Print(void) const;
    Vector2i Min(const Vector2i& rhs);
    Vector2i Max(const Vector2i& rhs);

    Vector4i Swizzle(SWIZZLE a, SWIZZLE b, SWIZZLE c, SWIZZLE d);
    Vector3i Swizzle(SWIZZLE a, SWIZZLE b, SWIZZLE c);
    Vector2i Swizzle(SWIZZLE a, SWIZZLE b);

    Vector2 ToF(void) const;
  public:
    union
    {
      struct 
      {
        int32 x, y;
      };

      int32 v[2];
    };
  };
}

