#pragma once

namespace PiConspiracy
{
  struct Vector2;
  struct Vector4;
  struct Vector3i;
  struct Matrix3;
  struct Matrix4;

  struct Vector3
  {
    // Constructors
    Vector3(void);
    Vector3(const Vector2& rhs, float a_z = 0.0f);
    Vector3(const Vector3& rhs);
    Vector3(float a_x, float a_y, float a_z);

    // Assignment operator, does not need to handle self assignment
    Vector3& operator=(const Vector3& rhs);

    // Unary negation operator, negates all components and returns a copy
    Vector3  operator-(void) const;  

    // Math Operators
    Vector3  operator+(const Vector3& rhs) const;
    Vector3  operator-(const Vector3& rhs) const;
    Vector3  operator*(const Matrix4& rhs) const;
    Vector3  operator*(const Matrix3& rhs) const;
    Vector3  operator*(const float rhs) const;
    Vector3  operator/(const float rhs) const;
    Vector3& operator+=(const Vector3& rhs);
    Vector3& operator-=(const Vector3& rhs);
    Vector3& operator*=(const float rhs);
    Vector3& operator/=(const float rhs);

    // Comparison operators
    bool operator==(const Vector3& rhs) const;
    bool operator!=(const Vector3& rhs) const;
    bool operator< (const Vector3 & rhs) const;
    bool operator> (const Vector3 & rhs) const;
         
    // casting operators
    friend Vector3 operator*(float lhs, Vector3 const & rhs);

    // Linear Algebra
    float   Length(void) const;
    float   LengthSq(void) const;
    void    Negate(void);
    Vector3 GetNormalized(void); //returns a normalized version of the vector
    void    Normalize(void);
    float   Dot(const Vector3& rhs) const;
    Vector3 Cross(const Vector3& rhs) const;
    bool    IsZero(void);

    // Setters
    void Zero(void);

    // Helpers
    void    Print(void) const;
    Vector3 Min(const Vector3& rhs);
    Vector3 Max(const Vector3& rhs);

    Vector4 Swizzle(SWIZZLE a, SWIZZLE b, SWIZZLE c, SWIZZLE d);
    Vector3 Swizzle(SWIZZLE a, SWIZZLE b, SWIZZLE c);
    Vector2 Swizzle(SWIZZLE a, SWIZZLE b);

    Vector3i ToI(void);

  public:
    union
    {
      struct 
      {
        float x, y, z;
      };

      float v[3];
    };
  };
}

