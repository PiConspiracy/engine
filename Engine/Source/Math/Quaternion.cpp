#include "Precompiled.h"
#include "Quaternion.h"

namespace PiConspiracy
{
	Quaternion::Quaternion(void):x(0.0f),y(0.0f),z(0.0f),w(0.0f)
	{
	}

	Quaternion::Quaternion(float a_x, float a_y, float a_z, float a_w):x(a_x),y(a_y),z(a_z),w(a_w)
	{
	}

	Quaternion & Quaternion::operator=(const Quaternion &a_Q)
	{
		x = a_Q.x;
		y = a_Q.y;
		z = a_Q.z;
		w = a_Q.w;
		return *this;
	}

	Quaternion & Quaternion::operator+=(const Quaternion &a_Q)
	{
		x += a_Q.x;
		y += a_Q.y;
		z += a_Q.z;
		w += a_Q.w;
		return *this;
	}

	Quaternion & Quaternion::operator-=(const Quaternion &a_Q)
	{
		x -= a_Q.x;
		y -= a_Q.y;
		z -= a_Q.z;
		w -= a_Q.w;
		return *this;
	}

	Quaternion & Quaternion::operator*=(const Quaternion &a_Q)
	{
		x *= a_Q.x;
		y *= a_Q.y;
		z *= a_Q.z;
		w *= a_Q.w;
		return *this;
	}

	Quaternion & Quaternion::operator*=(float a_scalar)
	{
		x *= a_scalar;
		y *= a_scalar;
		z *= a_scalar;
		w *= a_scalar;
		return *this;
	}

	Quaternion & Quaternion::operator/=(float a_scalar)
	{
		
		x /= a_scalar;
		y /= a_scalar;
		z /= a_scalar;
		w /= a_scalar;
		return *this;
	}

	Quaternion Quaternion::operator-(void) const
	{
		return Quaternion(-x,-y,-z,-w);
	}

	Quaternion Quaternion::operator+(const Quaternion &a_Q) const
	{
		return Quaternion(
			x + a_Q.x,
			y + a_Q.y,
			z + a_Q.z,
			w + a_Q.w
		);
	}

	Quaternion Quaternion::operator-(const Quaternion& a_rhs) const
	{
		return Quaternion(
			x - a_rhs.x,
			y - a_rhs.y,
			z - a_rhs.z,
			w - a_rhs.w
		);
	}

	Quaternion Quaternion::operator*(const Quaternion& a_rhs) const
	{
		return Quaternion(
			x * a_rhs.x,
			y * a_rhs.y,
			z * a_rhs.z,
			w * a_rhs.w
		);
	}

	Vector3 Quaternion::operator*(const Vector3& v) const
	{
		Vector3 quatVec(x,y,z);
		Vector3 uv;
		Vector3Cross(uv, quatVec,v);
		Vector3 uuv;
		Vector3Cross(uuv, quatVec, uv);

		return v + ((uv * this->w) + uuv) * 2;
	}

	Quaternion Quaternion::operator*(const float a_scalar) const
	{
		return Quaternion(x * a_scalar, y * a_scalar, z * a_scalar, w * a_scalar);
	}

	Vector4 Quaternion::operator*(const Vector4& v) const
	{
		return Vector4(*this * Vector3(v.x,v.y,v.z), v.w);
	}

	Quaternion Quaternion::operator/(float a_scalar) const
	{
		if (a_scalar == 0.0f)
		{
			ENGINE_WARNING("error dividing quaternion by 0\n");
			return Quaternion(0,0,0,0);
		}
		return Quaternion(
			x/a_scalar,
			y/a_scalar,
			z/a_scalar,
			w/a_scalar
			);
	}

	Vector3 operator*(const Vector3 & a_v, const Quaternion & a_Q)
	{
		Quaternion inv = a_Q.Inverse();
		return inv * a_v;
	}

	Quaternion operator*(float a_lhs, Quaternion & a_rhs)
	{
		return Quaternion(a_rhs.x * a_lhs, a_rhs.y * a_lhs, a_rhs.z * a_lhs, a_rhs.w * a_lhs);
	}

	Vector4 operator*(const Vector4 & a_v, const Quaternion & a_Q)
	{
		Quaternion inv = a_Q.Inverse();
		return inv * a_v;
	}

	bool Quaternion::operator==(const Quaternion & a_rhs) const
	{
		return(
			abs(x - a_rhs.x) <= EPSILON &&
			abs(y - a_rhs.y) <= EPSILON &&
			abs(z - a_rhs.z) <= EPSILON &&
			abs(w - a_rhs.w) <= EPSILON 
			);
		
	}

	bool Quaternion::operator!=(const Quaternion & a_rhs) const
	{
		return(
			abs(x - a_rhs.x) > EPSILON ||
			abs(y - a_rhs.y) > EPSILON ||
			abs(z - a_rhs.z) > EPSILON ||
			abs(w - a_rhs.w) > EPSILON
			);
	}

	bool Quaternion::operator<(const Quaternion & a_rhs) const
	{
		return (x < a_rhs.x) && (y < a_rhs.y) && (z < a_rhs.z) && (w < a_rhs.w);
	}

	bool Quaternion::operator>(const Quaternion & a_rhs) const
	{
		return (x > a_rhs.x) && (y > a_rhs.y) && (z > a_rhs.z) && (w > a_rhs.w);
	}

	bool Quaternion::operator>=(const Quaternion & a_rhs) const
	{
		return (x >= a_rhs.x) && (y >= a_rhs.y) && (z >= a_rhs.z) && (w >= a_rhs.w);
	}

	bool Quaternion::operator<=(const Quaternion & a_rhs) const
	{
	   return (x <= a_rhs.x) && (y <= a_rhs.y) && (z <= a_rhs.z) && (w <= a_rhs.w);
	}


	const float Quaternion::Dot(const Quaternion & a_rhs) const
	{
		//refer glm lib, file quaternion.inl line# 20
		Vector4 tmp(x * a_rhs.x , y * a_rhs.y , z * a_rhs.z , w * a_rhs.w);
		return (tmp.x + tmp.y) + (tmp.z + tmp.w);
	}

	const Quaternion& Quaternion::Conjugate(void) const
	{
		return Quaternion(- (*this));
	}

	const Quaternion & Quaternion::Inverse(void) const
	{
		return this->Conjugate() / Dot(*this);
	}
	const float Quaternion::GetLength(void) const
	{
		return sqrt(Dot(*this));
	}

	const Quaternion & Quaternion::GetNormalize(void) const
	{
		float length = GetLength();
		if (length <= 0.0f)
		{
			return Quaternion(1.0f,0.0f,0.0f,0.0f);
		}
		else
		{
			float oneOverLength = 1.0f / length;
			return Quaternion(this->x * oneOverLength, 
							  this->y * oneOverLength,
							  this->z * oneOverLength,
					          this->w * oneOverLength
			);
		}
	}

	float Quaternion::GetRoll(void) const
	{
		return std::atan2f((2.0f) * (x * y + w * z), w * w + x * x - y * y - z * z);
	}

	float Quaternion::GetPitch(void) const
	{
		float y1 = (2.0f) * (y * z + w * x);
		float x1 = w * w - x * x - y * y + z * z;

		if (x <= EPSILON || y<= EPSILON) //avoid atan2(0,0) - handle singularity - Matiis
			return(2.0f) * std::atan2f(this->x, this->w);

		return std::atan2f(y1, x1);
	}

	float Quaternion::GetYaw(void) const
	{
		return std::asinf( std::clamp(2.0f * (x * z - w * y), -1.0f, 1.0f) );
	}

	Vector3 Quaternion::GetEulerAngles(void) const
	{
		return Vector3(GetPitch(),GetYaw(),GetRoll());
	}

	float Quaternion::GetAngle(void) const
	{
		return acosf(w) * 2.0f;
	}

	Vector3 Quaternion::GetAxis(void) const
	{
		float temp1 = 1.0f - w * w;
		if (temp1 <= 0.0f)
		{
			return Vector3(0.0f,0.0f,1.0f);
		}
		float temp2 = 1.0f / sqrtf(temp1);
		return Vector3(x*temp2 ,y*temp2 , z*temp2);
	}

	Vector4 Quaternion::IsNan(void) const
	{

		return Vector4((float)isnan(x), (float)isnan(y), (float)isnan(z), (float)isnan(w));
	}

	Vector4 Quaternion::IsInf(void) const
	{

		return Vector4((float)isinf(x), (float)isinf(y), (float)isinf(z), (float)isinf(w));
	}

	Matrix3 Quaternion::ToMatrix3(void) const
	{
		Matrix3 Result;
		float qxx(x * x);
		float qyy(y * y);
		float qzz(z * z);
		float qxz(x * z);
		float qxy(x * y);
		float qyz(y * z);
		float qwx(w * x);
		float qwy(w * y);
		float qwz(w * z);

		Result.m[0][0] = (1) - (2) * (qyy + qzz);
		Result.m[0][1] = (2) * (qxy + qwz);
		Result.m[0][2] = (2) * (qxz - qwy);
		
		Result.m[1][0] = (2) * (qxy - qwz);
		Result.m[1][1] = (1) - (2) * (qxx + qzz);
		Result.m[1][2] = (2) * (qyz + qwx);
		
		Result.m[2][0] = (2) * (qxz + qwy);
		Result.m[2][1] = (2) * (qyz - qwx);
		Result.m[2][2] = (1) - (2) * (qxx + qyy);
		return Result;
	}



	

	

	
}