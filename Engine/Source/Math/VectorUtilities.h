#pragma once

namespace PiConspiracy
{
  // Vector4
  void Vector4Min(Vector4& res, const Vector4& v1, const Vector4& v2);
  void Vector4Max(Vector4& res, const Vector4& v1, const Vector4& v2);
  void Vector4Dot(float& res, const Vector4& v1, const Vector4& v2);
  void Vector4Cross(Vector4& res, const Vector4& v1, const Vector4& v2);
  void Vector4Distance(float& res, const Vector4& v1, const Vector4& v2);
  void Vector4DistanceSq(float& res, const Vector4& v1, const Vector4& v2);
  void Vector4Normalize(Vector4& res, const Vector4& v);
  void Vector4Transform(Vector4& res, const Vector4& v, const Matrix4& m);
  void Vector4Lerp(Vector4& res, const Vector4& v1, const Vector4& v2, float t);
  Vector4 Vector4Lerp(const Vector4& v1, const Vector4& v2, float t);

  // Vector3
  void Vector3Min(Vector3& res, const Vector3& v1, const Vector3& v2);
  void Vector3Max(Vector3& res, const Vector3& v1, const Vector3& v2);
  void Vector3Dot(float& ret, const Vector3& v1, const Vector3& v2);
  void Vector3Cross(Vector3& ret, const Vector3& v1, const Vector3& v2);
  void Vector3Distance(float& ret, const Vector3& v1, const Vector3& v2);
  void Vector3DistanceSq(float& ret, const Vector3& v1, const Vector3& v2);
  void Vector3Normalize(Vector3& ret, const Vector3& v);
  void Vector3Transform(Vector3& ret, const Vector3& v, const Matrix4& m);
  void Vector3Transform(Vector3& ret, const Vector3& v, const Matrix3& m);
  void Vector3Projection(Vector3& ret, const Vector3& v1, const Vector3& v2);
  float Vector3Angle(const Vector3 & lhs ,const Vector3 & rhs);
  bool Vector3IsClose(const Vector3& lhs, const Vector3& rhs, float magicNumber);//??
  Vector3 Vector3NormalizedPerpendicular(const Vector3 & rhs);
  float Vector3DistanceSq_PointToLine(const Vector3 & line0, const Vector3 & line1, const Vector3 & point);
  float Vector3DistanceSq_PointToTri(const Vector3 & t0, const Vector3 & t1, const Vector3 & t2, const Vector3 & point);
  void Vector3Lerp(Vector3& ret, const Vector3& v1, const Vector3& v2, float t);
  Vector3 Vector3Lerp(const Vector3& v1, const Vector3& v2, float t);

  // Vector2
  void Vector2Min(Vector2& ret, const Vector2& v1, const Vector2& v2);
  void Vector2Max(Vector2& ret, const Vector2& v1, const Vector2& v2);
  void Vector2Dot(float& ret, const Vector2& v1, const Vector2& v2);
  void Vector2Cross(float& ret, const Vector2& v1, const Vector2& v2);
  void Vector2Distance(float& ret, const Vector2& v1, const Vector2& v2);
  void Vector2DistanceSq(float& ret, const Vector2& v1, const Vector2& v2);
  void Vector2Normalize(Vector2& ret, const Vector2& v);
  void Vector2Lerp(Vector2& ret, const Vector2& v1, const Vector2& v2, float t);
  Vector2 Vector2Lerp(const Vector2& v1, const Vector2& v2, float t);

  // Plane 
  // solves ax + by + cz + d = 0 into outPlane[a,b,c,d]
  void PlaneEquation(const Vector3 & point, Vector3& normal, Vector4 & outPlane); 
  void PlaneDotProduct(float& res, const Vector4& p, const Vector3& v);
  void PlaneNormalize(Vector4&res, const Vector4& p);
}

