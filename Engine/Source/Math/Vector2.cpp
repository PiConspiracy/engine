#include "Precompiled.h"
#include "Vector2.h"

namespace PiConspiracy
{
  Vector2::Vector2(void) : x(0.0f), y(0.0f)
  {
  }

  Vector2::Vector2(const Vector2& rhs) : x(rhs.x), y(rhs.y)
  {
  }

  Vector2::Vector2(float a_x, float a_y) : x(a_x), y(a_y)
  {
  }

  Vector2& Vector2::operator=(const Vector2& rhs)
  {
    if(this != &rhs)
    {
      x = rhs.x;
      y = rhs.y;
    }
    return *this;
  }

  Vector2 Vector2::operator-(void) const
  {
    return Vector2(-x, -y);
  }

  Vector2 Vector2::operator+(const Vector2& rhs) const
  {
    return Vector2(x + rhs.x, y + rhs.y);
  }

  Vector2 Vector2::operator-(const Vector2& rhs) const
  {
    return Vector2(x - rhs.x, y - rhs.y);
  }

  Vector2 Vector2::operator*(const float rhs) const
  {
    return Vector2(x * rhs, y * rhs);
  }

  Vector2 Vector2::operator/(const float rhs) const
  {
    return Vector2(x / rhs, y / rhs);
  }


  Vector2& Vector2::operator+=(const Vector2& rhs)
  {
    x += rhs.x;
    y += rhs.y;
    return *this;
  }

  Vector2& Vector2::operator-=(const Vector2& rhs)
  {
    x -= rhs.x;
    y -= rhs.y;
    return *this;
  }

  Vector2& Vector2::operator*=(const float rhs)
  {
    x *= rhs;
    y *= rhs;
    return *this;
  }

  Vector2& Vector2::operator/=(const float rhs)
  {
    x /= rhs;
    y /= rhs;
    return *this;
  }

  bool Vector2::operator==(const Vector2& rhs) const
  {
    return abs(x - rhs.x) <= EPSILON &&
           abs(y - rhs.y) <= EPSILON;
  }

  bool Vector2::operator!=(const Vector2& rhs) const
  {
    return abs(x - rhs.x) > EPSILON ||
           abs(y - rhs.y) > EPSILON;
  }

  Vector2 operator*(float f, Vector2 const & v)
  {
    return Vector2(v.x * f, v.y * f);
  }

  bool Vector2::operator < (const Vector2 & rhs) const
  {
    return 
      (x < rhs.x) || 
      (y < rhs.y);
  }
  bool Vector2::operator > (const Vector2 & rhs) const
  {
    return 
      (x > rhs.x) || 
      (y > rhs.y);
  }

  Vector2 Vector2::GetNormalized(void)
  {
    float len = Length();
    if(len == 0.0f)
    {
      ENGINE_WARNING("Normalizing a zero vector!");
      return Vector2();
    }    
    return Vector2(x / len, y / len);
  }

  void Vector2::Normalize(void)
  {
    float len = Length();
    if(len == 0.0f)
    {
      ENGINE_WARNING("Normalizing a zero vector!");
      return;
    }   
    x /= len;
    y /= len;
  }
  
  float Vector2::Dot(const Vector2& rhs) const
  {
    return (x * rhs.x) + (y * rhs.y);
  }

  float Vector2::Cross(const Vector2& rhs) const
  {
    return (x * rhs.y) - (y * rhs.x);
  }

  bool Vector2::IsZero(void)
  {
    if(x == 0.0f && y == 0.0f) return true;
    else                       return false;
  }

  void Vector2::Zero(void)
  {
    x = y = 0.0f;
  }

  void Vector2::Negate(void)
  {
    x = -x; y = -y;
  }

  float Vector2::Length(void) const
  {
    return ::sqrt(LengthSq());
  }
  
  float Vector2::LengthSq(void) const
  {
    return (x * x) + (y * y);
  }
  
  void Vector2::Print(void) const
  {
    ENGINE_LOG("%5.2f, %5.2f\n", x, y);
  }

  Vector2 Vector2::Min(const Vector2& rhs)
  {
    return Vector2(std::min(x, rhs.x), std::min(y, rhs.y));
  }

  Vector2 Vector2::Max(const Vector2& rhs)
  {
    return Vector2(std::max(x, rhs.x), std::max(y, rhs.y));
  }

  Vector4 Vector2::Swizzle(SWIZZLE a, SWIZZLE b, SWIZZLE c, SWIZZLE d)
  {
#ifdef ENGINE_DEBUG
    if(a > 1 || b > 1 || c > 1 || d > 1) 
    {
      ENGINE_WARNING("Swizzling out of bounds.");
      return Vector4(); 
    }
#endif

    return Vector4(v[a], v[b], v[c], v[d]);
  }

  Vector3 Vector2::Swizzle(SWIZZLE a, SWIZZLE b, SWIZZLE c)
  {
#ifdef ENGINE_DEBUG
    if(a > 1 || b > 1 || c > 1) 
    { 
      ENGINE_WARNING("Swizzling out of bounds.");
      return Vector3(); 
    }
#endif

    return Vector3(v[a], v[b], v[c]);
  }

  Vector2 Vector2::Swizzle(SWIZZLE a, SWIZZLE b)
  {
#ifdef ENGINE_DEBUG
    if(a > 1 || b > 1) 
    {
      ENGINE_WARNING("Swizzling out of bounds.");
      return Vector2(); 
    }
#endif

    return Vector2(v[a], v[b]);
  }

  Vector2i Vector2::ToI()
  {
    return Vector2i(static_cast<int32>(x), static_cast<int32>(y));
  }
}

