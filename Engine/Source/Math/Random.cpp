#include "Precompiled.h"
#include "Random.h"

namespace PiConspiracy
{
  float RandGauss01()
  {
    return (RandGauss11() + 1.0f) / 2.0f;
  }

  float RandGauss11()
  {
    return RandGauss33()/3.0f;
  }

  float RandGauss33()
  {
    static uint64 seed = GAUSS_DEFAULT_SEED;
    static float sum = 0; sum = 0;
    static int64 r = 0;
    static uint64 hold = 0;
    for(int32 i=0; i<3; i++)
    {   
      //Uses an xorshift PRNG
      hold = seed;
      seed^=seed<<13; 
      seed^=seed>>17;
      seed^=seed<<5;
      r = hold+seed;
      sum += static_cast<float>(r * (1.0f/0x7FFFFFFF));
    }
    if(sum < -3.0f) { sum = -3.0f; }
    if(sum >  3.0f) { sum =  3.0f; }
    return sum;
  }

  float Rand11()
  {
    return ((2.0f)*(static_cast<float>(rand() % 101)*0.01f)) - 1.0f;
  }

  float Rand01()
  {
    return RandBetween(0, 1, 100.0f);
  }

  float RandBetween(float mins, float maxs, float base)
  {
    return ((maxs - mins)*(static_cast<float>(RandBetween(0, static_cast<int32>(base)))/base)) + mins;
  }

  float RandFloat(float mini, float maxi)
  {
    return mini + static_cast<float>(rand() / static_cast<float>(RAND_MAX)) * (maxi - mini);
  }

  int32 RandBetween(int32 min, int32 max)
  {
    return min + (rand() % (max + 1));
  }

  int32 RandInt32(int32 mini, int32 maxi)
  {
    if(maxi - mini == 0)
    {
      return 0;
    }

    return mini + rand() % (maxi - mini);
  }
}
