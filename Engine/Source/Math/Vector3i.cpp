#include "Precompiled.h"
#include "Vector3i.h"

namespace PiConspiracy
{
  Vector3i::Vector3i(void) : 
    x(0), y(0), z(0)
  {
  }

  Vector3i::Vector3i(const Vector2i& rhs, int32 a_z) : 
    x(rhs.x), y(rhs.y), z(a_z)
  {
  }

  Vector3i::Vector3i(const Vector3i& rhs) : 
    x(rhs.x), y(rhs.y), z(rhs.z)
  {
  }

  Vector3i::Vector3i(int32 a_x, int32 a_y, int32 a_z) : 
    x(a_x), y(a_y), z(a_z)
  {
  }

  Vector3i& Vector3i::operator=(const Vector3i& rhs)
  {
    if(this != &rhs)
    {
      x = rhs.x;
      y = rhs.y;
      z = rhs.z;
    }
    return *this;
  }

  Vector3i Vector3i::operator-(void) const
  {
    return Vector3i(-x, -y, -z);
  }

  Vector3i Vector3i::operator+(const Vector3i& rhs) const
  {
    return Vector3i(x + rhs.x, y + rhs.y, z + rhs.z);
  }

  Vector3i Vector3i::operator-(const Vector3i& rhs) const
  {
    return Vector3i(x - rhs.x, y - rhs.y, z - rhs.z);
  }

  Vector3i Vector3i::operator*(const int32 rhs) const
  {
    return Vector3i(x * rhs, y * rhs, z * rhs);
  }

  Vector3i Vector3i::operator/(const int32 rhs) const
  {
    return Vector3i(x / rhs, y / rhs, z / rhs);
  }


  Vector3i& Vector3i::operator+=(const Vector3i& rhs)
  {
    x += rhs.x;
    y += rhs.y; 
    z += rhs.z;
    return *this;
  }

  Vector3i& Vector3i::operator-=(const Vector3i& rhs)
  {
    x -= rhs.x;
    y -= rhs.y; 
    z -= rhs.z;
    return *this;
  }

  Vector3i& Vector3i::operator*=(const int32 rhs)
  {
    x *= rhs;
    y *= rhs; 
    z *= rhs;
    return *this;
  }

  Vector3i& Vector3i::operator/=(const int32 rhs)
  {
    x /= rhs;
    y /= rhs; 
    z /= rhs;
    return *this;
  }

  bool Vector3i::operator==(const Vector3i& rhs) const
  {
    return  abs(x - rhs.x) <= EPSILON &&
            abs(y - rhs.y) <= EPSILON &&
            abs(z - rhs.z) <= EPSILON;
  }

  bool Vector3i::operator!=(const Vector3i& rhs) const
  {
    return  abs(x - rhs.x) > EPSILON ||
            abs(y - rhs.y) > EPSILON ||
            abs(z - rhs.z) > EPSILON;
  }

  Vector3i operator*(int32 f, Vector3i const & v)
  {
    return Vector3i(v.x * f, v.y * f, v.z * f);
  }

  bool Vector3i::operator < (const Vector3i & rhs) const
  {
    return (x < rhs.x) && (y < rhs.y) && (z < rhs.z);
  }

  bool Vector3i::operator > (const Vector3i & rhs) const
  {
    return (x > rhs.x) && (y > rhs.y) && (z > rhs.z);
  }

  float Vector3i::Length(void) const
  {
    return ::sqrt(static_cast<float>(LengthSq()));
  }


  int32 Vector3i::LengthSq(void) const
  {
    return (x * x) + (y * y) + (z * z);
  }

  void Vector3i::Negate(void)
  {

  }

  Vector3 Vector3i::GetNormalized(void)
  {
    Vector3 floats = ToF();
    floats.Normalize();
    return floats;
  }

  int32 Vector3i::Dot(const Vector3i& rhs) const
  {
    return (x * rhs.x) + (y * rhs.y) + (z * rhs.z);
  }

  Vector3i Vector3i::Cross(const Vector3i& rhs) const
  {
    return Vector3i( y * rhs.z - z * rhs.y,
                    z * rhs.x - x * rhs.z,
                    x * rhs.y - y * rhs.x);
  }

  void Vector3i::Zero(void)
  {
    x = y = z = 0;
  }

  void Vector3i::Print(void) const
  {
    ENGINE_LOG("%5.3d, %5.3d, %5.3d\n", x, y, z);
  }

  Vector3i Vector3i::Min(const Vector3i & rhs)
  {
    return Vector3i(std::min(x, rhs.x), std::min(y, rhs.y), std::min(z, rhs.z));
  }

  Vector3i Vector3i::Max(const Vector3i & rhs)
  {
    return Vector3i(std::max(x, rhs.x), std::max(y, rhs.y), std::max(z, rhs.z));
  }

  Vector4i Vector3i::Swizzle(SWIZZLE a, SWIZZLE b, SWIZZLE c, SWIZZLE d)
  {
#ifdef ENGINE_DEBUG
    if(a > 2 || b > 2 || c > 2 || d > 2) 
    {
      ENGINE_WARNING("Swizzling out of bounds.");
      return Vector4i(); 
    }

    return Vector4i(v[a], v[b], v[c], v[d]);
  }
#endif

  Vector3i Vector3i::Swizzle(SWIZZLE a, SWIZZLE b, SWIZZLE c)
  {
#ifdef ENGINE_DEBUG
    if(a > 2 || b > 2 || c > 2) 
    { 
      ENGINE_WARNING("Swizzling out of bounds.");
      return Vector3i(); 
    }

    return Vector3i(v[a], v[b], v[c]);
  }
#endif

  Vector2i Vector3i::Swizzle(SWIZZLE a, SWIZZLE b)
  {
#ifdef ENGINE_DEBUG
    if(a > 2 || b > 2) 
    { 
      ENGINE_WARNING("Swizzling out of bounds.");
      return Vector2i(); 
    }
#endif

    return Vector2i(v[a], v[b]);
  }

  Vector3 Vector3i::ToF(void)
  {
    return Vector3(static_cast<float>(x), static_cast<float>(y), static_cast<float>(z));
  }
}

