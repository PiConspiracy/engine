#pragma once

namespace PiConspiracy
{
  struct Vector3;
  struct Vector4;

  struct Matrix4
  {
    union
    {
      struct  
      {
        float m00, m01, m02, m03,
              m10, m11, m12, m13,
              m20, m21, m22, m23,
              m30, m31, m32, m33;
      };
  
      float m[4][4];
      float v[16];
    };
  
    // Constructor
    Matrix4(void);
    Matrix4(const Matrix4& rhs);
    Matrix4(float a_m00, float a_m01, float a_m02, float a_m03,
            float a_m10, float a_m11, float a_m12, float a_m13,
            float a_m20, float a_m21, float a_m22, float a_m23,
            float a_m30, float a_m31, float a_m32, float a_m33);
  
    // Assignment operator, does not need to handle self-assignment
    Matrix4& operator=(const Matrix4& rhs);
  
    // Multiplying a Matrix4 with a Vector4 or a Point4
    Vector4 operator*(const Vector4& rhs) const;
  
    // Basic Matrix arithmetic operations
    Matrix4 operator+(const Matrix4& rhs) const;
    Matrix4 operator-(const Matrix4& rhs) const;
    Matrix4 operator*(const Matrix4& rhs) const;
    Matrix4& operator+=(const Matrix4& rhs);
    Matrix4& operator-=(const Matrix4& rhs);
    Matrix4& operator*=(const Matrix4& rhs);
    Matrix4 operator*(const float rhs) const;
    Matrix4 operator/(const float rhs) const;
    Matrix4& operator*=(const float rhs);
    Matrix4& operator/=(const float rhs);
  
    // Comparison operators
    bool operator==(const Matrix4& rhs) const;
    bool operator!=(const Matrix4& rhs) const;

    // Linear Algebra
    void Transpose(void);
    void Inverse(void);

    // Setters
    void Zero(void);
    void Identity(void);
  
    // Other
    void Print(void) const;
  };
}

