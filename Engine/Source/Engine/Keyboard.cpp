#include "Precompiled.h"
#include "Keyboard.h"
#include "InputState.h"

PUSH_BASIC_TYPE_RESTRICTORS

#undef unsigned
#undef int
#undef short
#undef long

#include <SDL2/SDL.h>

POP_BASIC_TYPE_RESTRICTORS

namespace PiConspiracy
{
  Keyboard::Keyboard(void)
    : m_textInput(false)
  {

  }

  Keyboard::~Keyboard(void)
  {

  }

  bool Keyboard::ProcessEvent(const SDL_Event& a_event, InputState& a_inputState)
  {
    switch (a_event.type)
    {
    case SDL_KEYDOWN:
    {
      // Only generate the next message if we are doing
      if (m_textInput)
      {
        a_inputState.m_type = EInputType::KeyboardButtonText;
        a_inputState.m_keyboardState.m_pressedTime[a_event.key.keysym.scancode] = a_event.key.timestamp;
        a_inputState.m_keyboardState.m_buttonStates[a_event.key.keysym.scancode] = EKeyboardButtonState::Pressed;
        a_inputState.m_action.m_keyboardButton = static_cast<EKeyboardButton>(a_event.key.keysym.scancode);
        
      }
      else
      {
        if (a_inputState.m_keyboardState.m_buttonStates[a_event.key.keysym.scancode] != EKeyboardButtonState::Held)
        {
          a_inputState.m_type = EInputType::KeyboardButtonRealtime;
          a_inputState.m_keyboardState.m_pressedTime[a_event.key.keysym.scancode] = a_event.key.timestamp;
          a_inputState.m_keyboardState.m_buttonStates[a_event.key.keysym.scancode] = EKeyboardButtonState::Pressed;
          a_inputState.m_action.m_keyboardButton = static_cast<EKeyboardButton>(a_event.key.keysym.scancode);
        }
        // Completely discard this message from the system. We will handle repetitive update ourselves.
        else
        {
          return false;
        }
      }
    }
    break;
  
    case SDL_KEYUP:
    {     
      if (!m_textInput)
      {
        // There seems to be weird behavior where some keys "ghost" some other keys.
        // For example, on my keyboard, pressing 'K' generates KEY_DOWN, however, 
        // pressing 'J' afterwards, while 'K' is pressed, generates KEY_UP on 'K'.
        // This seems to be an internal keyboard/IP issue, and I don't know
        // how to mitigate that. Right now, luckily, most of "game-friendly" keys
        // are unaffected by this.

        a_inputState.m_type = EInputType::KeyboardButtonRealtime;
      }
      else
      {
        a_inputState.m_type = EInputType::KeyboardButtonText;
      }

      a_inputState.m_keyboardState.m_pressedTime[a_event.key.keysym.scancode] = 0;
      a_inputState.m_keyboardState.m_buttonStates[a_event.key.keysym.scancode] = EKeyboardButtonState::Released;
      a_inputState.m_action.m_keyboardButton = static_cast<EKeyboardButton>(a_event.key.keysym.scancode);
    }
    break;
  
    case SDL_TEXTEDITING:
    {
      
    }
    break;
  
    case SDL_TEXTINPUT:
    {
      if (m_textInput)
      {
        if (a_inputState.m_keyboardState.m_buttonStates[a_event.key.keysym.scancode] != EKeyboardButtonState::Held)
        {
          a_inputState.m_keyboardState.m_pressedTime[a_event.key.keysym.scancode] = a_event.key.timestamp;
          a_inputState.m_keyboardState.m_buttonStates[a_event.key.keysym.scancode] = EKeyboardButtonState::Pressed;
        }

        a_inputState.m_type = EInputType::KeyboardButtonText;
        a_inputState.m_action.m_keyboardButton = static_cast<EKeyboardButton>(a_event.key.keysym.scancode);
      }
      else
      {
        return false;
      }
    }
    break;
  
    case SDL_KEYMAPCHANGED:
    {
  
    }
    break;
  
    default:
      ENGINE_CRITICAL("Bad Keyboard message");
      break;
    }

    return true;
  }

  void Keyboard::KeyboardTextInput(bool a_text)
  {
    m_textInput = a_text;
  }

  bool Keyboard::IsTextInputEnabled(void) const
  {
    return m_textInput;
  }
}
