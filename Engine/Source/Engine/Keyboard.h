#pragma once

union SDL_Event;

namespace PiConspiracy
{
  class Keyboard
  {
  public:
    Keyboard(void);
    ~Keyboard(void);

    bool ProcessEvent(const SDL_Event& a_event, class InputState& a_inputState);
    void KeyboardTextInput(bool a_text);
    bool IsTextInputEnabled(void) const;

  private:
    bool m_textInput;
  };
}

