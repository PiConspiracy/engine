#include "Precompiled.h"
#include "Platform.h"
#include "Window.h"
#include "InputManager.h"
#include "FrameRateController.h"
#include "SystemEvent.h"

PUSH_BASIC_TYPE_RESTRICTORS

#undef unsigned
#undef int
#undef short
#undef long

#include "SDL2/SDL.h"
#include "SDL2/SDL_syswm.h"

#undef CreateWindow

POP_BASIC_TYPE_RESTRICTORS

namespace PiConspiracy
{
  Platform::Platform(void)
  {
    SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS | SDL_INIT_GAMECONTROLLER);

    m_inputManager = new InputManager();
    m_messenger = Singleton<Messenger>::Get();
    m_framerateController = new FrameRateController();
    m_terminated = false;
  }

  Platform::~Platform(void)
  {
    delete m_framerateController;
    delete m_inputManager;

    SDL_Quit();
  }

  string Platform::GetPlatformName(void) const
  {
    return SDL_GetPlatform();
  }

  Window* Platform::CreateWindow(const string& a_name, int32 a_x, int32 a_y, int32 a_width, int32 a_height, uint32 a_flags)
  {
    WindowDesc desc;

    SDL_Window* sdlWindow = SDL_CreateWindow(a_name.c_str(), a_x, a_y, a_width, a_height, a_flags);
    desc.m_sdlWindow = sdlWindow;

    SDL_SysWMinfo wmInfo;
    SDL_VERSION(&wmInfo.version);
    SDL_GetWindowWMInfo(sdlWindow, &wmInfo);

    desc.m_handle = wmInfo.info.win.window;
    desc.m_instance = wmInfo.info.win.hinstance;

    Window* window = new Window(desc);

    return window;
  }

  void Platform::Intitialize(void)
  {
    m_inputManager->SetInputMessenger(m_messenger);
  }

  bool Platform::Update(void)
  {
    // If exit message was set (and processed at this time, quit.
    if (m_terminated)
    {
      return false;
    }

    m_framerateController->FrameStart();

    SDL_Event event;
    while (SDL_PollEvent(&event))
    {
      if (event.type >= SDL_KEYDOWN && event.type < SDL_CLIPBOARDUPDATE)
      {
        m_inputManager->ProcessInputEvent(event);
      }
      else if (event.type == SDL_QUIT)
      {
        SystemEvent event(ESystemMessageType::Exit);
        m_messenger->Submit(event);
        m_terminated = true;
      }
      else if (event.type == SDL_APP_LOWMEMORY)
      {
        SystemEvent event(ESystemMessageType::LowMemory);
        m_messenger->Submit(event);
      }
      else if (event.type == SDL_APP_DIDENTERBACKGROUND)
      {
        SystemEvent event(ESystemMessageType::EnteredBackground);
        m_messenger->Submit(event);
      }
      else if (event.type == SDL_APP_DIDENTERFOREGROUND)
      {
        SystemEvent event(ESystemMessageType::EnteredForeground);
        m_messenger->Submit(event);
      }
    }

    // Update the input device states
    m_inputManager->UpdateStates();

    m_messenger->Update(m_framerateController->GetFrameTime());

    return true;
  }

  void Platform::FrameEnd(void)
  {
    m_framerateController->FrameEnd();
  }
}
