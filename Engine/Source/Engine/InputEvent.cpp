#include "Precompiled.h"
#include "InputEvent.h"

namespace PiConspiracy
{
  InputEvent::InputEvent(const InputState& a_inputState)
    : EventBase
    , m_inputState(a_inputState)
  {

  }

  InputEvent::~InputEvent(void)
  {

  }

  bool InputEvent::IsInputType(EInputType a_type)
  {
    return a_type == m_inputState.m_type;
  }

  bool InputEvent::IsButton(EMouseButton a_mouseButton)
  {
    if (m_inputState.m_type == EInputType::MouseButton)
    {
      if (a_mouseButton == m_inputState.m_action.m_mouseButton)
      {
        return true;
      }
    }

    return false;
  }

  bool InputEvent::IsButton(EKeyboardButton a_keyboardButton)
  {
    if (m_inputState.m_type == EInputType::KeyboardButtonRealtime || m_inputState.m_type == EInputType::KeyboardButtonText)
    {
      if (a_keyboardButton == m_inputState.m_action.m_keyboardButton)
      {
        return true;
      }
    }

    return false;
  }

  bool InputEvent::IsMouseButtonDown(EMouseButton a_mouseButton) const
  {
    return m_inputState.m_mouseState.m_buttonStates[a_mouseButton] == EMouseButtonState::Pressed ||
      m_inputState.m_mouseState.m_buttonStates[a_mouseButton] == EMouseButtonState::Held;
  }

  bool InputEvent::IsMouseButtonPressed(EMouseButton a_mouseButton) const
  {
    return m_inputState.m_mouseState.m_buttonStates[a_mouseButton] == EMouseButtonState::Pressed;
  }

  bool InputEvent::IsMouseButtonReleased(EMouseButton a_mouseButton) const 
  {
    return m_inputState.m_mouseState.m_buttonStates[a_mouseButton] == EMouseButtonState::Released;
  }

  bool InputEvent::IsMouseButtonDoubleClicked(EMouseButton a_mouseButton) const
  {
    return m_inputState.m_mouseState.m_buttonStates[a_mouseButton] == EMouseButtonState::DoubleClicked;
  }

  bool InputEvent::IsMouseButtonHeld(EMouseButton a_mouseButton, uint16 a_heldTime) const
  {
    TODO("need to do time check");
    return m_inputState.m_mouseState.m_buttonStates[a_mouseButton] == EMouseButtonState::Held;
  }

  bool InputEvent::IsMouseButtonDragged(EMouseButton a_mouseButton) const
  {
    return m_inputState.m_mouseState.m_buttonsDragged[a_mouseButton];
  }

  int16 InputEvent::GetHorizontalScroll(void) const
  {
    return m_inputState.m_mouseState.m_horizontalScroll;
  }

  int16 InputEvent::GetVerticalScroll(void) const
  {
    return m_inputState.m_mouseState.m_verticalScroll;
  }

  void InputEvent::GetMousePosition(int32& a_x, int32 a_y) const
  {
    a_x = m_inputState.m_mouseState.m_xPosition;
    a_y = m_inputState.m_mouseState.m_yPosition;
  }

  void InputEvent::GetMouseMotion(int32& a_x, int32 a_y) const
  {
    a_x = m_inputState.m_mouseState.m_xMotion;
    a_y = m_inputState.m_mouseState.m_yPosition;
  }

  bool InputEvent::IsKeyboardButtonDown(EKeyboardButton a_keyboardButton) const
  {
    return m_inputState.m_keyboardState.m_buttonStates[a_keyboardButton] == EKeyboardButtonState::Pressed ||
      m_inputState.m_keyboardState.m_buttonStates[a_keyboardButton] == EKeyboardButtonState::Held;
  }

  bool InputEvent::IsKeyboardButtonPressed(EKeyboardButton a_keyboardButton) const
  {
    return m_inputState.m_keyboardState.m_buttonStates[a_keyboardButton] == EKeyboardButtonState::Pressed;
  }

  bool InputEvent::IsKeyboardButtonReleased(EKeyboardButton a_keyboardButton) const
  {
    //ENGINE_LOG("%d, %d", m_inputState.m_keyboardState.m_buttonStates[14], m_inputState.m_keyboardState.m_buttonStates[13]);
    return m_inputState.m_keyboardState.m_buttonStates[a_keyboardButton] == EKeyboardButtonState::Released;
  }

  bool InputEvent::IsKeyboardButtonHeld(EKeyboardButton a_keyboardButton, uint16 a_heldTime) const
  {
    TODO("need to do time check");
    return m_inputState.m_keyboardState.m_buttonStates[a_keyboardButton] == EKeyboardButtonState::Held;
  }
}
