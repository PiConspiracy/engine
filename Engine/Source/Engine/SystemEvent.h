#pragma once

namespace PiConspiracy
{
  struct SystemMessageType
  {
    enum Enum
    {
      Exit,
      LowMemory,
      EnteredBackground,
      EnteredForeground
    };
  };
  
  typedef SystemMessageType::Enum ESystemMessageType;
  
  class SystemEvent : public Event
  {
  public:
    SystemEvent(ESystemMessageType a_eventType)
      : EventBase
      , m_eventType(a_eventType)
    {
  
    }
  
    ESystemMessageType m_eventType;
  };
}