#include "Precompiled.h"
#include "FrameRateController.h"

PUSH_BASIC_TYPE_RESTRICTORS

#undef unsigned
#undef int
#undef short
#undef long

#include <SDL2/SDL_timer.h>
POP_BASIC_TYPE_RESTRICTORS

namespace PiConspiracy
{
	#define MIN_FRAME_TIME 0.016667f
	FrameRateController::FrameRateController(int32 a_maxFrameRate)
    : m_tickStart(0)
    , m_tickEnd(0)
    , m_frameTime(0.0f)
    , m_elaspsedTime(0.0f)
    , m_maxFrameRate(a_maxFrameRate)
    , m_ticksPerFrame(1000.0f / m_maxFrameRate)
	{

	}

	FrameRateController::~FrameRateController(void)
	{

	}

	void FrameRateController::FrameStart(void)
	{
		m_tickStart = SDL_GetTicks();
	}

	void FrameRateController::FrameEnd(void)
	{
		m_tickEnd = SDL_GetTicks();
		while (m_tickEnd- m_tickStart < m_ticksPerFrame)
		{
			m_tickEnd = SDL_GetTicks();
		}
		m_frameTime = m_tickEnd - m_tickStart;
	}

	void FrameRateController::SetMaxFrameRate(int32 a_maxFrameRate)
	{
		if (a_maxFrameRate == 0)
		{
			m_maxFrameRate = UINT_MAX;
		}
		else
		{
			m_maxFrameRate = a_maxFrameRate;
		}
		m_ticksPerFrame = 1000.0f / m_maxFrameRate;
	}

	float FrameRateController::GetFrameTime(void)
	{
		return m_frameTime;
	}

	float FrameRateController::GetElapsedTime(void)
	{
		return m_elaspsedTime;
	}

	float FrameRateController::GetMaxFrameRate(void)
	{
		return m_maxFrameRate;
	}

}