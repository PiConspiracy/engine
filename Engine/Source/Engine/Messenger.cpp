#include "Precompiled.h"
#include "Messenger.h"

namespace PiConspiracy
{
  Messenger::Messenger(void)
  {

  }

  Messenger::~Messenger(void)
  {
    ENGINE_CRITICAL(m_events.empty(), "Event queue is not empty on messenger destruction!");

    for (auto it = m_archetypes.begin(); it != m_archetypes.end(); ++it)
    {
      delete it->second;
    }
  }

  void Messenger::Register(Listener* a_listener)
  {
    for (size_t i = 0; i < m_listeners.size(); ++i)
    {
      if (m_listeners[i] == a_listener)
      {
        ENGINE_CRITICAL("Failed to register listener: listener already registered!");
      }
    }

    m_listeners.push_back(a_listener);
  }

  void Messenger::Unregister(Listener* a_listener)
  {
    for (size_t i = 0; i < m_listeners.size(); ++i)
    {
      if (m_listeners[i] == a_listener)
      {
        m_listeners.erase(m_listeners.begin() + i);
        return;
      }
    }

    ENGINE_CRITICAL("Failed to unregister listener: listener not found!");
  }

  void Messenger::Update(const float t)
  {
    size_t eventCount = m_events.unsafe_size();
    while (eventCount != 0)
    {
      EventMetadata event;
      while (!m_events.try_pop(event)) {}

      switch (event.m_timing)
      {
      case EEventTiming::Instant:
      {
        Send(&event);
      }
      break;

      case EEventTiming::DelayedMs:
      {
        event.m_timer -= t;
        if (event.m_timer > 0.0f)
        {
          m_events.push(event);
        }
        else
        {
          Send(&event);
        }
      }
      break;

      case EEventTiming::DelayedFrames:
      {
        --event.m_frames;
        if (event.m_frames > 0)
        {
          m_events.push(event);
        }
        else
        {
          Send(&event);
        }
      }
      break;

      default:
        break;
      }

      --eventCount;
    }
  }

  void Messenger::Send(const EventMetadata* a_eventMetadata)
  {
    auto it = m_archetypes.find(a_eventMetadata->m_event->GetHash());
    if (it == m_archetypes.end())
    {
      ENGINE_WARNING("Event could not find the listeners. Make sure event hash is correct!! (Did you use EventBase for base initializer?)");
      return;
    }

    for (Listener* subscriber : *it->second)
    {
      subscriber->OnEvent(a_eventMetadata->m_event);
    }
  }
}
