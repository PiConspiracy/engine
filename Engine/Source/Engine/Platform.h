#pragma once

namespace PiConspiracy
{
  class Window;

  class Platform : public Singleton<Platform>
  {
  public:
    Platform(void);
    ~Platform(void);

    // Public platform hooks into SDL.

    // Get the OS platform name.
    string GetPlatformName(void) const;

    // Create a window based on given parameters. The window will auto-cleanup upon destruction.
    Window* CreateWindow(const string& a_name, int32 a_x, int32 a_y, int32 a_width, int32 a_height, uint32 a_flags);

    // Initialize the platform.
    void Intitialize(void);

    // Update loop that haldles all the SDL events.
    bool Update(void);

    // Stop the frame counter
    void FrameEnd(void);

  private:
    class InputManager* m_inputManager;
    class Messenger* m_messenger;
    class FrameRateController* m_framerateController;
    bool m_terminated;
  };
}
