#include "Precompiled.h"
#include "JsonParser.h"

PUSH_BASIC_TYPE_RESTRICTORS

#undef unsigned
#undef int
#undef short
#undef long
#include <fstream>
#include <nlohmann/json.hpp>
#include <iostream>
POP_BASIC_TYPE_RESTRICTORS

namespace PiConspiracy
{
  JsonParser::JsonParser(const string& a_filePath)
  {
    m_jsonObject = SharedPtr<json>(new json());
    m_jsonPath = a_filePath;

    std::ifstream in;
    in.open(a_filePath, std::ifstream::in);

    if (in.is_open())
    {
      in >> (*m_jsonObject);
      in.close();
    }
    else
    {
      ENGINE_WARNING(in.is_open(), "couldnt open %s\n", a_filePath);
    }
  }

  JsonParser::~JsonParser(void)
  {

  }

  const int32 JsonParser::ParseInt(SharedPtr<json> a_obj, const string & a_attribute) const
  {
    auto exists = a_obj->find(a_attribute);
    if (exists != a_obj->end())
    {
      return exists.value();
    }
    return int32();
  }

  const string JsonParser::ParseString(SharedPtr<json> a_obj, const string& a_attribute) const
  {
    auto it1 = a_obj->find(a_attribute);
    if (it1 != a_obj->end())
    {
      return it1.value();
    }

    return string();
  }

  const float JsonParser::ParseFloat(SharedPtr<json> a_obj, const string & a_attribute) const
  {
    auto it1 = a_obj->find(a_attribute);
    if (it1 != a_obj->end())
    {
      return it1.value();
    }

    return 0.0f;
  }

  const bool JsonParser::ParseBool(SharedPtr<json> a_obj, const string & a_attribute) const
  {
    auto it1 = a_obj->find(a_attribute);
    if (it1 != a_obj->end())
    {
      return it1.value();
    }

    return false;
  }

  bool JsonParser::ValueExists(SharedPtr<json> a_obj, const string & a_attribute)
  {
    auto it1 = a_obj->find(a_attribute);
    return it1 != a_obj->end();
  }

  const SharedPtr<json> JsonParser::GetRoot(void) const
  {
    return m_jsonObject;
  }

  const SharedPtr<json> JsonParser::GetObject(const SharedPtr<json> a_parent, const string& a_attribute, size_t a_index) const
  {
    auto it = a_parent->find(a_attribute);
    if (it == a_parent->end())
    {
      ENGINE_WARNING("The parent object does not contain the argument %s", a_attribute);
      return nullptr;
    }

    const json& object = it.value();
    if (object.type() == json::value_t::array)
    {
      if (object.size() > a_index)
      {
        return SharedPtr<json>(new json(object[a_index]));
      }
      else
      {
        ENGINE_WARNING("Bad index for object, object array size %d, requested index %d", object.size(), a_index);
      }
    }

    return SharedPtr<json>(new json(object));
  }

  const SharedPtr<json> JsonParser::GetObjectArray(const SharedPtr<json> a_parent, const string & a_attribute) const
  {
    auto it = a_parent->find(a_attribute);
    if (it == a_parent->end())
    {
      ENGINE_WARNING("The parent object does not contain the argument %s", a_attribute);
      return nullptr;
    }

    SharedPtr<json> object = SharedPtr<json>(new json(it.value()));
    if (object->type() == json::value_t::array)
    {
      return object;
    }
    else
    {
      ENGINE_WARNING("The object %s requested from parent is not an array", a_attribute);
    }

    return nullptr;
  }

  const size_t JsonParser::GetObjectArraySize(const SharedPtr<json> a_parent, const string& a_attribute) const
  {
    SharedPtr<json> arrayObj = GetObjectArray(a_parent, a_attribute);

    if (!arrayObj->is_null())
    {
      return arrayObj->size();
    }

    return 0;
  }
}
