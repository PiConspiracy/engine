#pragma once

namespace PiConspiracy
{
	//struct for location and the file's respective extention
	struct DirFiles
	{
		//location/path of the file
		string m_loc;
		uint32 m_locHash;

		//extension of the above file
		string m_ext;
		uint32 m_extHash;

	//	//if path is a folder, add here
	//	unordered_map<std::string, DirFiles> folder;

	};

	class FileParser : Singleton<FileParser>
	{
	public:
		FileParser(void);
		~FileParser(void);

		//function to explore what files are stored in a directory
		unordered_map<string, DirFiles> ExploreDirectory(const string& a_path);

  private:
    void ExploreDirectoryRec(const string& a_path);

		//all files found in the directory go here
		unordered_map<string, DirFiles> m_listOfFiles;

    unordered_map<string, DirFiles> m_folder;
		//unordered_map<std::string, unordered_map<std::string, DirFiles>> m_folders;
	};
}
