#pragma once

#include "InputState.h"

union SDL_Event;

namespace PiConspiracy
{
  class Messenger;

	class InputManager : Singleton<InputManager>
	{
	public:
		InputManager(void);
		~InputManager(void);

    // Processes a received input event.
    void ProcessInputEvent(const SDL_Event& a_event);

    // Updates device key states (to correctly represent the "pressed" behavior).
		void UpdateStates(void);

    // Whether treat the mouse messages as text-friendly messaging, or continuons stream.
    void KeyboardTextInput(bool a_text);

    // Set the input messenger to use.
    void SetInputMessenger(Messenger* a_messenger);

	private:
    InputState m_previousStates;
    InputState m_currentStates;
    class Mouse* m_mouse;
    class Keyboard* m_keyboard;
    Messenger* m_messenger;
	};
}
