#pragma once

#define DEPLOY_MAIN \
int main(int argc, char** argv) \
{ \
  PiConspiracy::Messenger messenger; \
  PiConspiracy::Application app; \
  \
  app.InitializeBase(argc, argv); \
  app.Subscribe(); \
  app.UpdateBase(); \
  app.Unsubscribe(); \
  return static_cast<int>(app.ExitBase()); \
}
