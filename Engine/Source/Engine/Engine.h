#pragma once

// Messenger system
#include "Event.h"
#include "Listener.h"
#include "Messenger.h"

// Input event
#include "InputEvent.h"

// Base application (lowest level, engine).
#include "ApplicationBase.h"

// App entry point.
#include "EntryPoint.h"

// Window class
#include "Window.h"

//File Parser
#include "FileParser.h"

// Global system event
#include "SystemEvent.h"


TODO("Remove, should go through the file parser");
//json parser
#include "JsonParser.h"
