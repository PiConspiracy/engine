#pragma once

#include "Core/Core.h"
#include "Math/Math.h"

// Messenger system
#include "Event.h"
#include "Listener.h"
#include "Messenger.h"

// Input event
#include "InputEvent.h"

#include "FileParser.h"

TODO("Remove, should go through the file parser");
//json parser
#include "JsonParser.h"
