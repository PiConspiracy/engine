#include "Precompiled.h"
#include "FileParser.h"

PUSH_BASIC_TYPE_RESTRICTORS

#undef unsigned
#undef int
#undef short
#undef long

#include "dirent.h"

TODO("REMOVE IOSTREAM")
#include <iostream>

POP_BASIC_TYPE_RESTRICTORS


namespace PiConspiracy 
{
	
	FileParser::FileParser(void)
	{
	}


	FileParser::~FileParser(void)
	{
	}

  unordered_map<string, DirFiles> FileParser::ExploreDirectory(const string& a_path)
	{
    ExploreDirectoryRec(a_path);

    return m_listOfFiles;
	}

  void FileParser::ExploreDirectoryRec(const string& a_path)
  {
    DIR *directory;
    struct dirent *files;
    struct stat info;
    DirFiles currFile;

    //opening the directory
    directory = opendir(a_path.c_str());
    if (!directory)
    {
      std::cout << "Path specified not found \n" << a_path;

      return;
    }

    //reading the directory
    while ((files = readdir(directory)) != NULL)
    {
      if (files->d_name[0] != '.')
      {
        //get the path
        currFile.m_loc = std::string(a_path) + "/" + std::string(files->d_name);
        currFile.m_locHash = Hash(currFile.m_loc);

        //get the extension of the above path
        currFile.m_ext = currFile.m_loc.substr(currFile.m_loc.find_last_of(".") + 1);
        currFile.m_extHash = Hash(currFile.m_ext);

        std::pair<std::string, DirFiles> keynFile = { currFile.m_loc, currFile };

        //push them in the vector
        m_listOfFiles.insert(keynFile);


        //std::cout << currFile.loc << std::endl << currFile.ext << std::endl;

        stat(currFile.m_loc.c_str(), &info);
        if (S_ISDIR(info.st_mode))
        {
          ExploreDirectoryRec((char*)currFile.m_loc.c_str());
        }

      }
    }

    //closing the directory
    closedir(directory);
  }
}