#pragma once

namespace PiConspiracy
{
  // These macros are only valid for subscription/unsubscription in the listener.
#define SUBSCRIBE_EVENT(x) m_messenger->Subscribe<x>(this, STRINGIFY(x))
#define UNSUBSCRIBE_EVENT(x) m_messenger->Unsubscribe<x>(this)

  class Listener
  {
  public:
    virtual ~Listener(void);

    virtual void Subscribe(void) = 0;
    virtual void Unsubscribe(void) = 0;
    
    virtual void OnEvent(const SharedPtr<Event> a_event) = 0;

  protected:
    Listener(void);
    class Messenger* m_messenger;
  };
}

