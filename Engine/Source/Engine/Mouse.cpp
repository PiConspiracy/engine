#include "Precompiled.h"
#include "Mouse.h"
#include "InputState.h"

PUSH_BASIC_TYPE_RESTRICTORS

#undef unsigned
#undef int
#undef short
#undef long

#include <SDL2/SDL.h>
#include <math.h>

POP_BASIC_TYPE_RESTRICTORS

namespace PiConspiracy
{
  struct MouseButtonData
  {
    uint32 m_timestamp;
    uint32 m_xCoordinate;
    uint32 m_yCoordinate;
    EMouseButton m_button;
    bool m_pressed;
    bool m_doubleClicked;
  };

  Mouse::Mouse(void)
  {
    m_previousMouseMessages = new MouseButtonData[2];

    memset(m_previousMouseMessages, 0, sizeof(MouseButtonData) * 2);
    m_previousMouseMessages[0].m_button = EMouseButton::None;
    m_previousMouseMessages[1].m_button = EMouseButton::None;

    m_previousMouseStates = 0;
    m_doubleClickRadius = 1;
    m_doubleClickMilliseconds = 500;
  }

  Mouse::~Mouse(void)
  {
    delete[] m_previousMouseMessages;
  }

  void Mouse::ProcessEvent(const SDL_Event& a_event, InputState& a_inputState)
  {
    switch (a_event.type)
    {
    case SDL_MOUSEMOTION:
    {
      a_inputState.m_type = EInputType::MouseMovement;
      a_inputState.m_mouseState.m_xMotion = static_cast<int16>(a_event.motion.xrel);
      a_inputState.m_mouseState.m_yMotion = static_cast<int16>(a_event.motion.yrel);
      a_inputState.m_mouseState.m_xPosition = a_event.motion.x;
      a_inputState.m_mouseState.m_yPosition = a_event.motion.y;

      for (int32 i = 0; i < EMouseButton::Num; ++i)
      {
        if (a_inputState.m_mouseState.m_buttonStates[i] == EMouseButtonState::Pressed ||
          a_inputState.m_mouseState.m_buttonStates[i] == EMouseButtonState::DoubleClicked ||
          a_inputState.m_mouseState.m_buttonStates[i] == EMouseButtonState::Held)
        {
          a_inputState.m_mouseState.m_buttonsDragged[i] = true;
        }
      }
    }
    break;

    case SDL_MOUSEBUTTONDOWN:
    {
      a_inputState.m_type = EInputType::MouseButton;
      a_inputState.m_mouseState.m_buttonStates[a_event.button.button - 1] = EMouseButtonState::Pressed;
      a_inputState.m_mouseState.m_pressedTime[a_event.button.button - 1] = a_event.button.timestamp;
      a_inputState.m_action.m_mouseButton = static_cast<EMouseButton>(a_event.button.button);

      MouseButtonData data;
      data.m_button = static_cast<EMouseButton>(a_event.button.button);
      data.m_pressed = true;
      data.m_xCoordinate = a_event.button.x;
      data.m_yCoordinate = a_event.button.y;
      data.m_timestamp = a_event.button.timestamp;
      data.m_doubleClicked = false;

      if (m_previousMouseMessages[0].m_button == a_event.button.button && 
        !m_previousMouseMessages[0].m_pressed &&
        m_previousMouseMessages[1].m_button == a_event.button.button &&
        m_previousMouseMessages[1].m_pressed && 
        !m_previousMouseMessages[1].m_doubleClicked)
      {
        if (data.m_timestamp - m_previousMouseMessages[1].m_timestamp <= m_doubleClickMilliseconds &&
          abs(int64(data.m_xCoordinate - m_previousMouseMessages[1].m_xCoordinate)) < m_doubleClickRadius &&
          abs(int64(data.m_yCoordinate - m_previousMouseMessages[1].m_yCoordinate)) < m_doubleClickRadius)
        {
          data.m_doubleClicked = true;
          a_inputState.m_mouseState.m_buttonStates[a_event.button.button - 1] = EMouseButtonState::DoubleClicked;
        }
      }

      m_previousMouseMessages[1] = m_previousMouseMessages[0];
      m_previousMouseMessages[0] = data;
    }
    break;

    case SDL_MOUSEBUTTONUP:
    {
      a_inputState.m_type = EInputType::MouseButton;
      a_inputState.m_mouseState.m_buttonStates[a_event.button.button - 1] = EMouseButtonState::Released;
      a_inputState.m_mouseState.m_pressedTime[a_event.button.button - 1] = 0;
      a_inputState.m_mouseState.m_buttonsDragged[a_event.button.button - 1] = false;
      a_inputState.m_action.m_mouseButton = static_cast<EMouseButton>(a_event.button.button);

      MouseButtonData data;
      data.m_button = static_cast<EMouseButton>(a_event.button.button);
      data.m_pressed = false;
      data.m_xCoordinate = a_event.button.x;
      data.m_yCoordinate = a_event.button.y;
      data.m_timestamp = a_event.button.timestamp;
      data.m_doubleClicked = false;

      m_previousMouseMessages[1] = m_previousMouseMessages[0];
      m_previousMouseMessages[0] = data;
    }
    break;

    case SDL_MOUSEWHEEL:
    {
      a_inputState.m_type = EInputType::MouseWheel;
      a_inputState.m_mouseState.m_horizontalScroll = static_cast<int16>(a_event.wheel.x);
      a_inputState.m_mouseState.m_verticalScroll = static_cast<int16>(a_event.wheel.y);
    }
    break;

    default:
      ENGINE_CRITICAL("Bad Mouse message");
      break;
    }
  }
}

