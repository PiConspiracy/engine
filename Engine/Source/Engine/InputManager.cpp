#include "Precompiled.h"
#include "InputManager.h"

#include "Mouse.h"
#include "Keyboard.h"
#include "InputEvent.h"

PUSH_BASIC_TYPE_RESTRICTORS

#undef unsigned
#undef int
#undef short
#undef long

#include <memory>
#include <SDL2/SDL.h>
#include <SDL2/SDL_syswm.h>

POP_BASIC_TYPE_RESTRICTORS

namespace PiConspiracy
{
 	InputManager::InputManager(void)
	{
    m_mouse = new Mouse();
    m_keyboard = new Keyboard();
    m_messenger = nullptr;
	}

	InputManager::~InputManager(void)
	{
    delete m_keyboard;
    delete m_mouse;
	}

  void InputManager::ProcessInputEvent(const SDL_Event& a_event)
  {
    if (a_event.type >= SDL_KEYDOWN && a_event.type < SDL_MOUSEMOTION)
    {     
      bool send = m_keyboard->ProcessEvent(a_event, m_currentStates);
      if (send)
      {
        if (m_messenger != nullptr)
        {
          InputEvent message(m_currentStates);
          m_messenger->Submit(message);
        }
      }
    }
    else if (a_event.type >= SDL_MOUSEMOTION && a_event.type < SDL_JOYAXISMOTION)
    {
      m_mouse->ProcessEvent(a_event, m_currentStates);
      if (m_messenger != nullptr)
      {
        InputEvent message(m_currentStates);
        m_messenger->Submit(message);
      }
    }
    else if (a_event.type >= SDL_JOYAXISMOTION && a_event.type < SDL_CONTROLLERAXISMOTION)
    {

    }
    else if (a_event.type >= SDL_CONTROLLERAXISMOTION && a_event.type < SDL_FINGERDOWN)
    {

    }
    else if (a_event.type >= SDL_FINGERDOWN && a_event.type < SDL_DOLLARGESTURE)
    {

    }
    else
    {

    }
  }

	void InputManager::UpdateStates(void)
	{
    // Update mouse buttons
    for (int32 i = 0; i < EMouseButton::Num; ++i)
    {
      EMouseButtonState& currentState = m_currentStates.m_mouseState.m_buttonStates[i];
      EMouseButtonState& previousState = m_previousStates.m_mouseState.m_buttonStates[i];

      if (previousState == EMouseButtonState::Released && currentState == EMouseButtonState::Released)
      {
        currentState = EMouseButtonState::Idle;
      }
      else if (previousState == EMouseButtonState::Pressed && currentState == EMouseButtonState::Pressed)
      {
        currentState = EMouseButtonState::Held;
      }
      else if (previousState == EMouseButtonState::DoubleClicked && currentState == EMouseButtonState::DoubleClicked)
      {
        currentState = EMouseButtonState::Held;
      }

      if (currentState == EMouseButtonState::Held)
      {
        m_currentStates.m_action.m_mouseButton = static_cast<EMouseButton>(i + 1);
        m_currentStates.m_type = EInputType::MouseButton;

        if (m_messenger != nullptr)
        {
          InputEvent message(m_currentStates);
          m_messenger->Submit(message);
        }
      }
    }

    for (int32 i = 0; i < EKeyboardButton::Num; ++i)
    {
      if (m_currentStates.m_keyboardState.m_buttonStates[i] != EKeyboardButton::SCANCODE_UNKNOWN)
      {
        EKeyboardButtonState& currentState = m_currentStates.m_keyboardState.m_buttonStates[i];
        EKeyboardButtonState& previousState = m_previousStates.m_keyboardState.m_buttonStates[i];

        if (previousState == EKeyboardButtonState::Released && currentState == EKeyboardButtonState::Released)
        {
          currentState = EKeyboardButtonState::Idle;
        }
        else if (previousState == EKeyboardButtonState::Pressed && currentState == EKeyboardButtonState::Pressed)
        {
          currentState = EKeyboardButtonState::Held;
        }

        // Only send real-time messages with the non-text input
        if (currentState == EKeyboardButtonState::Held && !m_keyboard->IsTextInputEnabled())
        {
          m_currentStates.m_action.m_keyboardButton = static_cast<EKeyboardButton>(i);
          m_currentStates.m_type = EInputType::KeyboardButtonRealtime;

          if (m_messenger != nullptr)
          {
            InputEvent message(m_currentStates);
            m_messenger->Submit(message);
          }
        }
      }
    }

    m_previousStates = m_currentStates;
	}

  void InputManager::KeyboardTextInput(bool a_text)
  {
    m_keyboard->KeyboardTextInput(a_text);
  }

  void InputManager::SetInputMessenger(Messenger* a_messenger)
  {
    m_messenger = a_messenger;
  }
}
