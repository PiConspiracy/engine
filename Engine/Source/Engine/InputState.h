#pragma once

#include "MouseState.h"
#include "KeyboardState.h"

namespace PiConspiracy
{
  struct InputType
  {
    enum Enum
    {
      None = 0, 

      MouseButton,
      MouseMovement,
      MouseWheel,
      KeyboardButtonRealtime,
      KeyboardButtonText
    };
  };

  typedef InputType::Enum EInputType;

  union InputAction
  {
    EKeyboardButton m_keyboardButton;
    EMouseButton m_mouseButton;
  };

  class InputState
  {
  public:
    InputState(void)
      : m_type(EInputType::None)
    {
      memset(&m_mouseState, 0, sizeof(MouseState));
      memset(&m_keyboardState, 0, sizeof(KeyboardState));
      m_action.m_keyboardButton = EKeyboardButton::SCANCODE_UNKNOWN;
      m_type = EInputType::None;
    }

    ~InputState(void) {}

    MouseState m_mouseState;
    KeyboardState m_keyboardState;
    InputAction m_action;
    EInputType m_type;
  };
}

