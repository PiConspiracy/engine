#pragma once
/* Start Header -------------------------------------------------------
Copyright (C) 2018 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: FrameRateManager.h
Purpose: Provides timer functionality to the engine
Language: C++
Project: GAM550
Author: Sujay shah
Creation date: 9/10/18
- End Header --------------------------------------------------------*/

namespace PiConspiracy
{
	class FrameRateController : Singleton<FrameRateController>
	{
	public:
		FrameRateController(int32 a_maxFrameRate=60);
		~FrameRateController(void);

		void FrameStart(void);
		void FrameEnd(void);
		void SetMaxFrameRate(int32 a_maxFrameRate = 60);
		float GetFrameTime(void);
		float GetElapsedTime(void);
		float GetMaxFrameRate(void);

	private:
		uint32 m_tickStart;
		uint32 m_tickEnd;
		float m_frameTime;
		float m_elaspsedTime;
		uint32 m_maxFrameRate;
		float m_ticksPerFrame;
	};
}
