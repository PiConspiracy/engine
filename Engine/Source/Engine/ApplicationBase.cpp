#include "Precompiled.h"
#include "ApplicationBase.h"
#include "Platform.h"

namespace PiConspiracy
{
  ApplicationBase::ApplicationBase(void)
  {
    m_platform = new Platform();
  }

  ApplicationBase::~ApplicationBase(void)
  {
    delete m_platform;
  }

  void ApplicationBase::InitializeBase(int32 a_argumentCount, char** a_arguments)
  {
    m_platform->Intitialize();

    Initialize(a_argumentCount, a_arguments);
  }

  void ApplicationBase::UpdateBase(void)
  {
    while (m_platform->Update())
    {
      Update();

      m_platform->FrameEnd();
    }
  }

  int32 ApplicationBase::ExitBase(void)
  { 
    Exit();

    return 0;
  }

  Window* ApplicationBase::CreateWindow(const string& a_name, int32 a_x, int32 a_y, int32 a_width, int32 a_height, uint32 a_flags)
  {
    return m_platform->CreateWindow(a_name, a_x, a_y, a_width, a_height, a_flags);
  }
}
