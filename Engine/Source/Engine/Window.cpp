#include "Precompiled.h"
#include "Window.h"

PUSH_BASIC_TYPE_RESTRICTORS

#undef unsigned
#undef int
#undef short
#undef long

#include "SDL2/SDL.h"
#include "SDL2/SDL_syswm.h"

#undef CreateWindow

POP_BASIC_TYPE_RESTRICTORS

namespace PiConspiracy
{
  Window::Window(WindowDesc& a_desc)
    : m_desc(a_desc)
  {

  }

  uint32 Window::GetWindowFlags(Window* a_window)
  {
    return SDL_GetWindowFlags(static_cast<SDL_Window*>(m_desc.m_sdlWindow));
  }

  void Window::SetWindowTitle(Window* a_window, const string& a_title)
  {
    SDL_SetWindowTitle(static_cast<SDL_Window*>(m_desc.m_sdlWindow), a_title.c_str());
  }

  string Window::GetWindowTitle(Window* a_window)
  {
    return SDL_GetWindowTitle(static_cast<SDL_Window*>(m_desc.m_sdlWindow));
  }

  //SDL_SetWindowIcon

  void Window::SetWindowPosition(Window* a_window, int32 a_x, int32 a_y)
  {
    SDL_SetWindowPosition(static_cast<SDL_Window*>(m_desc.m_sdlWindow), a_x, a_y);
  }

  void Window::GetWindowPosition(Window* a_window, int32& a_x, int32& a_y)
  {
    SDL_GetWindowPosition(static_cast<SDL_Window*>(m_desc.m_sdlWindow), &a_x, &a_y);
  }

  void Window::SetWindowSize(Window* a_window, int32 a_width, int32 a_height)
  {
    SDL_SetWindowSize(static_cast<SDL_Window*>(m_desc.m_sdlWindow), a_width, a_height);
  }

  void Window::GetWindowSize(Window* a_window, int32& a_width, int32& a_height)
  {
    SDL_GetWindowSize(static_cast<SDL_Window*>(m_desc.m_sdlWindow), &a_width, &a_height);
  }

  void Window::SetWindowMinimumSize(Window* a_window, int32 a_width, int32 a_height)
  {
    SDL_SetWindowMinimumSize(static_cast<SDL_Window*>(m_desc.m_sdlWindow), a_width, a_height);
  }

  void Window::GetWindowMinimumSize(Window* a_window, int32& a_width, int32& a_height)
  {
    SDL_GetWindowMinimumSize(static_cast<SDL_Window*>(m_desc.m_sdlWindow), &a_width, &a_height);
  }

  void Window::SetWindowMaximumSize(Window* a_window, int32 a_width, int32 a_height)
  {
    SDL_SetWindowMaximumSize(static_cast<SDL_Window*>(m_desc.m_sdlWindow), a_width, a_height);
  }

  void Window::GetWindowMaximumSize(Window* a_window, int32& a_width, int32& a_height)
  {
    SDL_GetWindowMaximumSize(static_cast<SDL_Window*>(m_desc.m_sdlWindow), &a_width, &a_height);
  }

  void Window::SetWindowBordered(Window* a_window, bool a_bordered)
  {
    SDL_SetWindowBordered(static_cast<SDL_Window*>(m_desc.m_sdlWindow), static_cast<SDL_bool>(a_bordered));
  }

  void Window::ShowWindow(Window* a_window, bool a_visible)
  {
    if (a_visible)
    {
      SDL_ShowWindow(static_cast<SDL_Window*>(m_desc.m_sdlWindow));
    }
    else
    {
      SDL_HideWindow(static_cast<SDL_Window*>(m_desc.m_sdlWindow));
    }
  }

  void Window::RaiseWindow(Window* a_window)
  {
    SDL_RaiseWindow(static_cast<SDL_Window*>(m_desc.m_sdlWindow));
  }

  void Window::MaximizeWindow(Window* a_window)
  {
    SDL_MaximizeWindow(static_cast<SDL_Window*>(m_desc.m_sdlWindow));
  }

  void Window::MinimizeWindow(Window* a_window)
  {
    SDL_MinimizeWindow(static_cast<SDL_Window*>(m_desc.m_sdlWindow));
  }

  void Window::RestoreWindow(Window* a_window)
  {
    SDL_RestoreWindow(static_cast<SDL_Window*>(m_desc.m_sdlWindow));
  }

  //void Window::SetWindowFullscreen(Window* a_window, bool a_fullScreen)
  //{
  //  SDL_SetWindowFullscreen(static_cast<SDL_Window*>(m_desc.m_sdlWindow))
  //}

  void Window::SetWindowGrab(Window* a_window, bool a_grabbed)
  {
    SDL_SetWindowGrab(static_cast<SDL_Window*>(m_desc.m_sdlWindow), static_cast<SDL_bool>(a_grabbed));
  }

  bool Window::GetWindowGrab(Window* a_window)
  {
    return SDL_GetWindowGrab(static_cast<SDL_Window*>(m_desc.m_sdlWindow));
  }
}

