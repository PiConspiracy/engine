#pragma once

namespace PiConspiracy
{
  class Event;
  class Listener;

  struct EventTiming
  {
    enum Enum
    {
      Instant,
      DelayedMs,
      DelayedFrames
    };
  };

  typedef EventTiming::Enum EEventTiming;

  struct EventMetadata
  {
    SharedPtr<Event> m_event;
    union
    {
      float m_timer;
      int32 m_frames;
    };
    EEventTiming m_timing;
  };

  class Messenger : Singleton<Messenger>
  {
  public:
    Messenger(void);
    ~Messenger(void);

    void Register(Listener* a_listener);
    void Unregister(Listener* a_listener);

    template <typename T>
    void Subscribe(Listener* a_listener, const string& a_name)
    {
      size_t classHash = typeid(T).hash_code();
      auto it = m_archetypes.find(classHash);
      if (it == m_archetypes.end())
      {
        vector<Listener*>* subscribers = new vector<Listener*>();
        subscribers->push_back(a_listener);

        m_archetypes.insert({ classHash, subscribers });
      }
      else
      {
        vector<Listener*>* subscribers = it->second;
        for (auto subscriber : *subscribers)
        {
          if (subscriber == a_listener)
          {
            ENGINE_CRITICAL("Attempting to register a listener to the same %s message!", typeid(T).name());
          }
        }

        subscribers->push_back(a_listener);
      }
    }

    template <typename T>
    void Unsubscribe(const Listener* a_listener)
    {
      size_t classHash = typeid(T).hash_code();
      auto it = m_archetypes.find(classHash);
      if (it != m_archetypes.end())
      {
        vector<Listener*>* subscribers = it->second;
        for (size_t i = 0; i < subscribers->size(); ++i)
        {
          if ((*subscribers)[i] == a_listener)
          {
            subscribers->erase(subscribers->begin() + i);
            return;
          }
        }

        ENGINE_CRITICAL("Cannot unsubscribe: the listener is not subscribed to %s message!", typeid(T).name());
      }
      else
      {
        ENGINE_CRITICAL("Cannot unsubscribe: %s message type was not registered!", typeid(T).name());
      }
    }

    template <class T>
    void Submit(const T& a_event)
    {
      size_t classHash = typeid(T).hash_code();
      auto it = m_archetypes.find(classHash);
      if (it != m_archetypes.end())
      {
        EventMetadata wrapper;
        wrapper.m_event = SharedPtr<T>(new T(a_event));
        wrapper.m_timing = EEventTiming::Instant;
        wrapper.m_frames = 0;

        m_events.push(wrapper);
      }
      else
      {
        ENGINE_WARNING("Nobody is subscribed to %s message type!", typeid(T).name());
      }
    }

    template <class T>
    void Submit(const T& a_event, int32 a_frameCount)
    {
      size_t classHash = typeid(T).hash_code();
      auto it = m_archetypes.find(classHash);
      if (it != m_archetypes.end())
      {
        EventMetadata wrapper;
        wrapper.m_event = SharedPtr<T>(new T(a_event));
        wrapper.m_timing = EEventTiming::DelayedFrames;
        wrapper.m_frames = a_frameCount;

        m_events.push(wrapper);
      }
      else
      {
        ENGINE_WARNING("Nobody is subscribed to %s message type!", typeid(T).name());
      }
    }

    template <class T>
    void Submit(const T& a_event, float a_milliseconds)
    {
      size_t classHash = typeid(T).hash_code();
      auto it = m_archetypes.find(classHash);
      if (it != m_archetypes.end())
      {
        EventMetadata wrapper;
        wrapper.m_event = SharedPtr<T>(new T(a_event));
        wrapper.m_timing = EEventTiming::DelayedMs;
        wrapper.m_timer = a_milliseconds;

        m_events.push(wrapper);
      }
      else
      {
        ENGINE_WARNING("Nobody is subscribed to %s message type!", typeid(T).name());
      }
    }

    // Update events for this frame. This operation
    // only accounts for the events that were pushed
    // before the method was called. If there are events
    // that are pushed while the method runs, they will
    // not be recognized until the next frame.
    void Update(const float t);

  private:
    void Send(const EventMetadata* a_eventMetadata);

    vector<Listener*> m_listeners;
    concurrent_unordered_map<size_t, vector<Listener*>*> m_archetypes;
    concurrent_queue<EventMetadata> m_events;
  };
}

