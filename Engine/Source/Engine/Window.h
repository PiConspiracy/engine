#pragma once

#include "WindowDefines.h"

namespace PiConspiracy
{
  class Platform;

  class Window
  {
  public:
    Window(WindowDesc& a_desc);

    // Get the combination of options that made this window.
    uint32 GetWindowFlags(Window* a_window);

    // Setter/Getter for window name.
    void SetWindowTitle(Window* a_window, const string& a_title);
    string GetWindowTitle(Window* a_window);

    TODO("need definition for icon. Surface?");
    //SDL_SetWindowIcon

    // Setter/Getter for window position.
    void SetWindowPosition(Window* a_window, int32 a_x, int32 a_y);
    void GetWindowPosition(Window* a_window, int32& a_x, int32& a_y);

    // Setter/Getter for window size.
    void SetWindowSize(Window* a_window, int32 a_width, int32 a_height);
    void GetWindowSize(Window* a_window, int32& a_width, int32& a_height);

    // Setter/Getter for window minimum size.
    void SetWindowMinimumSize(Window* a_window, int32 a_width, int32 a_height);
    void GetWindowMinimumSize(Window* a_window, int32& a_width, int32& a_height);

    // Setter/Getter for window minimum size.
    void SetWindowMaximumSize(Window* a_window, int32 a_width, int32 a_height);
    void GetWindowMaximumSize(Window* a_window, int32& a_width, int32& a_height);

    // Setter for borderness of the window.
    void SetWindowBordered(Window* a_window, bool a_bordered);

    // Setter for the visibility of the window.
    void ShowWindow(Window* a_window, bool a_visible);

    // Bring the window to the foreground.
    void RaiseWindow(Window* a_window);

    // Maximization/Minimization setters for window.
    void MaximizeWindow(Window* a_window);
    void MinimizeWindow(Window* a_window);

    // Restores the state of the window.
    void RestoreWindow(Window* a_window);

    TODO("From here, or from RHI?")
    // Sets whether the window is fullscreen or not.
    //void SetWindowFullscreen(Window* a_window, bool a_fullScreen);

    // Sets the input grab enabled/disabled on this window.
    void SetWindowGrab(Window* a_window, bool a_grabbed);

    // Returns the state whether the window is grabbed or not.
    bool GetWindowGrab(Window* a_window);

  private:
    WindowDesc m_desc;
  };
}
