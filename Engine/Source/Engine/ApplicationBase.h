#pragma once

namespace PiConspiracy
{
  class ApplicationBase : public Singleton<ApplicationBase>
  {
  public:
    ~ApplicationBase(void);

    // Initialize the base application (internal).
    void InitializeBase(int32 a_argumentCount, char** a_arguments);

    // Update the base application (internal).
    void UpdateBase(void);

    // Exit the base application (internal).
    int32 ExitBase(void);

    // Virtual interface for client functionality (Project level).

    // Initialize the base application.
    virtual void Initialize(int32 a_argumentCount, char** a_arguments) = 0;

    // Update the base application.
    virtual void Update(void) = 0;

    // Exit the base application.
    virtual int32 Exit(void) = 0;

    // Window creation interface helper.
    class Window* CreateWindow(const string& a_name, int32 a_x, int32 a_y, int32 a_width, int32 a_height, uint32 a_flags);

  protected:
    ApplicationBase(void);

  private:
    class Platform* m_platform;
  };
}
