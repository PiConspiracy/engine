#pragma once

union SDL_Event;

namespace PiConspiracy
{
  struct MouseButtonData;

  class Mouse
  {
  public:
    Mouse(void);
    ~Mouse(void);

    void ProcessEvent(const SDL_Event& a_event, class InputState& a_inputState);

  private:
    MouseButtonData* m_previousMouseMessages;
    uint32 m_previousMouseStates;
    uint32 m_doubleClickRadius;
    uint32 m_doubleClickMilliseconds;
  };
}
