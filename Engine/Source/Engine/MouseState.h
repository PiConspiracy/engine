#pragma once

namespace PiConspiracy
{
  struct MouseButton
  {
    enum Enum : uint8
    {
      None = 0,
      Left,
      Middle,
      Right,
      X1,
      X2,

      Num = X2
    };
  };

  typedef MouseButton::Enum EMouseButton;

  struct MouseButtonState
  {
    enum Enum : uint8
    {
      Idle = BITMASK(0),
      Pressed = BITMASK(1),
      Released = BITMASK(2),
      DoubleClicked = BITMASK(3),
      Held = BITMASK(5),
      Down = Pressed | Released | DoubleClicked
    };
  };

  typedef MouseButtonState::Enum EMouseButtonState;

  struct MouseState
  {
    uint16 m_pressedTime[EMouseButton::Num];
    EMouseButtonState m_buttonStates[EMouseButton::Num];
    bool m_buttonsDragged[EMouseButton::Num];
    int16 m_xMotion;
    int16 m_yMotion;
    int32 m_xPosition;
    int32 m_yPosition;
    int16 m_horizontalScroll;
    int16 m_verticalScroll;
  };
}

