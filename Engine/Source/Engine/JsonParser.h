#pragma once

#include "nlohmann/json_fwd.hpp"

namespace PiConspiracy
{
	using json = nlohmann::json;
	class JsonParser
	{
		public:
			JsonParser(const string& a_filePath);
			~JsonParser(void);

			//const int32 ParseInt(const json& a_parent,const string& a_attrib1,const int32 a_index)const;
			const int32 ParseInt(const SharedPtr<json> a_obj, const string& a_attribute) const;
			const string ParseString(const SharedPtr<json> a_obj,const string& a_attribute) const;
			const float ParseFloat(const SharedPtr<json> a_obj, const string& a_attribute) const;
			const bool ParseBool(const SharedPtr<json> a_obj, const string& a_attribute) const;

			//attrib checks
			bool ValueExists(const SharedPtr<json> a_obj, const string& a_attrib);

			const SharedPtr<json> GetRoot(void) const;
      const SharedPtr<json> GetObject(const SharedPtr<json> a_parent, const string& a_attribute, size_t a_index = 0) const;
			const size_t GetObjectArraySize(const SharedPtr<json> a_parent, const string& a_attribute) const;
      const SharedPtr<json> GetObjectArray(const SharedPtr<json> a_parent,const string& a_attribute) const;

	private:
    SharedPtr<json> m_jsonObject;
		string m_jsonPath;
	};
}


