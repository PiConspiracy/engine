#pragma once

#define EventBase Event(*this)

namespace PiConspiracy
{
  class Event
  {
  public:
    template <class T>
    Event(T)
      : m_hash(typeid(T).hash_code())
    {

    }

    template <class T>
    bool IsEqual() const
    {
      return m_hash == typeid(T).hash_code();
    }

    //template <class T>
    //T* Get(void) const
    //{
    //  return static_cast<T*>(this);
    //}

    size_t GetHash(void)
    {
      return m_hash;
    }

    virtual ~Event(void) = 0
    {

    }

  private:
    size_t m_hash;
  };
}

