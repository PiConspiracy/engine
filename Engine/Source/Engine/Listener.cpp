#include "Precompiled.h"
#include "Listener.h"
#include "Messenger.h"

namespace PiConspiracy
{
  Listener::Listener(void)
  {
    m_messenger = Singleton<Messenger>::Get();
    m_messenger->Register(this);
  }

  Listener::~Listener(void)
  {
    m_messenger->Unregister(this);
  }
}
