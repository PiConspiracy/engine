#pragma once

#include "InputState.h"

namespace PiConspiracy
{
  struct InputEvent : public Event
  {
    InputEvent(const InputState& a_inputState);
    ~InputEvent(void);

    // Check for input type.
    bool IsInputType(EInputType a_type);

    // Check button input specifically.
    bool IsButton(EMouseButton a_mouseButton);
    bool IsButton(EKeyboardButton a_keyboardButton);

    // All possible mouse button states.
    bool IsMouseButtonDown(EMouseButton a_mouseButton) const;
    bool IsMouseButtonPressed(EMouseButton a_mouseButton) const;
    bool IsMouseButtonReleased(EMouseButton a_mouseButton) const;
    bool IsMouseButtonDoubleClicked(EMouseButton a_mouseButton) const;
    bool IsMouseButtonHeld(EMouseButton a_mouseButton, uint16 a_heldTime = 0) const;
    bool IsMouseButtonDragged(EMouseButton a_mouseButton) const;

    // Scroll information.
    int16 GetHorizontalScroll(void) const;
    int16 GetVerticalScroll(void) const;

    // Movement information.
    void GetMousePosition(int32& a_x, int32 a_y) const;
    void GetMouseMotion(int32& a_x, int32 a_y) const;

    // All possible keyboard button states.
    bool IsKeyboardButtonDown(EKeyboardButton a_keyboardButton) const;
    bool IsKeyboardButtonPressed(EKeyboardButton a_keyboardButton) const;
    bool IsKeyboardButtonReleased(EKeyboardButton a_keyboardButton) const;
    bool IsKeyboardButtonHeld(EKeyboardButton a_keyboardButton, uint16 a_heldTime = 0) const;

  private:
    const InputState m_inputState;
  };
}


