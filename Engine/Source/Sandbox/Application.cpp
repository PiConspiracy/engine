#include "Precompiled.h"
#include "Application.h"


//remove includes below this, included just for testing..
PUSH_BASIC_TYPE_RESTRICTORS

#undef unsigned
#undef int
#undef short
#undef long
//#include <processenv.h>
POP_BASIC_TYPE_RESTRICTORS


namespace PiConspiracy
{
  

  Application::Application(void)
  {

  }

  Application::~Application(void)
  {

  }

  void Application::Subscribe(void)
  {
    SUBSCRIBE_EVENT(SystemEvent);
    SUBSCRIBE_EVENT(InputEvent);
  }

  void Application::Unsubscribe(void)
  {
    UNSUBSCRIBE_EVENT(InputEvent);
    UNSUBSCRIBE_EVENT(SystemEvent);
  }

  void Application::OnEvent(const SharedPtr<Event> a_event)
  {
    if (a_event->IsEqual<InputEvent>())
    {
      InputEvent* inputEvent = static_cast<InputEvent*>(a_event.get());

      if (inputEvent->IsButton(EKeyboardButton::SCANCODE_A))
      {
        if (inputEvent->IsKeyboardButtonPressed(EKeyboardButton::SCANCODE_A))
        {
          ENGINE_LOG("A PRESSED");
        }
        if (inputEvent->IsKeyboardButtonReleased(EKeyboardButton::SCANCODE_A))
        {
          ENGINE_LOG("A RELEASED");
        }
        if (inputEvent->IsKeyboardButtonHeld(EKeyboardButton::SCANCODE_A))
        {
          ENGINE_LOG("A HELD");
        }
      }
      if (inputEvent->IsButton(EKeyboardButton::SCANCODE_S))
      {
        if (inputEvent->IsKeyboardButtonPressed(EKeyboardButton::SCANCODE_S))
        {
          ENGINE_LOG("S PRESSED");
        }
        if (inputEvent->IsKeyboardButtonReleased(EKeyboardButton::SCANCODE_S))
        {
          ENGINE_LOG("S RELEASED");
        }
        if (inputEvent->IsKeyboardButtonHeld(EKeyboardButton::SCANCODE_S))
        {
          ENGINE_LOG("S HELD");
        }
      }
      if (inputEvent->IsButton(EKeyboardButton::SCANCODE_D))
      {
        if (inputEvent->IsKeyboardButtonPressed(EKeyboardButton::SCANCODE_D))
        {
          ENGINE_LOG("D PRESSED");
        }
        if (inputEvent->IsKeyboardButtonReleased(EKeyboardButton::SCANCODE_D))
        {
          ENGINE_LOG("D RELEASED");
        }
        if (inputEvent->IsKeyboardButtonHeld(EKeyboardButton::SCANCODE_D))
        {
          ENGINE_LOG("D HELD");
        }
      }
      if (inputEvent->IsButton(EKeyboardButton::SCANCODE_W))
      {
        if (inputEvent->IsKeyboardButtonPressed(EKeyboardButton::SCANCODE_W))
        {
          ENGINE_LOG("W PRESSED");
        }
        if (inputEvent->IsKeyboardButtonReleased(EKeyboardButton::SCANCODE_W))
        {
          ENGINE_LOG("W RELEASED");
        }
        if (inputEvent->IsKeyboardButtonHeld(EKeyboardButton::SCANCODE_W))
        {
          ENGINE_LOG("W HELD");
        }
      }
    }
  }

  void Application::Initialize(int32 a_argumentCount, char** a_arguments)
  {
	//test ur code here
   /* Tests();
	RandTest();
	JsonTest();*/

	/*FileParser *FP = new FileParser;
	
	char path[64] = "C:/Sandbox/GAM550/engine/Engine/Source";
	FP->ExploreDirectory(path);

	delete FP;*/
	
  }

  void Application::Update(void)
  {

  }

  int32 Application::Exit(void)
  {
    return 0;
  }
}