#pragma once
#include "Precompiled.h"
#include "Application.h"

//TODO : FOr Testing Purpose only. Add your tests here. REMOVE AFTER FINAL BUILD OF THE GAME

namespace PiConspiracy
{
	void Tests(void)
	{
		int32 i = BITMASK(8);
		uint32 hashres = Hash("foo", 3);

		vector<int32> v(32);

		struct FooStruct : public NonCopyable
		{
			int32 var = 5;
		};

		FooStruct original;
		//FooStruct cantCopy = original;
		//FooStruct cantCopy(original);

		struct FooSingle : public Singleton<FooSingle>
		{
			int32 bar = 10;
		};

		FooSingle s;
		//FooSingle error;

		FooSingle* fooptr = Singleton<FooSingle>::Get();
		Application* app = Singleton<Application>::Get();
		Window* window = app->CreateWindow("test", WINDOWPOS_CENTERED, WINDOWPOS_CENTERED, 800, 600, EWindowFlags::WINDOW_SHOWN);

		//DBGBREAK;

		STDStream::PrintOut("%d\n", 5);
		STDStream::PrintErr("%d, %d\n", 6, 8);

		VSStream::Print("FOOBARBAZ\n");

		//ENGINE_ERROR(3 + 5 == 10, "error, 3 + 5 is not equal to 10");
		ENGINE_LOG("bla bla");

		DateAndTime dt;
		SystemClock::GetDateAndTime(dt);

		//FStream::Write("this goes to a file\n");
		//Event<int32> e;

		class MockEvent : public Event
		{
		public:
			MockEvent() : EventBase{}
		};

		class MockListener : public Listener
		{
		public:
			virtual void Subscribe(void)
			{
				SUBSCRIBE_EVENT(MockEvent);
			}

			virtual void Unsubscribe(void)
			{
				UNSUBSCRIBE_EVENT(MockEvent);
			}

			virtual void OnEvent(const SharedPtr<Event> a_event)
			{

			}
		};

		//MockListener* l = new MockListener();
		////l.Unsubscribe();
		//l->Subscribe();
		////l.Subscribe();
		////l.Unsubscribe();
		////l.Unsubscribe();
		//MockEvent m;
		//
		//Singleton<Messenger>::Get()->Submit(m);
		//Singleton<Messenger>::Get()->Submit(MockEvent(), 345);
		//Singleton<Messenger>::Get()->Submit(MockEvent(), 23.3f);
	}

	void RandTest(void)
	{
		Random rTest;
		//	  STDStream::PrintOut("binomial distributed random test =  %d\n" ,rTest.GetBinomialDistributedRandom(100,0.5));
		//	 STDStream::PrintOut("uniformly distributed random test =  %d\n", rTest.GetUniformlyDistributedRandom(10.0f, 90.0f));
	}

	void JsonTest(void)
	{
		//example usage of parser
		std::string file = "../../../Assets/dummy.json";
		/*FILE* f;
		fopen_s(&f, file.c_str(), "r");*/
		JsonParser j(file);
		const json& jroot = j.GetRoot();
		STDStream::PrintOut("JSON test\n");
		STDStream::PrintOut("integer : %d\t", j.ParseInt(&jroot, "integer"));
		STDStream::PrintOut("float : %f\n", j.ParseFloat(&jroot, "float"));
		STDStream::PrintOut("string : %s\n", j.ParseString(&jroot, "string").c_str());

		STDStream::PrintOut("testing array of objects\n");



		//STDStream::PrintOut("obj1\t %s\n",j.ParseString(&child,"componentName").c_str());
		int32 size = j.GetObjectArraySize(jroot, "ObjectArrayDemo");

		//testing array of objects
		for (int32 i = 0; i < size; ++i)
		{
			const json& child = j.GetObject(jroot, "ObjectArrayDemo", i);
			STDStream::PrintOut("obj %d : %s\n", i + 1, j.ParseString(&child, "componentName").c_str());

			//To Do check whether obj with key names exists
			if (j.ValueExists(&child, "position"))
			{
				const json& child2 = j.GetObject(child, "position");
				STDStream::PrintOut("\tx : %d", j.ParseInt(&child2, "x"));
				STDStream::PrintOut("\tx : %d", j.ParseInt(&child2, "y"));
				STDStream::PrintOut("\tx : %d\n", j.ParseInt(&child2, "z"));
			}
			if (j.ValueExists(&child, "drawable"))
			{
				STDStream::PrintOut("\tstatus : %d\n", j.ParseBool(&child, "drawable"));
			}
		}
	}

	void QuatTest(void)
	{
		Quaternion q();
	}
}