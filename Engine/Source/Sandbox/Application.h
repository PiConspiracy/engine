#pragma once

namespace PiConspiracy
{
  class Application 
    : public ApplicationBase
    , public Singleton<Application>
    , public Listener
  {
  public:
    Application(void);
    ~Application(void);

    // Listener interface
    virtual void Subscribe(void) final;
    virtual void Unsubscribe(void) final;
    virtual void OnEvent(const SharedPtr<Event> a_event) final;

    virtual void Initialize(int32 a_argumentCount, char** a_arguments) final;
    virtual void Update(void) final;
    virtual int32 Exit(void) final;
  };
}
